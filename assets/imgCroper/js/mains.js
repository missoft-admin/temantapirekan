$(function () {
	'use strict';
	var console = window.console || {
		log: function () {}
	};
	var URL = window.URL || window.webkitURL;
	var $image = $('#images');
	var $dataX = $('#dataX');
	var $dataY = $('#dataY');
	var $dataHeight = $('#dataHeight');
	var $dataWidth = $('#dataWidth');
	var $dataRotate = $('#dataRotate');
	var $dataScaleX = $('#dataScaleX');
	var $dataScaleY = $('#dataScaleY');
	var options = {
		aspectRatio: 16 / 9,
		viewMode: 1,
        dragMode: 'move',
        autoCropArea:1,
        restore:false,
        modal:false,
		cropBoxMovable: false,
		cropBoxResizable: false,
		preview: '.img-preview',
		crop: function (e) {
			$dataX.val(Math.round(e.x));
			$dataY.val(Math.round(e.y));
			$dataHeight.val(Math.round(e.height));
			$dataWidth.val(Math.round(e.width));
			$dataRotate.val(e.rotate);
			$dataScaleX.val(e.scaleX);
			$dataScaleY.val(e.scaleY)
		}
	};
	var originalImageURL = $image.attr('src');
	var uploadedImageName = 'cropped.jpg';
	var uploadedImageType = 'image/jpeg';
	var uploadedImageURL;
	$('[data-toggle="tooltip"]').tooltip();
	$image.on({
		ready: function (e) {
			console.log(e.type)
		},
		cropstart: function (e) {
			console.log(e.type, e.action)
		},
		cropmove: function (e) {
			console.log(e.type, e.action);
			$('#reswidth2').val(e.width)
			$('#resheight2').val(e.height)
			$('#x_axis2').val(e.x);
			$('#y_axis2').val(e.y)
		},
		cropend: function (e) {
			console.log(e.type, e.action)
		},
		crop: function (e) {
			console.log(e.type, e.x, e.y, e.width, e.height, e.rotate, e.scaleX, e.scaleY);
			$('#reswidth2').val(e.width)
			$('#resheight2').val(e.height)
			$('#x_axis2').val(e.x);
			$('#y_axis2').val(e.y);
			$('#upfotoga').val(1)
		},
		zoom: function (e) {
			console.log(e.type, e.ratio)
		}
	}).cropper(options);
	if (!$.isFunction(document.createElement('canvas').getContext)) {
		$('button[data-method="getCroppedCanvas"]').prop('disabled', !0)
	}
	if (typeof document.createElement('cropper').style.transition === 'undefined') {
		$('button[data-method="rotate"]').prop('disabled', !0);
		$('button[data-method="scale"]').prop('disabled', !0)
	}
	$('.docs-toggles').on('change', 'input', function () {
		var $this = $(this);
		var name = $this.attr('name');
		var type = $this.prop('type');
		var cropBoxData;
		var canvasData;
		if (!$image.data('cropper')) {
			return
		}
		if (type === 'checkbox') {
			options[name] = $this.prop('checked');
			cropBoxData = $image.cropper('getCropBoxData');
			canvasData = $image.cropper('getCanvasData');
			options.ready = function () {
				$image.cropper('setCropBoxData', cropBoxData);
				$image.cropper('setCanvasData', canvasData)
			}
		} else if (type === 'radio') {
			options[name] = $this.val()
		}
		$image.cropper('destroy').cropper(options)
	});
	$('.docs-buttons').on('click', '[data-method]', function () {
		var $this = $(this);
		var data = $this.data();
		var cropper = $image.data('cropper');
		var cropped;
		var $target;
		var result;
		if ($this.prop('disabled') || $this.hasClass('disabled')) {
			return
		}
		if (cropper && data.method) {
			data = $.extend({}, data);
			if (typeof data.target !== 'undefined') {
				$target = $(data.target);
				if (typeof data.option === 'undefined') {
					try {
						data.option = JSON.parse($target.val())
					} catch (e) {
						console.log(e.message)
					}
				}
			}
			cropped = cropper.cropped;
			switch (data.method) {
				case 'rotate':
					if (cropped && options.viewMode > 0) {
						$image.cropper('clear')
					}
					break;
				case 'getCroppedCanvas':
					if (uploadedImageType === 'image/jpeg') {
						if (!data.option) {
							data.option = {}
						}
						data.option.fillColor = '#fff'
					}
					break
			}
			result = $image.cropper(data.method, data.option, data.secondOption);
			switch (data.method) {
				case 'rotate':
					if (cropped && options.viewMode > 0) {
						$image.cropper('crop')
					}
					break;
				case 'scaleX':
				case 'scaleY':
					$(this).data('option', -data.option);
					break;
				case 'getCroppedCanvas':
					if (result) {
						$('#image_previews').html(result)
                        console.log(result)
                    }
					break;
				case 'destroy':
					if (uploadedImageURL) {
						URL.revokeObjectURL(uploadedImageURL);
						uploadedImageURL = '';
						$image.attr('src', originalImageURL)
					}
					break
			}
			if ($.isPlainObject(result) && $target) {
				try {
					$target.val(JSON.stringify(result))
				} catch (e) {
					console.log(e.message)
				}
			}
		}
	});
	// $(document.body).on('keydown', function (e) {
	// 	if (!$image.data('cropper') || this.scrollTop > 300) {
	// 		return
	// 	}
	// 	switch (e.which) {
	// 		case 37:
	// 			e.preventDefault();
	// 			$image.cropper('move', -1, 0);
	// 			break;
	// 		case 38:
	// 			e.preventDefault();
	// 			$image.cropper('move', 0, -1);
	// 			break;
	// 		case 39:
	// 			e.preventDefault();
	// 			$image.cropper('move', 1, 0);
	// 			break;
	// 		case 40:
	// 			e.preventDefault();
	// 			$image.cropper('move', 0, 1);
	// 			break
	// 	}
	// });
	var $inputImage = $('#inputImages');
	if (URL) {
		$inputImage.change(function () {
			var files = this.files;
			var file;
			$('#nofotos').attr('hidden', 'hidden')
			$('#btn-modalFotos').removeAttr('disabled')
			if (!$image.data('cropper')) {
				return
			}
			if (files && files.length) {
				file = files[0];
				if (/^image\/\w+$/.test(file.type)) {
					uploadedImageName = file.name;
					uploadedImageType = file.type;
					if (uploadedImageURL) {
						URL.revokeObjectURL(uploadedImageURL)
					}
					uploadedImageURL = URL.createObjectURL(file);
					$image.cropper('destroy').attr('src', uploadedImageURL).cropper(options);
					console.log($inputImage.val())
				} else {
					window.alert('Please choose an image file.')
				}
			}
		})
	} else {
		$inputImage.prop('disabled', !0).parent().addClass('disabled')
	}
})
