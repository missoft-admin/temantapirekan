<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<title>Kulcimart | {title}</title>
	<meta name="description" content="Kulcimart" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- for ios 7 style, multi-resolution icon of 152x152 -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
	<link rel="apple-touch-icon" href="{img_path}logo-default.png">
	<meta name="apple-mobile-web-app-title" content="Flatkit">
	<!-- for Chrome on Android, multi-resolution icon of 196x196 -->
	<meta name="mobile-web-app-capable" content="yes">
	<link rel="shortcut icon" sizes="196x196" href="{img_path}logo-default.png">

	<!-- style -->

	<link rel="stylesheet" href="{libs_path}font-awesome/css/font-awesome.min.css" type="text/css" />
	<link rel="stylesheet" href="{libs_path}select2/dist/css/select2.min.css">

	<!-- build:css ../assets/css/app.min.css -->
	<link rel="stylesheet" href="{libs_path}bootstrap/dist/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="{css_path}app.css" type="text/css" />
	<link rel="stylesheet" href="{css_path}style.css" type="text/css" />
	<link rel="stylesheet" href="{css_path}theme/warning.css" type="text/css" />
	<link rel="stylesheet" href="{css_path}sweetalert2.min.css" type="text/css" />
	<link rel="stylesheet" href="{cropperCss}cropper.css">
	<link rel="stylesheet" href="{cropperCss}main.css">
	<!-- endbuild -->

	<!-- jQuery -->
	<script src="{libs_path}jquery/dist/jquery.min.js"></script>

	<!-- Data Table -->
	<script src="{libs_path}datatables/media/js/jquery.dataTables.min.js"></script>
	<script src="{libs_path}datatables.net-bs4/js/dataTables.bootstrap4.js"></script>
	<link rel="stylesheet" href="{libs_path}datatables.net-bs4/css/dataTables.bootstrap4.css" type="text/css" />
	<script src="{scripts_path}plugins/datatable.js"></script>

	<!-- Summer Note -->
	<link rel="stylesheet" href="{libs_path}summernote/dist/summernote.css" type="text/css" />
	<link rel="stylesheet" href="{libs_path}summernote/dist/summernote-bs4.css" type="text/css" />
	<script src="{libs_path}summernote/dist/summernote.min.js"></script>
	<script src="{libs_path}summernote/dist/summernote-bs4.min.js"></script>

	<!-- Photo Swipe -->
	<link rel="stylesheet" href="{libs_path}photo-swipe/photoswipe.css" type="text/css" />
	<link rel="stylesheet" href="{libs_path}photo-swipe/default-skin/default-skin.css" type="text/css" />
	<script src="{libs_path}photo-swipe/photoswipe.min.js"></script>
	<script src="{libs_path}photo-swipe/photoswipe-ui-default.min.js"></script>
	<style>
		.disabledbutton {
			pointer-events: none;
			opacity: 0.4;
		}
	</style>
</head>

<body>


	<div class="app" id="app">

		<!-- ############ LAYOUT START-->

		<!-- ############ Aside START-->
		<div id="aside" class="app-aside fade  box-shadow-x nav-expand white" aria-hidden="true">
			<div class="sidenav modal-dialog dk dark">
				<!-- sidenav top -->
				<div class="navbar dark-white lt">
					<!-- brand -->
					<a href="{site_url}" class="navbar-brand">
						<!-- <svg viewBox="0 0 24 24" height="28" width="28" xmlns="http://www.w3.org/2000/svg">
		    	    <path d="M0 0h24v24H0z" fill="none"/>
		    	    <path d="M19.51 3.08L3.08 19.51c.09.34.27.65.51.9.25.24.56.42.9.51L20.93 4.49c-.19-.69-.73-1.23-1.42-1.41zM11.88 3L3 11.88v2.83L14.71 3h-2.83zM5 3c-1.1 0-2 .9-2 2v2l4-4H5zm14 18c.55 0 1.05-.22 1.41-.59.37-.36.59-.86.59-1.41v-2l-4 4h2zm-9.71 0h2.83L21 12.12V9.29L9.29 21z" fill="#fff" class="fill-theme"/>
		    	</svg> -->
						<img src="{img_path}logo-web-landscape.png" alt="." height="18">
						<!-- <span class="hidden-folded d-inline">Apply</span> -->
					</a>
					<!-- / brand -->
				</div>

				<!-- Flex nav content -->
				<div class="flex hide-scroll">
					<div class="scroll">
						<div class="nav-border" data-nav>
							<?php $this->load->view('page_navigation'); ?>
						</div>
					</div>
				</div>

				<!-- sidenav bottom -->
				<div class="no-shrink lt">
					<div class="nav-fold">
						<div class="hidden-folded flex p-2-3 bg">
							<div class="d-flex p-1">
								<span style="width:40px;"></span>
								<?php if(get_by_field('idanggota',$this->session->userdata('idanggota'),'manggota')->staktif == '1'){ ?>
									<a href="#" class="px-2 text-success" style="font-size:17px;font-family:fantasy;">TOKO BUKA
								<?php }else{ ?>
									<a href="#" class="px-2 text-danger" style="font-size:17px;font-family:fantasy;">
									TOKO TUTUP
								<?php } ?>
		    			</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ############ Aside END-->

		<!-- ############ Content START-->
		<div id="content" class="app-content box-shadow-1 box-radius-1" role="main">
			<!-- Header -->
			<div class="content-header white  box-shadow-1" id="content-header">
				<div class="navbar navbar-expand-lg">
					<!-- btn to toggle sidenav on small screen -->
					<a class="d-lg-none mx-2" data-toggle="modal" data-target="#aside">
	    	    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 512 512"><path d="M80 304h352v16H80zM80 248h352v16H80zM80 192h352v16H80z"/></svg>
	    	  </a>
					<!-- Page title -->
					<div class="navbar-text nav-title flex" id="pageTitle">{toptitle}</div>

					<ul class="nav flex-row order-lg-2">
	    	    <!-- Notification -->
	    	    <li class="nav-item dropdown">
	    	      <a class="nav-link px-3" data-toggle="dropdown">
	    	        <i class="fa fa-bell text-muted"></i>
	    	        <span class="badge badge-pill up danger">14</span>
	    	      </a>
	    	      <!-- dropdown -->
	    	      <div class="dropdown-menu dropdown-menu-right w-md animate fadeIn mt-2 p-0">
	    	          <div class="scrollable hover" style="max-height: 250px">
	    	            <div class="list">
	    	                <div class="list-item " data-id="item-11">
	    	                  <span class="w-24 avatar circle blue">
	    	                      <span class="fa fa-code-fork"></span>
	    	                  </span>
	    	                  <div class="list-body">
	    	                        <a href="" class="item-title _500">Tiger Nixon</a>

	    	                      <div class="item-except text-sm text-muted h-1x">
	    	                        Looking for some client-work
	    	                      </div>

	    	                    <div class="item-tag tag hide">
	    	                    </div>
	    	                  </div>
	    	                  <div>
	    	                      <span class="item-date text-xs text-muted">16:00</span>
	    	                  </div>
	    	                </div>
	    	                <div class="list-item " data-id="item-2">
	    	                  <span class="w-24 avatar circle light-blue">
	    	                      <span class="fa fa-git"></span>
	    	                  </span>
	    	                  <div class="list-body">
	    	                        <a href="" class="item-title _500">Kygo</a>

	    	                      <div class="item-except text-sm text-muted h-1x">
	    	                        What&#x27;s the project progress now
	    	                      </div>

	    	                    <div class="item-tag tag hide">
	    	                    </div>
	    	                  </div>
	    	                  <div>
	    	                      <span class="item-date text-xs text-muted">08:05</span>
	    	                  </div>
	    	                </div>
	    	                <div class="list-item " data-id="item-12">
	    	                  <span class="w-24 avatar circle green">
	    	                      <span class="fa fa-dot-circle-o"></span>
	    	                  </span>
	    	                  <div class="list-body">
	    	                        <a href="" class="item-title _500">Ashton Cox</a>

	    	                      <div class="item-except text-sm text-muted h-1x">
	    	                        Looking for some client-work
	    	                      </div>

	    	                    <div class="item-tag tag hide">
	    	                    </div>
	    	                  </div>
	    	                  <div>
	    	                      <span class="item-date text-xs text-muted">11:30</span>
	    	                  </div>
	    	                </div>
	    	                <div class="list-item " data-id="item-4">
	    	                  <span class="w-24 avatar circle pink">
	    	                      <span class="fa fa-male"></span>
	    	                  </span>
	    	                  <div class="list-body">
	    	                        <a href="" class="item-title _500">Judith Garcia</a>

	    	                      <div class="item-except text-sm text-muted h-1x">
	    	                        Eddel upload a media
	    	                      </div>

	    	                    <div class="item-tag tag hide">
	    	                    </div>
	    	                  </div>
	    	                  <div>
	    	                      <span class="item-date text-xs text-muted">12:05</span>
	    	                  </div>
	    	                </div>
	    	                <div class="list-item " data-id="item-14">
	    	                  <span class="w-24 avatar circle brown">
	    	                      <span class="fa fa-bell"></span>
	    	                  </span>
	    	                  <div class="list-body">
	    	                        <a href="" class="item-title _500">Brielle Williamson</a>

	    	                      <div class="item-except text-sm text-muted h-1x">
	    	                        Looking for some client-work
	    	                      </div>

	    	                    <div class="item-tag tag hide">
	    	                    </div>
	    	                  </div>
	    	                  <div>
	    	                      <span class="item-date text-xs text-muted">08:00</span>
	    	                  </div>
	    	                </div>
	    	                <div class="list-item " data-id="item-9">
	    	                  <span class="w-24 avatar circle cyan">
	    	                      <span class="fa fa-puzzle-piece"></span>
	    	                  </span>
	    	                  <div class="list-body">
	    	                        <a href="" class="item-title _500">Pablo Nouvelle</a>

	    	                      <div class="item-except text-sm text-muted h-1x">
	    	                        It&#x27;s been a Javascript kind of day
	    	                      </div>

	    	                    <div class="item-tag tag hide">
	    	                    </div>
	    	                  </div>
	    	                  <div>
	    	                      <span class="item-date text-xs text-muted">15:00</span>
	    	                  </div>
	    	                </div>
	    	            </div>
	    	          </div>
	    	          <div class="d-flex px-3 py-2 b-t">
	    	            <div class="flex">
	    	              <span>6 Notifications</span>
	    	            </div>
	    	            <a href="setting.html">See all <i class="fa fa-angle-right text-muted"></i></a>
	    	          </div>
	    	      </div>
	    	      <!-- / dropdown -->
	    	    </li>

	    	    <!-- User dropdown menu -->
	    	    <li class="dropdown d-flex align-items-center">
	    	      <a href="#" data-toggle="dropdown" class="d-flex align-items-center">
	    	        <span class="avatar w-32">
	    	          <img src="{img_path}a1.jpg" alt="...">
	    	        </span>
	    	      </a>
	    	      <div class="dropdown-menu dropdown-menu-right w pt-0 mt-2 animate fadeIn">
	    	        <div class="row no-gutters b-b mb-2">
	    	          <div class="col-4 b-r">
	    	            <a href="app.user.html" class="py-2 pt-3 d-block text-center">
	    	              <i class="fa text-md fa-phone-square text-muted"></i>
	    	              <small class="d-block">Call</small>
	    	            </a>
	    	          </div>
	    	          <div class="col-4 b-r">
	    	            <a href="app.message.html" class="py-2 pt-3 d-block text-center">
	    	              <i class="fa text-md fa-comment text-muted"></i>
	    	              <small class="d-block">Chat</small>
	    	            </a>
	    	          </div>
	    	          <div class="col-4">
	    	            <a href="app.inbox.html" class="py-2 pt-3 d-block text-center">
	    	              <i class="fa text-md fa-envelope text-muted"></i>
	    	              <small class="d-block">Email</small>
	    	            </a>
	    	          </div>
	    	        </div>
	    	        <a class="dropdown-item" href="profile.html">
	    	          <span>Profile</span>
	    	        </a>
	    	        <a class="dropdown-item" href="setting.html">
	    	          <span>Settings</span>
	    	        </a>
	    	        <a class="dropdown-item" href="app.inbox.html">
	    	          <span class="float-right"><span class="badge info">6</span></span>
	    	          <span>Inbox</span>
	    	        </a>
	    	        <a class="dropdown-item" href="app.message.html">
	    	          <span>Message</span>
	    	        </a>
	    	        <div class="dropdown-divider"></div>
	    	        <a class="dropdown-item" href="docs.html">
	    	          Need help?
	    	        </a>
	    	        <a class="dropdown-item" href="{site_url}Auth/sign_out">Sign out</a>
	    	      </div>
	    	    </li>
	    	    <!-- Navarbar toggle btn -->
	    	    <li class="d-lg-none d-flex align-items-center">
	    	      <a href="#" class="mx-2" data-toggle="collapse" data-target="#navbarToggler">
	    	        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 512 512"><path d="M64 144h384v32H64zM64 240h384v32H64zM64 336h384v32H64z"/></svg>
	    	      </a>
	    	    </li>
	    	  </ul>
					<!-- Navbar collapse -->
					<div class="collapse navbar-collapse no-grow order-lg-1" id="navbarToggler">
						<form class="input-group m-2 my-lg-0">
							<span class="input-group-btn">
	    	        <button type="button" class="btn no-border no-bg no-shadow"><i class="fa fa-search"></i></button>
	    	      </span>
							<input type="text" class="form-control no-border no-bg no-shadow" placeholder="Search projects...">
						</form>
					</div>

				</div>
			</div>
			<!-- Main -->
			
			<div class="content-main tutupToko" id="content-main">
					<?php $this->load->view($content);?>
			</div>

		<!-- Footer -->
		<div class="content-footer white " id="content-footer">
			<div class="d-flex p-3">
				<span class="text-sm text-muted flex">&copy; Copyright. Kulcimart</span>
				<div class="text-sm text-muted">Version 1.1.0</div>
			</div>
		</div>
	</div>
	<!-- ############ Content END-->

	<!-- ############ LAYOUT END-->
	</div>

	<!-- ############ PHOTO SWIPE -->
		<!-- Root element of PhotoSwipe. Must have class pswp. -->
		<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

	    <!-- Background of PhotoSwipe.
	         It's a separate element, as animating opacity is faster than rgba(). -->
	    <div class="pswp__bg"></div>

	    <!-- Slides wrapper with overflow:hidden. -->
	    <div class="pswp__scroll-wrap">

	        <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
	        <div class="pswp__container">
	            <!-- don't modify these 3 pswp__item elements, data is added later on -->
	            <div class="pswp__item"></div>
	            <div class="pswp__item"></div>
	            <div class="pswp__item"></div>
	        </div>

	        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
	        <div class="pswp__ui pswp__ui--hidden">

	            <div class="pswp__top-bar">

	                <!--  Controls are self-explanatory. Order can be changed. -->

	                <div class="pswp__counter"></div>

	                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

	                <button class="pswp__button pswp__button--share" title="Share"></button>

	                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

	                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

	                <!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
	                <!-- element will get class pswp__preloader--active when preloader is running -->
	                <div class="pswp__preloader">
	                    <div class="pswp__preloader__icn">
	                      <div class="pswp__preloader__cut">
	                        <div class="pswp__preloader__donut"></div>
	                      </div>
	                    </div>
	                </div>
	            </div>

	            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
	                <div class="pswp__share-tooltip"></div>
	            </div>

	            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
	            </button>

	            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
	            </button>

	            <div class="pswp__caption">
	                <div class="pswp__caption__center"></div>
	            </div>

	          </div>

	        </div>

		</div>
	<!-- ############ PHOTO SWIPE END-->

	<!-- build:js scripts/app.min.js -->
	<!-- Bootstrap -->
	<script src="{libs_path}popper.js/dist/umd/popper.min.js"></script>
	<script src="{libs_path}bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="{libs_path}select2/dist/js/select2.full.min.js"></script>
	<!-- core -->
	<script src="{libs_path}pace-progress/pace.min.js"></script>
	<script src="{libs_path}pjax/pjax.js"></script>

	<script src="{scripts_path}lazyload.config.js"></script>
	<script src="{scripts_path}lazyload.js"></script>
	<script src="{scripts_path}plugin.js"></script>
	<script src="{scripts_path}nav.js"></script>
	<script src="{scripts_path}scrollto.js"></script>
	<script src="{scripts_path}toggleclass.js"></script>
	<script src="{scripts_path}theme.js"></script>
	<script src="{scripts_path}ajax.js"></script>
	<script src="{scripts_path}app.js"></script>
	<script src="{scripts_path}sweetalert2.js"></script>
	<script src="{cropperJs}cropper.js"></script>
	<script src="{cropperJs}main.js"></script>
	<script src="{cropperJs}mains.js"></script>
	<!-- endbuild -->

	<script type="text/javascript">

		var openPhotoSwipe = function(url) {
			var pswpElement = document.querySelectorAll('.pswp')[0];

			// build items array
			var items = [
					{
							src: url,
							w: 964,
							h: 1024
					}
			];

			// define options (if needed)
			var options = {
				  // history & focus options are disabled on CodePen
					history: false,
					focus: false,

					showAnimationDuration: 0,
					hideAnimationDuration: 0

			};

			var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
			gallery.init();
		};
	</script>
</body>

</html>
