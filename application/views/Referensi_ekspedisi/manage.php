<?php 
	$provinsi = get_by_field('idanggota', $this->session->userdata('idanggota'), 'manggota')->provinsi;
	$kota = get_by_field('idanggota', $this->session->userdata('idanggota'), 'manggota')->kota;
	$query = 	"SELECT
					mekspedisi.idekspedisi,
					mekspedisi.namaekspedisi,
					t1.idanggota,
					( CASE WHEN t1.idekspedisi != 'E1801000' THEN 'disabled' WHEN t1.idanggota IS NULL THEN '' END ) AS STATUS 
				FROM
					mekspedisi
					LEFT JOIN 
						(
							SELECT 
								idekspedisi,
								idanggota
							FROM
								manggotaekspedisi
								WHERE idanggota = '".$this->session->userdata('idanggota')."'
						) As t1
					ON
						(t1.idekspedisi = mekspedisi.idekspedisi)
				WHERE
					mekspedisi.staktif = '1'";
	$ekspedisi = get_query($query);
?>

<div class="padding">
	<?php echo ErrorSuccess($this->session)?>
	<?php if($error != '') echo ErrorMessage($error)?>
	<div class="box">
		<div class="box-header d-flex">
			<h3>{title}</h3>
		</div>
		<?= form_open('referensi_ekspedisi/save');?>
			<div style="padding-top:0px;padding:1rem">
				<div class="row">
					<div class="col-12">

						<div class="form-group row">
							<label for="" class="col-md-3 col-form-label">Ekspedisi</label>
							<div class="col-md-9">
								<select class="form-control" id="idekspedisi" name="idekspedisi" data-plugin="select2" data-option="{}">
									<option>Pilih Opsi</option>
									<?php
										foreach($ekspedisi as $r):
									?>
										<option value="<?= $r->idekspedisi?>" <?= $r->STATUS?>><?= $r->namaekspedisi?></option>
										<?php
										endforeach;
									?>
								</select>
							</div>
						</div>
						<input type="hidden" name="namaekspedisi" id="namaekspedisi">

					</div>
					<div class="col-12">

						<div class="table-responsive" style="display:none;">
							<table id="datatable-kulcimart" class="table" border="3" data-plugin="dataTable">
								<tr>
									<td style="width:190px;">
										<select class="form-control" name="provinsi" id="provinsi" data-plugin="select2" data-option="{}">
											<option value="null" disabled selected>Provinsi</option>
											<?php foreach(get_all('mprovinsi') as $r):?>
											<option value="<?= $r->id?>" <?=($r->id == $provinsi ? 'selected' : '')?>>
												<?= $r->nama?>
											</option>
											<?php endforeach;?>
										</select>
									</td>
									<td style="width:135px;">
										<select class="form-control" name="kota" id="kota" data-plugin="select2" data-option="{}">
											<option value="null" disabled selected>Kota</option>
											<?php foreach(get_all('mkota',array('provinsiid' => $provinsi)) as $r):?>
											<option value="<?= $r->id?>" <?=($r->id == $kota ? 'selected' : '')?>>
												<?= $r->nama?>
											</option>
											<?php endforeach;?>
										</select>
									</td>
									<td style="width:185px;">
										<select class="form-control" name="kecamatan" id="kecamatan" data-plugin="select2" data-option="{}">
											<option value="null" disabled selected>Kecamatan</option>
											<?php foreach(get_all('mkecamatan',array('kotaid' => $kota)) as $r):?>
											<option value="<?= $r->id?>">
												<?= $r->nama?>
											</option>
											<?php endforeach;?>
										</select>
									</td>
									<td style="width:130px;">
										<select class="form-control" name="desa" id="desa" data-plugin="select2" data-option="{}">
											<option value="null" disabled selected>Desa</option>
										</select>
									</td>
									<td style="">
										<input type="text" name="harga" id="harga" class="form-control number" placeholder="Harga" value="0">
									</td>
									<td>
										<button type="button" id="tambah_ekspedisi" class="btn btn-sm btn-success" style="width:180px;">Tambah</button>
									</td>
								</tr>

								<tr class="active" style="text-align:center;">

									<th style="width:190px;">Provinsi</th>
									<th style="width:135px;">Kota</th>
									<th>Kecamatan</th>
									<th>Desa</th>
									<th colspan="2">Harga</th>
								</tr>

								<tbody id="ekspedisi_detail">
								</tbody>

							</table>
						</div>

						<input type="hidden" id="detaildata" name="detaildata">

					</div>
				</div>

				<hr>
				<div class="row">
					<div class="col-8"></div>
					<div class="col-2">
						<button type="submit" class="btn btn-sm btn-primary" style="width: 100%">
							<i class="fa fa-pencil"></i> Simpan</button>
					</div>
					<?= form_close();?>
						<div class="col-2">
							<a href="{site_url}referensi_ekspedisi">
								<button type="button" class="btn btn-sm btn-danger" style="width: 100%">
									<i class="fa fa-close"></i> Batal</button>
							</a>
						</div>
				</div>

			</div>
	</div>
</div>

<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$('.number').number(true, 0);
	});

	function number_format(number, decimals, decPoint, thousandsSep) {
		number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
		var n = !isFinite(+number) ? 0 : +number
		var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
		var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
		var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
		var s = ''

		var toFixedFix = function (n, prec) {
			var k = Math.pow(10, prec)
			return '' + (Math.round(n * k) / k)
				.toFixed(prec)
		}

		s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
		if (s[0].length > 3) {
			s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
		}
		if ((s[1] || '').length < prec) {
			s[1] = s[1] || ''
			s[1] += new Array(prec - s[1].length + 1).join('0')
		}
		return s.join(dec)
	}

	function detaildata() {
		var detail_tbl = $('table tbody#ekspedisi_detail tr.details').get().map(function (row) {
			return $(row).find('td').get().map(function (cell) {
				return $(cell).html();
			});
		});
		$("#detaildata").val(JSON.stringify(detail_tbl));
	}
	detaildata();

	$(document).on('change', '#provinsi', function () { // Get Kota
		var provinsi = $('#provinsi').val();
		$.ajax({
			url: '{site_url}referensi_ekspedisi/getKota/' + provinsi,
			dataType: 'json',
			success: function (data) {
				$('#kota').empty();
				$('#kota').append('<option value="null" disabled selected>Kota</option>');
				for (var i = 0; i < data.length; i++) {
					$('#kota').append('<option value="' + data[i].id + '">' + data[i].nama + '</option>');
				}
			}
		});
	});

	$(document).on('change', '#kota', function () { // Get Kecamatan
		var kota = $('#kota').val();
		$.ajax({
			url: '{site_url}referensi_ekspedisi/getKecamatan/' + kota,
			dataType: 'json',
			success: function (data) {
				$('#kecamatan').empty();
				$('#kecamatan').append('<option value="null" disabled selected>Kecamatan</option>');
				for (var i = 0; i < data.length; i++) {
					$('#kecamatan').append('<option value="' + data[i].id + '">' + data[i].nama + '</option>');
				}
			}
		});
	});

	$(document).on('change', '#kecamatan', function () { // Get Desa
		var kecamatan = $('#kecamatan').val();
		$.ajax({
			url: '{site_url}referensi_ekspedisi/getDesa/' + kecamatan,
			dataType: 'json',
			success: function (data) {
				$('#desa').empty();
				$('#desa').append('<option value="null" disabled selected>Desa</option>');
				for (var i = 0; i < data.length; i++) {
					$('#desa').append('<option value="' + data[i].id + '">' + data[i].nama + '</option>');
				}
			}
		});
	});

	$(document).on('click', '#tambah_ekspedisi', function () {
		var provinsi = $('#provinsi option:selected').val();
		var kota = $('#kota option:selected').val();
		var kecamatan = $('#kecamatan option:selected').val();
		var desa = $('#desa option:selected').val();

		if ($('#harga').val() == '0') {
			swal({
				title: "Peringatan!",
				text: "Pastikan Kolom Harga Sudah Terisi !",
				type: "warning",
				timer: 1500,
				showConfirmButton: false
			});
		} else {
			if (provinsi != 'null' && kota != 'null' && kecamatan != 'null' && desa != 'null') {
				var tabel = '<tr class="details">';
				tabel += '<td>' + $('#provinsi option:selected').text() + '</td>'; //0
				tabel += '<td>' + $('#kota option:selected').text() + '</td>'; //1
				tabel += '<td>' + $('#kecamatan option:selected').text() + '</td>'; // 2
				tabel += '<td>' + $('#desa option:selected').text() + '</td>'; // 3
				tabel += '<td colspan="2">' + number_format($('#harga').val()) + '</td>'; // 4
				tabel += '<td hidden>' + $('#provinsi option:selected').val() + '</td>'; // 5
				tabel += '<td hidden>' + $('#kota option:selected').val() + '</td>'; // 6
				tabel += '<td hidden>' + $('#kecamatan option:selected').val() + '</td>'; // 7
				tabel += '<td hidden>' + $('#desa option:selected').val() + '</td>'; // 8
				tabel += '</tr>';
				$('table tbody#ekspedisi_detail').append(tabel);
				detaildata();
			} else {
				$.ajax({
					url: '{site_url}referensi_ekspedisi/getReff/',
					method: 'POST',
					dataType: 'json',
					data: {
						provinsi: provinsi,
						kota: kota,
						kecamatan: kecamatan,
						desa: desa
					},
					success: function (data) {
						$('table tbody#ekspedisi_detail').empty();
						for (var i = 0; i < data.length; i++) {
							if (provinsi != 'null' && kota == 'null' && kecamatan == 'null' && desa == 'null') {
								var tabel = '<tr class="details">';
								tabel += '<td>' + $('#provinsi option:selected').text() + '</td>'; //0
								tabel += '<td>' + data[i].namakota + '</td>'; //1
								tabel += '<td>' + data[i].namakecamatan + '</td>'; // 2
								tabel += '<td>' + data[i].namadesa + '</td>'; // 3
								tabel += '<td colspan="2">' + number_format($('#harga').val()) + '</td>'; // 4
								tabel += '<td hidden>' + $('#provinsi option:selected').val() + '</td>'; // 5
								tabel += '<td hidden>' + data[i].idkota + '</td>'; // 6
								tabel += '<td hidden>' + data[i].idkecamatan + '</td>'; // 7
								tabel += '<td hidden>' + data[i].iddesa + '</td>'; // 8
								tabel += '</tr>';
								$('table tbody#ekspedisi_detail').append(tabel);
								detaildata();
							} else if (provinsi != 'null' && kota != 'null' && kecamatan == 'null' && desa == 'null') {
								var tabel = '<tr class="details">';
								tabel += '<td>' + $('#provinsi option:selected').text() + '</td>'; //0
								tabel += '<td>' + $('#kota option:selected').text() + '</td>'; //1
								tabel += '<td>' + data[i].namakecamatan + '</td>'; // 2
								tabel += '<td>' + data[i].namadesa + '</td>'; // 3
								tabel += '<td colspan="2">' + number_format($('#harga').val()) + '</td>'; // 4
								tabel += '<td hidden>' + $('#provinsi option:selected').val() + '</td>'; // 5
								tabel += '<td hidden>' + $('#kota option:selected').val() + '</td>'; // 6
								tabel += '<td hidden>' + data[i].idkecamatan + '</td>'; // 7
								tabel += '<td hidden>' + data[i].iddesa + '</td>'; // 8
								tabel += '</tr>';
								$('table tbody#ekspedisi_detail').append(tabel);
								detaildata();
							} else if (provinsi != 'null' && kota != 'null' && kecamatan != 'null' && desa == 'null') {
								var tabel = '<tr class="details">';
								tabel += '<td>' + $('#provinsi option:selected').text() + '</td>'; //0
								tabel += '<td>' + $('#kota option:selected').text() + '</td>'; //1
								tabel += '<td>' + $('#kecamatan option:selected').text() + '</td>'; // 2
								tabel += '<td>' + data[i].nama + '</td>'; // 3
								tabel += '<td colspan="2">' + number_format($('#harga').val()) + '</td>'; // 4
								tabel += '<td hidden>' + $('#provinsi option:selected').val() + '</td>'; // 5
								tabel += '<td hidden>' + $('#kota option:selected').val() + '</td>'; // 6
								tabel += '<td hidden>' + $('#kecamatan option:selected').val() + '</td>'; // 7
								tabel += '<td hidden>' + data[i].id + '</td>'; // 8
								tabel += '</tr>';
								$('table tbody#ekspedisi_detail').append(tabel);
								detaildata();
							}
						}
					}
				});
			}

		}
	});

	$(document).on('change', '#idekspedisi', function () {
		$('#namaekspedisi').val($('#idekspedisi option:selected').text());
		if ($('#idekspedisi option:selected').text() == 'Ekspedisi Pribadi') {
			$('.table-responsive').css('display', 'block');
		} else {
			$('.table-responsive').css('display', 'none');
		}
	});


</script>
