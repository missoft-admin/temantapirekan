<div class="padding">
	<?php echo ErrorSuccess($this->session)?>
	<?php if($error != '') echo ErrorMessage($error)?>
	<div class="box"> 
		<div class="box-header d-flex"> 
			<h3>{title}</h3>
		</div> 
		<div style="padding-top:0px;padding:1rem">
			<a href="{site_url}referensi_ekspedisi/tambah" style="float: right;font-size: 25px;"><i class="fa fa-plus-circle"></i></a>
			<div class="table-responsive"> 
				<table id="datatable-kulcimarts" class="table v-middle p-0 m-0 box" data-plugin="dataTable"> 
					<thead>
						<tr style="text-align:center;">
							<th colspan="6">EKSPEDISI NON PRIBADI</th>
						</tr>
						<tr> 
							<th>No</th>  
							<th>Nama Ekspedisi</th> 
						</tr> 
					</thead> 
					<tbody> 
						<?php 
							$no = 1;
							foreach(get_all('manggotaekspedisi', array('idanggota' => $this->session->userdata('idanggota'),'namaekspedisi !=' => 'Ekspedisi Pribadi')) as $r):
						?>
							<tr>
								<td><?= $no++;?></td>
								<td><?= $r->namaekspedisi?></td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table> 
			</div>
			<hr style="margin-bottom:0px;">
			<hr style="margin-top:2px;margin-bottom:0px;">
			<hr style="margin-top:2px;">
			<div class="table-responsive"> 
				<table id="datatable-kulcimart" class="table v-middle p-0 m-0 box" data-plugin="dataTable"> 
					<thead>
						<tr style="text-align:center;">
							<th colspan="6">EKSPEDISI PRIBADI</th>
						</tr>
						<tr> 
							<th>No</th>  
							<th>Provinsi</th> 
							<th>Kota</th> 
							<th>Kecamatan</th> 
							<th>Desa</th> 
							<th>Harga Kirim</th>
							<th>Aksi</th>
							<th hidden>idprovinsi</th>
							<th hidden>idkota</th>
							<th hidden>idkecamatan</th>
							<th hidden>iddesa</th>
						</tr> 
					</thead> 
					<tbody> 
						<?php 
							$number = 1;
							foreach($list_index as $r):
						?>
							<tr>
								<td><?= $number++;?></td>
								<td><?= $r->nama_provinsi?></td>
								<td><?= $r->nama_kota?></td>
								<td><?= $r->nama_kecamatan?></td>
								<td><?= $r->nama_desa?></td>
								<td><?= number_format($r->harga)?></td>
								<td><a href="#" class="edit" style="font-size:17px;"><i class="fa fa-pencil-square"></i></a>&nbsp;&nbsp;
								<a href="{site_url}referensi_ekspedisi/hapus/<?= $r->idprovinsi?>/<?= $r->idkota?>/<?= $r->idkecamatan?>/<?= $r->iddesa?>" style="font-size:17px;" onclick="return confirm('Yakin Menghapus Data?')"><i class="fa fa-trash-o"></i></a></td>
								<td hidden><?= $r->idprovinsi?></td>
								<td hidden><?= $r->idkota?></td>
								<td hidden><?= $r->idkecamatan?></td>
								<td hidden><?= $r->iddesa?></td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table> 
			</div>
		</div> 
	</div> 
</div>

<script type="text/javascript"> 
	$('#datatable-kulcimart').dataTable();
	$('#datatable-kulcimarts').dataTable();
</script>

<script>
	
	$(document).on('click','.edit',function(){
		var harga = $(this).closest('tr').find("td:eq(5)").html();
		$(this).closest('tr').find("td:eq(5)").html('<input type="text" class="form-control hargaBaru" value="'+harga+'">');
		$(this).closest('tr').find("td:eq(6)").html('<a href="#" class="edits" style="font-size:17px;"><i class="fa fa-check"></i></a>&nbsp;&nbsp;<a href="#" class="batal" style="font-size:17px;"><i class="fa fa-close"></i></a>');
	});

	$(document).on('click','.edits', function(){
		var hargaBaru = $('.hargaBaru').val();
		var idprovinsi = $(this).closest('tr').find("td:eq(7)").html();
		var idkota = $(this).closest('tr').find("td:eq(8)").html();
		var idkecamatan = $(this).closest('tr').find("td:eq(9)").html();
		var iddesa = $(this).closest('tr').find("td:eq(10)").html();

		$.ajax({
			url:'{site_url}referensi_ekspedisi/updateHarga',
			method:'POST',
			data:{hargaBaru:hargaBaru, idprovinsi:idprovinsi, idkota:idkota, idkecamatan:idkecamatan, iddesa:iddesa},
			success:function(data){
				location.reload();
			}
		});
		// alert(hargaBaru+idprovinsi+idkota+idkecamatan+iddesa);

	});

	$(document).on('click', '.batal', function(){
		location.reload();
	});
</script>