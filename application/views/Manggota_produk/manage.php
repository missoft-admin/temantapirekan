<?php
	$jenisAnggota = get_by_field('idanggota',$idanggota,'manggota')->jenisanggota;
?>

<div class="padding">
	<?php echo ErrorSuccess($this->session)?>
	<?php if($error != '') echo ErrorMessage($error)?>
	<div class="box">
		<div class="box-header d-flex">
			<h3>{title}</h3>
		</div>
		<?= form_open_multipart('manggota_produk/save');?>
			<input type="hidden" id="fpmemberold-hidden" name="fpmemberold-hidden" value="<?=($foto!=='')?'{foto}':'0' ?>">
			<input type="hidden" name="datauptipe" value="0" />
			<input type="hidden" name="upfotoga" id="upfotoga" value="0" />
			<input type="hidden" id="resheight" name="resheight" />
			<input type="hidden" id="reswidth" name="reswidth" />
			<input type="hidden" id="x_axis" name="x_axis" />
			<input type="hidden" id="y_axis" name="y_axis" />

			<div style="padding-top:0px;padding:1rem">
				<div class="row">
					<div class="col-6">
						<div class="form-group row">
							<label for="inputEmail3" class="col-md-3 col-form-label">ID PRODUK</label>
							<div class="col-md-9">
								<input type="text" class="form-control" name="idproduk" value="<?= $idproduk?>" readonly>
							</div>
						</div>
						<div class="form-group row">
							<label for="inputEmail3" class="col-md-3 col-form-label">NAMA PRODUK</label>
							<div class="col-md-9">
								<input type="text" class="form-control" name="namaproduk" value="<?= $namaProduk?>" required>
							</div>
						</div>
						<div class="form-group row">
							<label for="" class="col-md-3 col-form-label">TIPE PRODUK</label>
							<div class="col-md-9">
								<select class="form-control" name="tipeproduk" data-plugin="select2" data-option="{}">
									<option>Pilih Opsi</option>
									<?php
												foreach(get_all('mkategoriproduk',array('stkhusus' => '0', 'staktif' => '1')) as $r):
											?>
										<option value="<?= $r->idkategoriproduk?>" <?=($r->idkategoriproduk == $kategoriProduk ? 'selected' : '')?>>
											<?= $r->namakategori?>
										</option>
										<?php endforeach;?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="inputEmail3" class="col-md-3 col-form-label">DESKRIPSI</label>
							<div class="col-md-9">
								<textarea class="form-control" name="deskripsiproduk" required>
									<?= $deskripsiProduk?>
								</textarea>
							</div>
						</div>
						<div class="form-group row">
							<label for="inputEmail3" class="col-md-3 col-form-label">GAMBAR PRODUK</label>
							<div class="col-md-9">
								<button type="button" class="btn white" data-toggle="modal" data-target="#m-md">Unggah Foto</button>
								<?php if($foto == ''){?>
									<div style="cursor:pointer;height: 360px;max-width:360px;width:360px; max-height:360px;border: 1px solid #ddd;" id="image_preview">
										<img style="margin: auto;" id="imgke-1" src="{custom_path}gif/ajax-loader.gif">
										<img onerror="imgError(this);" hidden="hidden" style="cursor:pointer;margin: auto;max-width:360px; max-height:360px;" id="previewing"
										src="{custom_path}gif/ajax-loader.gif" class="lazyIMG" data-src="" />
									</div>
								<?php }else{?>
									<div style="cursor:pointer;height: 360px;max-width:360px;width:360px; max-height:360px;border: 1px solid #ddd;" id="image_preview">
										<img style="margin: auto;height:360px;width:360px;" id="imgke-1" src="{gambarProduk}besar/<?= $foto?>">
										<img onerror="imgError(this);" hidden="hidden" style="cursor:pointer;margin: auto;max-width:360px; max-height:360px;height:360px;width:360px;" id="previewing"
										src="{gambarProduk}besar/<?= $foto?>" class="lazyIMG" data-src="" />
									</div>
								<?php }?>

							</div>
						</div>
						<input type="hidden" name="sttampil" value="<?= $sttampil?>">
						<input type="hidden" name="keuntungan" id="keuntungan" value="">

					</div>
					<div class="col-6">

						<div class="form-group row">
							<label for="inputEmail3" class="col-md-3 col-form-label">BERAT PRODUK</label>
							<div class="col-md-9">
								<input type="text" class="form-control number" name="beratpackproduk" value="<?= $beratProduk?>" placeholder="Gram" required>
							</div>
						</div>
						<div class="form-group row">
							<label for="inputEmail3" class="col-md-3 col-form-label">HARGA JUAL</label>
							<div class="col-md-9">
								<input type="text" class="form-control number" name="hargaproduk" id="hargaproduk" value="<?= $hargaProduk?>">
							</div>
						</div>
						<div class="form-group row">
							<label for="inputEmail3" class="col-md-3 col-form-label">HARGA JUAL HOTDEALS</label>
							<div class="col-md-9">
								<input type="text" class="form-control number" value="{hargahotdeals}" name="hargahotdeals" id="hargahotdeals">
							</div>
						</div>

						<!-- MODAL -->
						<div id="m-md" class="modal fade black-overlay" data-backdrop="false">
							<div class="modal-dialog animate modal-lg" id="animate" data-class="fade-down">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title">Upload Foto Produk</h5>
									</div>
									<div class="modal-body text-center p-lg">
										<div class="row">
											<div class="col-md-12">
												<!-- <h3>Demo:</h3> -->
												<div class="img-container">
													<img id="image" hidden alt="Picture">
													<div id="nofoto">
														<h1 class="text-mute" style="font-weight: bold;font-size: 140px;">&nbsp;</h1>
														<h1 class="text-mute" style="font-weight: bold;font-size: 112px;">1 : 1</h1>
														<h1 class="text-mute" style="font-weight: bold;font-size: 140px;">&nbsp;</h1>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="modal-footer">

										<div class="col-md-12 docs-buttons">
											<div class="btn-group">
												<button type="button" class="btn btn-primary" data-method="zoom" data-option="0.1" title="Zoom In">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
														<span class="fa fa-search-plus"></span> Perbesar
													</span>
												</button>
												<button type="button" class="btn btn-primary" data-method="zoom" data-option="-0.1" title="Zoom Out">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
														<span class="fa fa-search-minus"></span> Perkecil
													</span>
												</button>
											</div>

											<!-- <div class="btn-group">
												<button type="button" class="btn btn-primary" data-method="rotate" data-option="-45" title="Rotate Left">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
														<span class="fa fa-rotate-left"></span>
													</span>
												</button>
												<button type="button" class="btn btn-primary" data-method="rotate" data-option="45" title="Rotate Right">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
														<span class="fa fa-rotate-right"></span>
													</span>
												</button>
											</div> -->

											<!-- <div class="btn-group">
												<button type="button" class="btn btn-primary" data-method="scaleX" data-option="-1" title="Flip Horizontal">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
														<span class="fa fa-arrows-h"></span>
													</span>
												</button>
												<button type="button" class="btn btn-primary" data-method="scaleY" data-option="-1" title="Flip Vertical">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
														<span class="fa fa-arrows-v"></span>
													</span>
												</button>
											</div> -->

											<div class="btn-group">
												<label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">
													<input type="file" class="sr-only" id="inputImage" name="produk_file" accept="image/*">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="Pilih foto anda">
														<span class="fa fa-upload"></span> Upload
													</span>
												</label>
											</div>
											<div class="btn-group btn-group-crop">
												<button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Batal <i class="fa fa-close"></i></button>
												<button type="button" data-dismiss="modal" class="btn btn-success" id="btn-modalFoto" disabled data-method="getCroppedCanvas"
												data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
														Selesai <i class="fa fa-check"></i>
													</span>
												</button>
											</div>
										</div>
									</div>
								</div>
								<!-- /.modal-content -->
							</div>
						</div>
						<!-- END MODAL -->


					</div>
				</div>

				<hr>
				<div class="row">
					<div class="col-8"></div>
					<div class="col-2">
						<button type="submit" class="btn btn-sm btn-primary" style="width: 100%">
							<i class="fa fa-pencil"></i> Simpan</button>
					</div>
					<?= form_close();?>
						<div class="col-2">
							<a href="{site_url}manggota_produk">
								<button type="button" class="btn btn-sm btn-danger" style="width: 100%">
									<i class="fa fa-close"></i> Batal</button>
							</a>
						</div>
				</div>

			</div>
	</div>
</div>

<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script>
	$(document).on('keyup', '#hargaproduk', function(){
		var hargaproduk = parseFloat($('#hargaproduk').val());
		var jenisAnggota = '<?= $jenisAnggota?>';
		if(jenisAnggota == '1' && hargaproduk > 50000 ){
			swal({
				title: "Peringatan!",
				text: "Maksimal Harga Tidak Boleh Lebih Dari Rp. 50.000 !",
				type: "warning",
				timer: 1500,
				showConfirmButton: false
			});
			$('#hargaproduk').val(0);
		}else{
			$('#hargaproduk').val();
		}
	});

	$(document).on('keyup','#hargahotdeals',function(){
		var hargaproduk = parseFloat($('#hargaproduk').val());
		var hargahotdeals = parseFloat($('#hargahotdeals').val());
		if(hargahotdeals > hargaproduk){
			swal({
				title: "Peringatan!",
				text: "Harga Hot Deals Lebih Mahal Dari Harga Produk!.",
				type: "warning",
				timer: 1500,
				showConfirmButton: false
			});
			$('#hargahotdeals').val(0);
		}else{
			$('#hargahotdeals').val();
		}
	});
</script>
<script>
	$(document).ready(function () {
		$('.number').number(true, 0);
	});

	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#asd').html('<img id="blah" src="#" alt="your image" />')
				$('#blah')
					.attr('src', e.target.result)
					.width(370)
					.height(200);
			};

			reader.readAsDataURL(input.files[0]);
		}
	}

</script>
