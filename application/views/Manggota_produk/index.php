<?php
	$prodAktif = get_row('manggotaproduk',array('idanggota' => $idanggota, 'sttampil' => '1'));
	$jenisAnggota = get_by_field('idanggota',$idanggota,'manggota')->jenisanggota;
?>
<div class="padding">
	<?php echo ErrorSuccess($this->session)?>
	<?php if($error != '') echo ErrorMessage($error)?>
	<div class="box"> 
		<div class="box-header d-flex"> 
			<h3>{title}</h3>
		</div> 
		<div style="padding-top:0px;padding:1rem">
			<a href="{site_url}manggota_produk/tambah" style="float: right;font-size: 25px;"><i class="fa fa-plus-circle"></i></a>
			<div class="table-responsive"> 
				<table id="datatable-kulcimart" class="table v-middle p-0 m-0 box" data-plugin="dataTable"> 
					<thead> 
						<tr> 
							<th>No</th>  
							<th>Nama Produk</th> 
							<th>Tipe Produk</th> 
							<th>Gambar</th> 
							<th>Deskripsi</th> 
							<th>Harga</th> 
							<th>Status</th> 
							<th>Aksi</th> 
						</tr> 
					</thead> 
					<tbody> 
						<? $number=0; ?> 
						<? foreach($list_index as $r) : ?> 
						<? $number = $number + 1; ?>
						<tr> 
							<td><?= $number ?></td>  
							<td><?= $r->namaproduk ?></td> 
							<td><?= tipeProduk($r->kategoriproduk) ?></td> 
							<td><img src="{gambarProduk}/besar/<?= $r->foto?>" width="100px" height="100px;"></td> 
							<td><?= $r->deskripsiproduk ?></td>
							<td><?= produkHotDeals($r->sthotdeals,$r->sttampilhotdeals,$r->hargajualhotdeals,$r->hargajual)?></td>
							<td><?= statusDataProduk($r->sttampil) ?>&nbsp;<?= hotDeals($r->sthotdeals,$r->sttampilhotdeals,$r->hargajualhotdeals,$r->hargajual)?></td>  
							<td> 
								<div class="dropdown"> 
									<button class="btn white dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-gear"></i> </button> 
									<div class="dropdown-menu" x-placement="bottom-start">
										<a class="dropdown-item" href="{site_url}manggota_produk/edit/<?= $r->idproduk?>">Edit Informasi</a>
										<? if($r->sttampil == 1 && $r->sthotdeals == 0 && $r->sttampilhotdeals == 0) : ?>
											<a class="dropdown-item" href="{site_url}manggota_produk/nonaktifProduk/<?= $r->idproduk?>" onclick="return confirm('Anda yakin akan menonaktifkan produk ini ?');">Nonaktif Produk</a>
											<!-- <a class="dropdown-item" href="{site_url}manggota_produk/hotDealsPending/<?= $r->idproduk?>" onclick="return confirm('Anda yakin akan mempending produk Hot Deals ?');">Pending HotDeals</a> -->
											<a class="dropdown-item" href="{site_url}manggota_produk/hotDeals/<?= $r->idproduk?>" onclick="return confirm('Anda yakin akan menambahkan produk Hot Deals ?');">Aktif HotDeals</a>
											<!-- <a class="dropdown-item" href="{site_url}manggota_produk/hotDealsNonaktif/<?= $r->idproduk?>" onclick="return confirm('Anda yakin akan Menonaktifkan produk Hot Deals ?');">Nonaktif HotDeals</a> -->
										<? elseif($r->sttampil == 1 && $r->sthotdeals == 1 && $r->sttampilhotdeals == 1) : ?>
											<a class="dropdown-item" href="{site_url}manggota_produk/nonaktifProduk/<?= $r->idproduk?>" onclick="return confirm('Anda yakin akan menonaktifkan produk ini ?');">Nonaktif Produk</a>
											<a class="dropdown-item" href="{site_url}manggota_produk/hotDealsPending/<?= $r->idproduk?>" onclick="return confirm('Anda yakin akan mempending produk Hot Deals ?');">Pending HotDeals</a>
											<!-- <a class="dropdown-item" href="{site_url}manggota_produk/hotDeals/<?= $r->idproduk?>" onclick="return confirm('Anda yakin akan menambahkan produk Hot Deals ?');">Aktif HotDeals</a> -->
											<a class="dropdown-item" href="{site_url}manggota_produk/hotDealsNonaktif/<?= $r->idproduk?>" onclick="return confirm('Anda yakin akan Menonaktifkan produk Hot Deals ?');">Nonaktif HotDeals</a>
										<? elseif($r->sttampil == 1 && $r->sthotdeals == 1 && $r->sttampilhotdeals == 0) : ?>
											<a class="dropdown-item" href="{site_url}manggota_produk/nonaktifProduk/<?= $r->idproduk?>" onclick="return confirm('Anda yakin akan menonaktifkan produk ini ?');">Nonaktif Produk</a>
											<!-- <a class="dropdown-item" href="{site_url}manggota_produk/hotDealsPending/<?= $r->idproduk?>" onclick="return confirm('Anda yakin akan mempending produk Hot Deals ?');">Pending HotDeals</a> -->
											<a class="dropdown-item" href="{site_url}manggota_produk/hotDeals/<?= $r->idproduk?>" onclick="return confirm('Anda yakin akan menambahkan produk Hot Deals ?');">Aktif HotDeals</a>
											<!-- <a class="dropdown-item" href="{site_url}manggota_produk/hotDealsNonaktif/<?= $r->idproduk?>" onclick="return confirm('Anda yakin akan Menonaktifkan produk Hot Deals ?');">Nonaktif HotDeals</a> -->
										<? elseif($r->stblokir == 1):?>
										<? else: ?>
											<?= validasiProdukIndex($jenisAnggota,$prodAktif,$r->idproduk)?>
											<!-- <a class="dropdown-item" href="{site_url}manggota_produk/aktifProduk/<?= $r->idproduk?>" onclick="return confirm('Anda yakin akan mengaktifkan produk ini ?');">Aktifkan</a> -->
										<? endif ?>
									</div> 
								</div>
							</td> 
						</tr> 
						<? endforeach ?>
					</tbody> 
				</table> 
			</div> 
		</div> 
	</div> 
</div>

<script type="text/javascript"> 
	$('#datatable-kulcimart').dataTable();
</script>
<script>
	$(document).on('click','#gagalAktif',function(){
		var ak = '<?= $prodAktif?>'
		swal({
			title: "Peringatan!",
			text: "Maksimal Produk Aktif Hanya "+ak+" !",
			type: "warning",
			timer: 1500,
			showConfirmButton: false
		});
	});
</script>