<?php $tipePartner = get_by_field('idanggota', $this->session->userdata('idanggota'),'manggota')->tipepartner;?>
<ul class="nav">
	<li class="nav-header" style="color:#ffffff;background-color:#315fae;">
		<span class="text-xs hidden-folded" style="font-size:14px;opacity:10;color:#fdab29;"><b>
        <i class="fa fa-home"></i>&nbsp;&nbsp;MENU UTAMA</b></span>
	</li>
    <li <?=menuIsActive('dashboard')?>>
      <a href="{base_url}dashboard">
        <span class="nav-icon">
          <i class="fa fa-bar-chart"></i>
        </span>
        <span class="nav-text">Dashboard</span>
      </a>
    </li>

    <li <?=menuIsActive('mpartner/edit_informasi')?>>
      <a href="{base_url}mpartner/edit_informasi/<?= $this->session->userdata('idanggota')?>">
        <span class="nav-icon">
          <i class="fa fa-user"></i>
        </span>
        <span class="nav-text">Edit Profil</span>
      </a>
    </li>

    <li <?=menuIsActive('manggota/rekening')?>>
      <a href="{base_url}manggota/rekening">
        <span class="nav-icon">
          <i class="fa fa-credit-card"></i>
        </span>
        <span class="nav-text">Edit Rekening</span>
      </a>
    </li>

    <li <?=menuIsActive('manggota/peta_jaringan')?>>
      <a href="{base_url}manggota/peta_jaringan/0">
        <span class="nav-icon">
          <i class="fa fa-line-chart"></i>
        </span>
        <span class="nav-text">Anggota Rekomendasi</span>
      </a>
    </li>

    <li <?=menuIsActive('referensi_ekspedisi')?>>
      <a href="{base_url}referensi_ekspedisi">
        <span class="nav-icon">
          <i class="fa fa-dropbox"></i>
        </span>
        <span class="nav-text">Referensi Ekpedisi</span>
      </a>
    </li>

      <li <?=menuIsActive('manggota_produk')?>>
        <a href="{base_url}manggota_produk">
          <span class="nav-icon">
            <i class="fa fa-tasks"></i>
          </span>
          <span class="nav-text">Data Produk</span>
        </a>
      </li>
      
      <!-- <li <?=menuIsActive('BayarRegistrasi_partner/riwayat_registrasi')?>>
        <a href="{base_url}BayarRegistrasi_partner/riwayat_registrasi">
          <span class="nav-icon">
            <i class="fa fa-history"></i>
          </span>
          <span class="nav-text">Riwayat Registrasi</span>
        </a>
      </li> -->

  </ul>

  <ul class="nav">
  <li class="nav-header" style="color:#ffffff;background-color:#315fae;">
    <span class="text-xs hidden-folded" style="font-size:14px;opacity:10;color:#fdab29;"><b>
        <i class="fa fa-shopping-cart"></i>&nbsp;&nbsp;TRANSAKSI</b></span>
  </li>

  <!-- <li <?=menuIsActive('BayarRegistrasi_partner')?>>
    <a href="{base_url}BayarRegistrasi_partner">
      <span class="nav-icon">
        <i class="fa fa-credit-card"></i>
      </span>
      <span class="nav-text">Bayar Registrasi</span>
    </a>
  </li> -->

    <?php if($tipePartner == '1'){?>
      <li <?=menuIsActive('tjual_prosespacking')?>>
      <a href="{site_url}tjual_prosespacking">
        <span class="nav-icon">
          <i class="fa fa-gg-circle"></i>
        </span>
        <span class="nav-text">Proses Eksekusi / Packing</span>
      </a>
    </li>

    <li <?=menuIsActive('tjual_packingpengiriman')?>>
      <a href="{site_url}tjual_packingpengiriman">
        <span class="nav-icon">
          <i class="fa fa-truck"></i>
        </span>
        <span class="nav-text">Proses Pengiriman</span>
      </a>
    </li>

    <li <?=menuIsActive('tjual_rekapitulasi/index')?>>
      <a href="{site_url}tjual_rekapitulasi/index/0">
        <span class="nav-icon">
          <i class="fa fa-newspaper-o"></i>
        </span>
        <span class="nav-text">Rekap Proses Transaksi</span>
      </a>
    </li>
    <?php }elseif($tipePartner == '2'){?>
    <li <?=menuIsActive('tjual_prosesselesai')?>>
      <a href="{site_url}tjual_prosesselesai">
        <span class="nav-icon">
          <i class="fa fa-handshake-o"></i>
        </span>
        <span class="nav-text">Diambil Pembeli</span>
      </a>
    </li>

    <li <?=menuIsActive('tjual_rekapitulasi/index')?>>
      <a href="{site_url}tjual_rekapitulasi/index/0">
        <span class="nav-icon">
          <i class="fa fa-newspaper-o"></i>
        </span>
        <span class="nav-text">Rekap Proses Transaksi</span>
      </a>
    </li>
    <?php }elseif($tipePartner == '3'){?>
      <li <?=menuIsActive('tjual_prosespacking')?>>
      <a href="{site_url}tjual_prosespacking">
        <span class="nav-icon">
          <i class="fa fa-gg-circle"></i>
        </span>
        <span class="nav-text">Proses Eksekusi / Packing</span>
      </a>
    </li>

    <li <?=menuIsActive('tjual_packingpengiriman')?>>
      <a href="{site_url}tjual_packingpengiriman">
        <span class="nav-icon">
          <i class="fa fa-truck"></i>
        </span>
        <span class="nav-text">Proses Pengiriman</span>
      </a>
    </li>

    <li <?=menuIsActive('tjual_prosesselesai')?>>
      <a href="{site_url}tjual_prosesselesai">
        <span class="nav-icon">
          <i class="fa fa-handshake-o"></i>
        </span>
        <span class="nav-text">Diambil Pembeli</span>
      </a>
    </li>

    <li <?=menuIsActive('tjual_rekapitulasi/index')?>>
      <a href="{site_url}tjual_rekapitulasi/index/0">
        <span class="nav-icon">
          <i class="fa fa-newspaper-o"></i>
        </span>
        <span class="nav-text">Rekap Proses Transaksi</span>
      </a>
    </li>
    <?php }else{echo '';}?>

    <!-- <li <?=menuIsActive('')?>>
      <a href="#">
        <span class="nav-icon">
          <i class="fa fa-buysellads"></i>
        </span>
        <span class="nav-text">Permintaan Iklan</span>
      </a>
    </li> -->
  
  </ul>

  <!-- Penjualan -->

  <ul class="nav">
  <li class="nav-header" style="color:#ffffff;background-color:#315fae;">
    <span class="text-xs hidden-folded" style="font-size:14px;opacity:10;color:#fdab29;"><b>
        <i class="fa fa-dollar"></i>&nbsp;&nbsp;PELAPORAN</b></span>
  </li>

  <li <?=menuIsActive('Pendapatan/laporan')?>>
    <a href="{base_url}Pendapatan/laporan/<?= date('Ym')?>">
        <span class="nav-icon">
          <i class="fa fa-file"></i>
        </span>
        <span class="nav-text">Laporan Penjualan</span>
      </a>
  </li>

  <li <?=menuIsActive('Pendapatan/sponsor')?>>
    <a href="{base_url}Pendapatan/sponsor">
        <span class="nav-icon">
          <i class="fa fa-tags"></i>
        </span>
        <span class="nav-text">Cashback Sponsor</span>
      </a>
  </li>

  <li <?=menuIsActive('Pendapatan/partnership1')?>>
    <a href="{base_url}Pendapatan/partnership1">
      <span class="nav-icon">
        <i class="fa fa-gift"></i>
      </span>
        <span class="nav-text">Cashback Royalti 1</span>
      </a>
  </li>

  <li <?=menuIsActive('Pendapatan/partnership2')?>>
    <a href="{base_url}Pendapatan/partnership2">
      <span class="nav-icon">
        <i class="fa fa-gift"></i>
      </span>
        <span class="nav-text">Cashback Royalti 2</span>
      </a>
  </li>

  <li <?=menuIsActive('Pendapatan/total')?>>
    <a href="{base_url}Pendapatan/total">
        <span class="nav-icon">
          <i class="fa fa-money"></i>
        </span>
        <span class="nav-text">Total Cashback</span>
      </a>
  </li>

</ul>

<ul class="nav">
  <li class="nav-header" style="color:#ffffff;background-color:#315fae;">
    <span class="text-xs hidden-folded" style="font-size:14px;opacity:10;color:#fdab29;"><b>
        <i class="fa fa-gear"></i>&nbsp;&nbsp;PENGATURAN</b></span>
  </li>

    <li <?=menuIsActive('GantiPassword')?>>
      <a href="{base_url}GantiPassword">
        <span class="nav-icon">
          <i class="fa fa-key"></i>
        </span>
        <span class="nav-text">Ganti Password</span>
      </a>
    </li>

    <?php if(get_by_field('idanggota',$this->session->userdata('idanggota'),'manggota')->staktif == '1'){ ?>
      <li <?=menuIsActive('manggota/tutupToko')?>>
        <a href="{base_url}manggota/tutupToko">
          <span class="nav-icon">
            <i class="fa fa-power-off"></i>
          </span>
          <span class="nav-text">Toko Tutup</span>
        </a>
      </li>
    <?php }else{?>
      <li <?=menuIsActive('manggota/bukaToko')?>>
        <a href="{base_url}manggota/bukaToko">
          <span class="nav-icon">
            <i class="fa fa-power-off"></i>
          </span>
          <span class="nav-text">Toko Buka</span>
        </a>
      </li>
    <?php } ?>

</ul>