<div class="padding">
	<?php echo ErrorSuccess($this->session)?>
	<?php if($error != '') echo ErrorMessage($error)?>
	<div class="box">
		<div class="box-header">
			<h2>{title}</h2>
		</div>
		<div class="box-divider m-0"></div>
		<div class="box-body">
			<?= form_open('mpartner/save_verifikasi_bayar_pendaftaran') ?>
				<table class="table table-striped white b-a">
					<thead> 
						<tr>  
							<th>DETAIL PEMBAYARAN</th>
							<th width="25%">&nbsp;</th> 
						</tr> 
					</thead>
					<tbody> 
						<tr> 
							<td>TANGGAL BAYAR</td> 
							<td>
								<?= form_input('tanggalbayar', '{tanggalbayar}', 'class="form-control" disabled') ?>
							</td> 
						</tr> 
						<tr>
							<td>NOMINAL</td>
							<td><?= form_input('nominaltransaksi', '{nominaltransaksi}', 'class="form-control" disabled') ?></td> 
						</tr>
						<tr>
							<td>NAMA BANK</td> 
							<td><?= form_input('namabank', '{namabank}', 'class="form-control" disabled') ?></td>
						</tr>
						<tr>
							<td>NOMOR REKENING</td> 
							<td><?= form_input('norekening', '{norekening}', 'class="form-control" disabled') ?></td>
						</tr>
						<tr>
							<td>ATAS NAMA</td>
							<td><?= form_input('atasnama', '{atasnama}', 'class="form-control" disabled') ?></td>
						</tr>
						<tr>
							<td>PERIODE AWAL</td> 
							<td><?= form_input('periodeawal', $periodeawal, 'class="form-control" readonly') ?></td> 
						</tr>
						<tr>
							<td>PERIODE AKHIR</td> 
							<td><?= form_input('periodeakhir', $periodeakhir, 'class="form-control" readonly') ?></td> 
						</tr>
					</tbody> 
				</table>
				<div class="text-right">
					<button type="submit" class="btn warning">Simpan</button>
					<?= anchor('mpartner/verifikasi_bayar_pendaftaran/{idanggotapartner}', 'Kembali', 'class="btn danger"') ?>
				</div>
				<?= form_hidden('idanggota', '{idanggotapartner}') ?>
			<?= form_close() ?>			
		</div>		
	</div>
</div>
