<div class="padding">
	<?php echo ErrorSuccess($this->session)?>
	<?php if($error != '') echo ErrorMessage($error)?>
	<div class="box">
		<div class="box-header">
			<h2>{title}</h2>
		</div>
		<div class="box-divider m-0"></div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-3">
					<div class="box p-1">
						<img src="{base_url}assets/files/bukti_transfer/{fotobuktitransfer}" alt="" class="w-100">
						<div class="p-2"> 
							<div class="text-ellipsis">Foto bukti transfer</div> 
						</div>
					</div>
				</div>
				<div class="col-md-9">
					<div class="form-group row">
						<div class="form-group col-md-6">
							<?= form_label('Tanggal Bayar') ?>
							<b class="form-control"><?= $tanggalbayar ?></b>
						</div>
						<div class="form-group col-md-6">
							<?= form_label('Nominal') ?>
							<b class="form-control"><?= numberIndo($nominaltransaksi) ?></b>
						</div>
						<div class="form-group col-md-6">
							<?= form_label('Nama Bank') ?>
							<b class="form-control"><?= $namabank ?></b>
						</div>
						<div class="form-group col-md-6">
							<?= form_label('Nomor Rekening') ?>
							<b class="form-control"><?= $norekening ?></b>
						</div>
						<div class="form-group col-md-6">
							<?= form_label('Atas Nama') ?>
							<b class="form-control"><?= $atasnama ?></b>
						</div>
						<div class="form-group col-md-6">
							<?= form_label('Untuk Periode') ?>
							<b class="form-control">
								<?= date('m/Y',strtotime($periodeawal)) ?> - <?= date('m/Y',strtotime($periodeakhir)) ?>
							</b>
						</div>
					</div>
				</div>
			</div>
			<div class="text-right">
				<?= anchor('mpartner/verifikasi_bayar_pendaftaran_decision/'.$idanggotapartner, 'Verifikasi', 'class="btn warning"') ?>
				<?= anchor('mpartner', 'Batal', 'class="btn danger"') ?>
			</div>
		</div>
	</div>
</div>
