        <div class="padding">
            <?php echo ErrorSuccess($this->session)?>
            <?php if($error != '') echo ErrorMessage($error)?>
            <div class="box">
                <div class="box-header">
                    <h2>{title}</h2>
                </div>
                <div class="box-divider m-0"></div>
                <div class="box-body">
                    <? ($sthotdeals == 0) ? $harga = $hargaproduk : $harga = $hargahotdeals ?>
                    <?= form_open_multipart('mpartner/save_produk_partner') ?>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="box p-1">
                                    <? $file = FCPATH.'assets/files/produk/'.$idanggota.'/'.$foto ?>
                                    <? if(is_file($file)): ?>
                                        <img src="{produk_path}{idanggota}/{foto}" alt="" class="w-100"> 
                                    <? else: ?>
                                        <img src="{produk_path}default.png" alt="" class="w-100">
                                    <? endif ?>
                                    <div class="form-file p-2"> 
                                        <input type="file" name="foto" value="{foto}"> 
                                        <button class="btn btn-block white">Select file ...</button> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <?= form_label('Nama Produk') ?>
                                        <?= form_input('namaproduk', '{namaproduk}', 'class="form-control"') ?>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <?= form_label('Deskripsi') ?>
                                        <div class="box">
                                            <textarea data-plugin="summernote" name="deskripsiproduk" data-option="{toolbar: [['style', ['bold', 'italic', 'underline', 'clear']],]}">{deskripsiproduk}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <?= form_label('Tipe Produk') ?>
                                    <?= dropdownTipeProduk($tipeproduk) ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <?= form_label('Berat (Gram)') ?>
                                    <?= form_input('beratpackproduk', '{beratpackproduk}', 'class="form-control"') ?>
                                </div>
                                <div class="form-group col-md-12"> 
                                    <label class="md-check"> 
                                        <?= form_checkbox('sthotdeals', 1, ($sthotdeals == 1) ? true : false, 'id="sthotdeals"') ?>
                                        <i class="blue"></i> Hotdeals 
                                    </label>
                                </div>
                            </div>
                            <div id="ftanggalhotdeals" style="display:none">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <?= form_label('Tanggal Awal') ?>
                                        <?= form_input('tanggalawalhotdeals', '{tanggalawalhotdeals}', 'class="form-control" placeholder="Tanggal Awal"') ?>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <?= form_label('Tanggal Akhir') ?>
                                        <?= form_input('tanggalakhirhotdeals', '{tanggalakhirhotdeals}', 'class="form-control" placeholder="Tanggal Akhir"') ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <?= form_label('Harga Produk') ?>
                                    <?= form_input('hargaproduk', $harga, 'class="form-control"') ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <?= form_label('Harga Jual') ?>
                                    <?= form_input('hargajual', getHargaJual($harga), 'class="form-control" readonly') ?>
                                </div>
                                <div class="form-group col-md-4">
                                    <?= form_label('Keuntungan') ?>
                                    <?= form_input('keuntungan', getKeuntungan($harga), 'class="form-control" readonly') ?>
                                </div>
                                <div class="form-group col-md-4">
                                    <?= form_label('Bonus Sponsor') ?>
                                    <?= form_input('bsponsor', getBSponsor($harga), 'class="form-control" readonly') ?>
                                </div>
                                <div class="form-group col-md-4">
                                    <?= form_label('Bonus Partner 1') ?>
                                    <?= form_input('bpartner1', getBPartner1($harga), 'class="form-control" readonly') ?>
                                </div>
                                <div class="form-group col-md-4">
                                    <?= form_label('Bonus Partner 2') ?>
                                    <?= form_input('bpartner2', getBPartner2($harga), 'class="form-control mb-3" readonly') ?>
                                </div>
                                <div class="form-group col-md-4">
                                    <?= form_label('Bonus Loyaliti') ?>
                                    <?= form_input('bloyaliti', getBLoyalti($harga), 'class="form-control mb-3" readonly') ?>
                                </div>
                                <div class="form-group col-md-4">
                                    <?= form_label('Pendapatan') ?>
                                    <?= form_input('pendapatan', getPendapatan($harga), 'class="form-control mb-3" readonly') ?>
                                </div>
                            </div>
                        </div>

                        <div class="text-right">
                            <button type="submit" class="btn warning">Simpan</button>
                            <?= anchor('mpartner/detail/{idanggota}?tab=produk', 'Batal', 'class="btn danger"') ?>
                        </div>
                        <?= form_hidden('idanggota', $idanggota) ?>
                        <?= form_hidden('idproduk', $idproduk) ?>
                    <?= form_close() ?>
                </div>
            </div>
        </div>

<script type="text/javascript">
    $(document).ready(function(){
        var sthotdeals = '{sthotdeals}';
        if(sthotdeals == 1) {
            $('#ftanggalhotdeals').css('display','block')
        } else {
            $('#ftanggalhotdeals').css('display','none')            
        }
    })

    $(document).on('change','#sthotdeals', function(){
        var checked = $(this).is(':checked');
        if(checked) {
            $('#ftanggalhotdeals').css('display','block')
            kalkulasi('{hargahotdeals}')
        } else {
            $('#ftanggalhotdeals').css('display','none')
            kalkulasi('{hargaproduk}')
        }
    })

    $(document).on('keyup','input[name=hargaproduk]', function(){
        harga = $(this).val()
        kalkulasi(harga)
    })

    function kalkulasi(harga) {
        $('input[name=hargaproduk]').val(harga)
        var hargaproduk = parseFloat( $('input[name=hargaproduk]').val() )
        $('input[name=hargajual]').val( getHargaJual(hargaproduk) )
        $('input[name=keuntungan]').val( getKeuntungan(hargaproduk) )
        $('input[name=bsponsor]').val( getBSponsor(hargaproduk) )
        $('input[name=bpartner1]').val( getBPartner1(hargaproduk) )
        $('input[name=bpartner2]').val( getBPartner2(hargaproduk) )
        $('input[name=bpartner2]').val( getBPartner2(hargaproduk) )
        $('input[name=bloyaliti]').val( getBLoyalti(hargaproduk) )
        $('input[name=pendapatan]').val( getPendapatan(hargaproduk) )
    }

    function getHargaJual($hargaProduk) {
        return $hargaProduk+((10/100)*$hargaProduk);
    }

    function getKeuntungan($hargaProduk) {
        return (10/100)*$hargaProduk;
    }

    function getPendapatan($hargaProduk) {
        $keuntungan = getKeuntungan($hargaProduk);
        return (45/100)*$keuntungan;
    }

    function getBSponsor($hargaProduk) {
        $keuntungan = getKeuntungan($hargaProduk);
        return (20/100)*$keuntungan;
    }

    function getBPartner1($hargaProduk) {
        $keuntungan = getKeuntungan($hargaProduk);
        return (12.5/100)*$keuntungan;    
    }

    function getBPartner2($hargaProduk) {
        $keuntungan = getKeuntungan($hargaProduk);
        return (7.5/100)*$keuntungan;    
    }

    function getBLoyalti($hargaProduk) {
        $keuntungan = getKeuntungan($hargaProduk);
        return (15/100)*$keuntungan;    
    }    
</script>

