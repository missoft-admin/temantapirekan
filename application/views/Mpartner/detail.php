<div class="content-main" id="content-main">
    <div class="">
        <div class="item">
            <div class="item-bg">
                <img src="{base_url}assets/files/anggota/dell.jpg" alt="." class="blur opacity-3">
            </div>
            <div class="p-4">
                <div class="row mt-3">
                    <div class="col-sm-7">
                        <div class="media">
                            <div class="media-body mx-3 mb-2">
                                <h4>{namaanggota}</h4>
                                <p class="text-muted">
                                    <span class="m-r">{alamat} {desa}</span> 
                                    <br>
                                    <small>{kecamatan}, {kota} {kodepos}</small>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="white bg b-b px-3">
            <div class="row">
                <div class="col-sm-6 order-sm-1">
                    <div class="py-4 clearfix nav-active-theme">
                        <ul class="nav nav-pills nav-sm">
                            <li class="nav-item">
                                <?= anchor('mpartner/detail/{idanggota}?tab=detail', 'Detail', 'class="nav-link active"') ?>
                            </li>
                            <li class="nav-item">
                                <?= anchor('mpartner/detail/{idanggota}?tab=produk', 'Produk', 'class="nav-link"') ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>