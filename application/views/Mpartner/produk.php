        <div class="padding">
            <?php echo ErrorSuccess($this->session)?>
            <?php if($error != '') echo ErrorMessage($error)?>
            <div class="box"> 
                <div class="box-header d-flex"> 
                    <h3>{title}</h3>
                </div> 
                <div style="padding-top:0px;padding:1rem"> 
                    <div class="table-responsive"> 
                        <table id="datatable-kulcimart" class="table v-middle p-0 m-0 box" data-plugin="dataTable"> 
                            <thead> 
                                <tr>
                                    <th>Foto</th> 
                                    <th>N. Produk</th>
                                    <th>Deskripsi</th>
                                    <th>Berat</th> 
                                    <th>Harga</th> 
                                    <th>H. Jual</th>
                                    <th>Aksi</th>
                                    <th hidden>St Hotdeals</th>  
                                </tr> 
                            </thead> 
                            <tbody> 
                                <? $number=0; ?> 
                                <? foreach($listProduk as $r) : ?> 
                                <? $number = $number + 1; ?>
                                <tr>
                                    <? if($r->sthotdeals == 1): ?>
                                        <td>                                            
                                            <span class="d-inline-block p-1 b-a">
                                                <? $file = FCPATH.'assets/files/produk/'.$r->idanggota.'/'.$r->foto ?>
                                                <? if(is_file($file)) : ?>
                                                    <img onClick="openPhotoSwipe('{produk_path}<?= $r->idanggota ?>/<?= $r->foto ?>')" src="{produk_path}<?= $r->idanggota ?>/<?= $r->foto ?>" width="100"> 
                                                <? else: ?>
                                                    <img onClick="openPhotoSwipe('{produk_path}/default.png')" src="{produk_path}/default.png" width="100">
                                                <? endif ?>
                                            </span>
                                        </td>
                                        <td><?= $r->namaproduk ?> <span class="badge badge-pill danger">Hotdeals</span></td>
                                        <td><?= strip_tags( substr($r->deskripsiproduk, 0, 25) ) ?> ... </td>
                                        <td><?= $r->beratpackproduk ?> g</td> 
                                        <td><?= numberIndo($r->hargahotdeals) ?></td>
                                        <td><?= numberIndo($r->hargajualhotdeals) ?></td>
                                    <? else: ?>
                                        <td>                                            
                                            <span class="d-inline-block p-1 b-a">
                                                <? $file = FCPATH.'assets/files/produk/'.$r->idanggota.'/'.$r->foto ?>
                                                <? if(is_file($file)) : ?>
                                                    <img onClick="openPhotoSwipe('{produk_path}<?= $r->idanggota ?>/<?= $r->foto ?>')" src="{produk_path}<?= $r->idanggota ?>/<?= $r->foto ?>" width="100"> 
                                                <? else: ?>
                                                    <img onClick="openPhotoSwipe('{produk_path}/default.png')" src="{produk_path}/default.png" width="100">
                                                <? endif ?>
                                            </span>
                                        </td>                                        
                                        <td><?= $r->namaproduk ?></td>
                                        <td><?= strip_tags( substr($r->deskripsiproduk, 0, 25) ) ?> ... </td>
                                        <td><?= $r->beratpackproduk ?> g</td> 
                                        <td><?= numberIndo($r->hargaproduk) ?></td>
                                        <td><?= numberIndo($r->hargajual) ?></td>
                                    <? endif ?>
                                    <td>
                                        <a href="{site_url}mpartner/detail/{idanggota}?tab=produk&amp;edit=<?=$r->idproduk?>" title="Edit"><i class="fa fa-pencil"></i></a>
                                    </td>
                                    <td hidden><?= $r->sthotdeals ?></td> 
                                </tr> 
                                <? endforeach ?>
                            </tbody> 
                        </table> 
                    </div> 
                </div> 
            </div> 
        </div>

        <script type="text/javascript"> 
            $('#datatable-kulcimart').dataTable({
                "order": [
                    [ 7, "desc" ],
                    [ 1, "asc" ],
                ],                      
            });
        </script>