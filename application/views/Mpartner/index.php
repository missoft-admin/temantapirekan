<div class="padding">
	<?php echo ErrorSuccess($this->session)?>
	<?php if($error != '') echo ErrorMessage($error)?>
	<div class="box">
		<div class="box-header d-flex">
			<h3>{title}</h3>
		</div>
		<div style="padding-top:0px;padding:1rem">
			<div class="table-responsive">
				<table id="datatable-kulcimart" class="table v-middle p-0 m-0 box" data-plugin="dataTable">
					<thead>
						<tr>
							<th>Nama</th>
							<th>Sponsor</th>
							<th>Alamat</th>
							<th>J Partner</th>
							<th>Status</th>
							<th>Aksi</th>
							<th hidden>jenisanggota</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	// $('#datatable-kulcimart').dataTable({
	// 	"order": [
	// 		[ 6, "desc" ],
	// 		[ 4, "asc" ],
	// 		[ 0, "asc" ],
	// 	],
	// 	"columnDefs": [
	// 		{ "width": "15%", "targets": 0 },
	// 		{ "width": "15%", "targets": 1 },
	// 		{ "width": "30%", "targets": 2 },
	// 		{ "width": "10%", "targets": 3 },
	// 		{ "width": "10%", "targets": 4 },
	// 		{ "width": "20%", "targets": 5 },
	// 	]
	// });
	$('#datatable-kulcimart').DataTable({
			"pageLength": 10,
			"ordering": true,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}mpartner/getIndex',
				type: "POST",
				dataType: 'json'
			},
			"columnDefs": [{
				"targets": [6],
				"visible": false,
				"orderable": true
			}]
		});
</script>
