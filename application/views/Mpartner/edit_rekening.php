<div class="padding">
	<?php echo ErrorSuccess($this->session)?>
	<?php if($error != '') echo ErrorMessage($error)?>
	<div class="box">
		<div class="box-header">
			<h2>{title}</h2>
		</div>
		<div class="box-divider m-0"></div>
		<div class="box-body">
			<?= form_open('mpartner/save_rekening') ?>
				<div class="form-group row">
					<div class="form-group col-md-6">
						<?= form_label('No Rekening') ?>
						<?= form_input('norekening', $norekening, 'class="form-control"') ?>
					</div>
					<div class="form-group col-md-6">
						<?= form_label('Nama Bank') ?>
						<?= dropdownBank($namabank) ?>
					</div>
					<div class="form-group col-md-6">
						<?= form_label('Cabang Bank') ?>
						<?= form_input('cabangbank', $cabangbank, 'class="form-control"') ?>
					</div>
				</div>
				<div class="text-right">
					<button type="submit" class="btn warning">Simpan</button>
					<a href="{site_url}mpartner" class="btn danger">Batal</a>
				</div>
				<?= form_hidden('idanggota', $idanggota) ?>
			<?= form_close() ?>
		</div>
	</div>
</div>
