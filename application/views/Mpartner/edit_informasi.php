<style type="text/css" media="screen">
	.map_canvas {
		width: 100%;
		height: 250px;
	}
	.map_canvass {
		width: 100%;
		height: 250px;
	}

</style>
<div class="padding">
	<?php echo ErrorSuccess($this->session)?>
	<?php if($error != '') echo ErrorMessage($error)?>
	<div class="box">
		<div class="box-header">
			<h2>{title}</h2>
		</div>
		<div class="box-divider m-0"></div>
		<div class="box-body">
			<?= form_open_multipart('mpartner/save_informasi') ?>

			<input type="hidden" id="fpmemberold-hidden" name="fpmemberold-hidden" value="<?=($fotoprofil!=='')?'{fotoprofil}':'0' ?>">
			<input type="hidden" name="datauptipe" value="0" />
			<input type="hidden" name="upfotoga" id="upfotoga" value="0" />
			<input type="hidden" id="resheight" name="resheight" />
			<input type="hidden" id="reswidth" name="reswidth" />
			<input type="hidden" id="x_axis" name="x_axis" />
			<input type="hidden" id="y_axis" name="y_axis" />

			<input type="hidden" id="resheight2" name="resheight2" />
			<input type="hidden" id="reswidth2" name="reswidth2" />
			<input type="hidden" id="x_axis2" name="x_axis2" />
			<input type="hidden" id="y_axis2" name="y_axis2" />
				<div class="row">
					<div class="col-md-3">
						<div class="box p-1 m-3">
							<div id="image_preview">
								<? if(is_file(FCPATH.'assets/files/anggota/besar/'.$fotoprofil)) : ?>
									<img src="{files_path}anggota/besar/{fotoprofil}" alt="" class="w-100">
									<? else: ?>
											<img style="margin: auto;" id="imgke-1" src="{custom_path}gif/ajax-loader.gif">
											<img onerror="imgError(this);" hidden="hidden" style="cursor:pointer;margin: auto;max-width:360px; max-height:360px;" id="previewing"
											src="{custom_path}gif/ajax-loader.gif" class="lazyIMG" data-src="" />
										<? endif ?>
							</div>
						</div>
						<div class="form-file mb-3">
							<button type="button" class="btn btn-block white" data-toggle="modal" data-target="#m-md">Pilih Foto Profil</button>
						</div>
						<div class="box p-1 m-3">
						<div id="image_previews" style="cursor:pointer;height: 149px;max-width:231px;width:231px; 
max-height:149px;border: 1px solid #ddd;display: flex;">
							<? if(is_file(FCPATH.'assets/files/anggota/sampul/'.$fotosampul)) : ?>
								<img src="{files_path}anggota/sampul/{fotosampul}" alt="" class="w-100">
								<? else: ?>
										<img style="margin: auto;" id="imgke-1" src="{custom_path}gif/ajax-loader.gif">
										<img onerror="imgError(this);" style="max-width:231px;max-height:149px;cursor:pointer;margin: auto;" id="previewing"
										src="{custom_path}gif/ajax-loader.gif" class="lazyIMG" data-src="" />
									<? endif ?>
							</div>
						</div>
						<div class="form-file mb-3">
							<button type="button" class="btn btn-block white" data-toggle="modal" data-target="#m-mds">Pilih Foto Sampul</button>
						</div>
					</div>
					<div class="col-md-9">
						<div class="form-row">
							<div class="form-group col-md-6">
							<!-- <?= form_label('Nama Usaha', 'namaanggota') ?>
								<?= form_input('namaanggota', $namaanggota, 'class="form-control"') ?> -->

								<?= form_label('ID Penjual', 'idpenjual') ?>
								<?= form_input('idpenjual', $idanggota, 'class="form-control" readonly') ?>
							</div>
							<div class="col-md-6">
								<?= form_label('Tanggal Daftar', 'tanggaldaftar') ?>
								<?= form_input('tanggaldaftar', $tanggaldaftar, 'class="form-control" readonly') ?>
							</div>
							<div class="form-group col-md-6" style="margin-bottom:0px;">
								<?= form_label('Tipe Penjual', 'tipepartner') ?>
								<?= dropdownTipePartner($tipepartner,'class="form-control mb-3"') ?>
								<?= form_label('Tempat Lahir', 'tempatlahir','hidden') ?>
								<?= form_input('tempatlahir', $tempatlahir, 'class="form-control mb-3" hidden') ?>
								<?= form_label('Tangal Lahir', 'tanggallahir','hidden') ?>
								<?= form_input('tanggallahir', $tanggallahir, 'class="form-control mb-3" hidden') ?>
								<?= form_label('Kelamin', 'jeniskelamin','hidden') ?>
								<?= dropdownJenkel($jeniskelamin,'class="form-control mb-3" hidden') ?>
								<?= form_label('Agama', 'agama','hidden') ?>
								<?= dropdownAgama($agama,'hidden') ?>
								<!-- <?= form_label('Ahli Waris', 'ahliwaris') ?>
								<?= form_input('ahliwaris', $ahliwaris, 'class="form-control mb-3"') ?> -->
							</div>
							<div class="form-group col-md-6" style="margin-bottom:0px;">
								<?= form_label('Status Kawin', 'statuspernikahan','hidden') ?>
									<?= dropdownStatusPernikahan($statuspernikahan,'class="form-control mb-3" hidden') ?>
									<?= form_label('Jenis Penjual', 'jenispenjual') ?>
									<?= form_input('jenispenjual', penjualJenis($jenisanggota), 'class="form-control" readonly') ?>
							</div>

							<div class="form-group col-md-12" style="margin-top:0px;">
								<?= form_label('No KTP', 'noktp') ?>
								<input type="text" pattern="[0-9]+" class="form-control" name="noktp" value="<?= $noktp?>">
								
								<?= form_label('Nama Pemilik', 'namapemilik') ?>
								<?= form_input('namapemilik', $namapemilik, 'class="form-control"') ?>

								<?= form_label('Nama Usaha', 'namaanggota') ?>
								<?= form_input('namaanggota', $namaanggota, 'class="form-control"') ?>
							</div>
							<div class="form-group col-md-12">
								<?= form_label('Deskripsi') ?>
									<div class="box">
										<textarea data-plugin="summernote" name="deskripsipartner" data-option="{toolbar: [['style', ['bold', 'italic', 'underline', 'clear']],]}">{deskripsipartner}</textarea>
									</div>
							</div>
							<div class="form-group col-md-12">
								<?= form_label('Provinsi', 'provinsi') ?>
								<? $options = createOptionsDropdown('mprovinsi','id','nama') ?>
								<?= form_dropdown('provinsi', $options, $provinsi,'class="form-control" id="provinsi" data-plugin="select2" data-option="{}"') ?>
							</div>
							<div class="form-group col-md-6">
								<?= form_label('Kota', 'Kota') ?>
								<? $options = createOptionsDropdown('mkota','id','nama',array('provinsiid' => $provinsi) ) ?>
								<?= form_dropdown('kota', $options, $kota,'class="form-control" id="kota"') ?>
							</div>
							<div class="form-group col-md-6">
								<?= form_label('Kecamatan', 'Kecamatan') ?>
								<? $options = createOptionsDropdown('mkecamatan','id','nama',array('kotaid' => $kota) ) ?>
								<?= form_dropdown('kecamatan', $options, $kecamatan,'class="form-control" id="kecamatan"') ?>
							</div>
							<div class="form-group col-md-6" style="margin-bottom:0px;">
								<?= form_label('Desa', 'desa') ?>
								<? $options = createOptionsDropdown('mdesa','id','nama',array('kecamatanid' => $kecamatan) ) ?>
								<?= form_dropdown('desa', $options, $desa,'class="form-control" id="desa"') ?>
							</div>
							<div class="form-group col-md-6" style="margin-bottom:0px;">
								<?= form_label('Kode Pos', 'kodepos') ?>
								<?= form_input('kodepos', '{kodepos}', 'class="form-control mb-3" id="kodepos"') ?>
							</div>

							<div class="col-sm-6" hidden>
								<div class="form-group">
									<label>Placeholder</label>
									<select class="form-control" data-plugin="select2" data-option="{}" data-placeholder="Select an option">
										<option></option>
										<optgroup label="Alaskan/Hawaiian Time Zone">
											<option value="AK">Alaska</option>
											<option value="HI" disabled="disabled">Hawaii</option>
										</optgroup>
										<optgroup label="Pacific Time Zone">
											<option value="CA">California</option>
											<option value="NV">Nevada</option>
											<option value="OR">Oregon</option>
											<option value="WA">Washington</option>
										</optgroup>
										<optgroup label="Mountain Time Zone">
											<option value="AZ">Arizona</option>
											<option value="CO">Colorado</option>
											<option value="ID">Idaho</option>
											<option value="MT">Montana</option>
											<option value="NE">Nebraska</option>
											<option value="NM">New Mexico</option>
											<option value="ND">North Dakota</option>
											<option value="UT">Utah</option>
											<option value="WY">Wyoming</option>
										</optgroup>
									</select>
								</div>
							</div>
							<div class="form-group col-md-12">
								<div class="box p-1">
									<div class="map_canvas"></div>
									Google Maps
								</div>
							</div>
							<div class="form-group col-md-5">
								<?  ($lat && $lon) ? $valueMaps = '{lat},{lon}' : $valueMaps = 'No, Jl. Sukamulya No.45, Sukagalih, Sukajadi, Kota Bandung, Jawa Barat 40162, Indonesia'; ?>
									<?= form_input('geocomplete', $valueMaps, 'class="form-control mb-3" id="geocomplete"') ?>
							</div>
							<div class="form-group col-md-1">
								<input type="button" id="find" class="btn warning btn-block mb-3" value="Cari">
							</div>
							<div class="form-group col-md-3">
								<?= form_input('lat', '{lat}', 'class="form-control mb-3" readonly') ?>
							</div>
							<div class="form-group col-md-3">
								<?= form_input('lng', '{lon}', 'class="form-control mb-3" readonly') ?>
							</div>
							<div class="form-group col-md-12">
								<?= form_label('Alamat', 'alamat') ?>
									<?= form_textarea('alamat', $alamat, 'rows="3" class="form-control" id="alamat"') ?>
							</div>
							<div class="form-group col-md-4" style="margin-bottom:0px;">
								<?= form_label('Telepon Usaha', 'telepon') ?>
								<input type="text" pattern="[0-9]+" name="telepon" value="<?= $telepon?>" class="form-control mb-3">
							</div>
							<div class="form-group col-md-4" style="margin-bottom:0px;">
								<?= form_label('HP', 'hp') ?>
								<input type="text" pattern="[0-9]+" name="hp" value="<?= $hp?>" class="form-control mb-3">
							</div>
							<div class="form-group col-md-4" style="margin-bottom:0px;">
								<?= form_label('HP(WA)', 'hpwa') ?>
								<input type="text" pattern="[0-9]+" name="hpwa" value="<?= $hpwa?>" class="form-control mb-3">
							</div>
							<div class="form-group col-md-6">
								<?= form_label('Akun Facebook', 'facebook') ?>
								<?= form_input('facebook', $facebook, 'class="form-control"') ?>
							</div>
							<div class="form-group col-md-6">
								<?= form_label('Akun Twitter', 'twitter') ?>
								<?= form_input('twitter', $twitter, 'class="form-control"') ?>
							</div>
							<div class="form-group col-md-12" style="margin-bottom:0px;">
								<?= form_label('Ahli Waris', 'ahliwaris') ?>
								<?= form_input('ahliwaris', $ahliwaris, 'class="form-control mb-3"') ?>
							</div>
							<div class="form-group col-md-12" style="margin-bottom:0px;">
								<?= form_label('Hub. Waris', 'hubunganwaris') ?>
								<?= dropdownHubunganWaris($hubunganwaris,'class="form-control mb-3"') ?>
							</div>
							<div class="form-group col-md-6">
								<?= form_label('Email', 'email') ?>
									<?= form_input('email', $email, 'class="form-control mb-3"') ?>
							</div>
							<div class="col-md-6">
								<?= form_label('Sponsor', 'idsponsor') ?>
								<? ($idsponsor) ? $sponsor = getByFields('manggota', array('idanggota' => $idsponsor) )->namaanggota : $sponsor = '-'; ?>
								<?= form_input('tanggallahir', $sponsor, 'class="form-control mb-3" disabled') ?>
							</div>
							<div class="form-group col-md-12" style="margin-bottom:0px;">
								<?= form_label('Pilih Alamat Utama', 'pilihalamat') ?>
								<select name="pilihalamat" id="pilihalamat" class="form-control mb-3" data-plugin="select2" data-option="{}">
									<option value="#" disabled>Pilih Alamat</option>
									<?php foreach(get_all('manggotaalamattujuan',array('idanggota' => $idanggota,'staktif' => '1')) as $r):?>
										<option value="<?= $r->nourut?>" <?= ($r->stutama == '1' ? 'selected' : '')?>><?= $r->alamat?> <?= ($r->nourut == '1' ? '(Default)' : '')?></option>
									<?php endforeach;?>
									<option value="0">Tambah Alamat</option>
								</select>
							</div>
							
							
						</div>

						<!-- Tambah Alamat -->

						<div id="alamatsbaru" style="display:none;">
						<hr style="margin-bottom:8px;">
						<h6>
							<b>TAMBAH ALAMAT</b>
						</h6>
						<hr style="margin-top:8px;">
						<div class="row" style="margin-bottom:10px;">
							<div class="col-md-3" style="padding-left:30px;">
								<select class="form-control" name="provinsi" id="idprovinsi" data-plugin="select2" data-option="{}" style="width:160px;">
									<option value="0" disabled selected>Provinsi</option>
									<?php foreach(get_all('mprovinsi') as $r):?>
									<option value="<?= $r->id?>" <?=($r->id == 12 ? 'selected' : '')?>><?= $r->nama?></option>
									<?php endforeach;?>
								</select>
							</div>
							<div class="col-md-3" style="padding-left:0px;">
								<select class="form-control" name="kota" id="idkota" data-plugin="select2" data-option="{}" style="width:190px;">
									<option value="0" disabled selected>Kota</option>
									<?php foreach(get_all('mkota',array('provinsiid' => '12')) as $r):?>
									<option value="<?= $r->id?>"><?= $r->nama?></option>
									<?php endforeach;?>
								</select>
							</div>
							<div class="col-md-3" style="padding-left:0px;">
								<select class="form-control" name="kecamatan" id="idkecamatan" data-plugin="select2" data-option="{}" style="width:190px;">
									<option value="" disabled selected>Kecamatan</option>
								</select>
							</div>
							<div class="col-md-3" style="padding-left:0px;">
								<select class="form-control" name="desa" id="iddesa" data-plugin="select2" data-option="{}">
									<option value="" disabled selected>Desa</option>
								</select>
							</div>
						</div>
						<div class="form-group col-md-12">
							<div class="box p-1">
								<div class="map_canvass"></div>
								Google Maps
							</div>
						</div>
						<div class="form-group col-md-5" style="float:left;padding-right:5px;">
							<?  ($lat && $lon) ? $valueMaps = '{lat},{lon}' : $valueMaps = 'No, Jl. Sukamulya No.45, Sukagalih, Sukajadi, Kota Bandung, Jawa Barat 40162, Indonesia'; ?>
								<?= form_input('geocomplete', '', 'class="form-control mb-3" id="geocompletes"') ?>
						</div>
						<div class="form-group col-md-1" style="float:left;padding-left:0px;padding-right:5px;">
							<input type="button" id="finds" class="btn warning btn-block mb-3" value="Cari">
						</div>
						<div class="form-group col-md-3" style="float:left;padding-left:0px;padding-right:5px;">
							<?= form_input('lat', '{lat}', 'id="lat" class="form-control mb-3" readonly') ?>
						</div>
						<div class="form-group col-md-3" style="float:left;padding-left:0px;padding-right:0px;">
							<?= form_input('lng', '{lon}', 'id="long" class="form-control mb-3" readonly') ?>
						</div>
						<!-- <div class="col-md-12"> -->
						Alamat Baru
						<textarea id="alamat_baru" class="form-control"></textarea>
						<div class="table-responsive">
							<table id="datatable-kulcimart" class="table" data-plugin="dataTable">
								<tr>
									<td hidden>
										&nbsp;
									</td>
									<td hidden>
										&nbsp;
									</td>
									<td hidden>
										&nbsp;
									</td>
									<td hidden>
										&nbsp;
									</td>
									<td hidden>
										<input type="text" name="harga" id="idkodepos" class="form-control" placeholder="Harga">
									</td>
									<td width="100%" align="right" hidden>
										 &nbsp;
									</td>
								</tr>

								<tr>
									<td colspan="6" width="100%" align="right">
										 <button type="button" id="tambah_alamat" class="btn btn-primary">
											Tambah <i class="fa fa-plus-square"></i>
										 </button>
									</td>
								</tr>

								<tr class="active" style="text-align:center;">

									<th>Alamat</th>
									<th style="width:190px;">Provinsi</th>
									<th style="width:135px;">Kota</th>
									<th>Kecamatan</th>
									<th>Desa</th>
									<th>Kode Pos</th>
								</tr>

								<tbody id="ekspedisi_detail">
									<?php foreach(get_all('manggotaalamattujuan',array('idanggota' => $this->session->userdata('idanggota'))) as $r):?>
									<tr class="details">
										<td>
											<?= $r->alamat;?>
										</td>
										<td>
											<?= get_by_field('id',$r->provinsi,'mprovinsi')->nama?>
										</td>
										<td>
											<?= get_by_field('id',$r->kota,'mkota')->nama?>
										</td>
										<td>
											<?= get_by_field('id',$r->kecamatan,'mkecamatan')->nama?>
										</td>
										<td>
											<?= get_by_field('id',$r->desa,'mdesa')->nama?>
										</td>
										<td>
											<?= $r->kodepos?>
										</td>
									</tr>
									<?php endforeach;?>
								</tbody>

							</table>
						</div>
						</div>
						<!-- </div> -->
						<!-- EOF Tambah Alamat -->
						
						<!-- MODAL -->
						<div id="m-md" class="modal fade black-overlay" data-backdrop="false">
							<div class="modal-dialog animate modal-lg" id="animate" data-class="fade-down">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title">Upload Foto Profil</h5>
									</div>
									<div class="modal-body text-center p-lg">
										<div class="row">
											<div class="col-md-12">
												<!-- <h3>Demo:</h3> -->
												<div class="img-container">
													<img id="image" hidden alt="Picture">
													<div id="nofoto">
														<h1 class="text-mute" style="font-weight: bold;font-size: 90px;">&nbsp;</h1>
														<h1 class="text-mute" style="font-weight: bold;font-size: 112px;">1 : 1</h1>
														<h1 class="text-mute" style="font-weight: bold;font-size: 90px;">&nbsp;</h1>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="modal-footer">

										<div class="col-md-12 docs-buttons">
											<div class="btn-group">
												<button type="button" class="btn btn-primary" data-method="zoom" data-option="0.1" title="Zoom In">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
														<span class="fa fa-search-plus"></span> Perbesar
													</span>
												</button>
												<button type="button" class="btn btn-primary" data-method="zoom" data-option="-0.1" title="Zoom Out">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
														<span class="fa fa-search-minus"></span> Perkecil
													</span>
												</button>
											</div>

											<!-- <div class="btn-group">
												<button type="button" class="btn btn-primary" data-method="rotate" data-option="-45" title="Rotate Left">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
														<span class="fa fa-rotate-left"></span>
													</span>
												</button>
												<button type="button" class="btn btn-primary" data-method="rotate" data-option="45" title="Rotate Right">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
														<span class="fa fa-rotate-right"></span>
													</span>
												</button>
											</div> -->

											<!-- <div class="btn-group">
												<button type="button" class="btn btn-primary" data-method="scaleX" data-option="-1" title="Flip Horizontal">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
														<span class="fa fa-arrows-h"></span>
													</span>
												</button>
												<button type="button" class="btn btn-primary" data-method="scaleY" data-option="-1" title="Flip Vertical">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
														<span class="fa fa-arrows-v"></span>
													</span>
												</button>
											</div> -->

											<div class="btn-group">
												<!-- <button type="button" class="btn btn-primary" data-method="reset" title="Reset">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
														<span class="fa fa-refresh"></span>
													</span>
												</button> -->
												<label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">
													<input type="file" class="sr-only" id="inputImage" name="fotoprofil" accept="image/*">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="Pilih foto anda">
														<span class="fa fa-upload"></span> Upload
													</span>
												</label>
												<!-- <button type="button" class="btn btn-primary" data-method="destroy" title="Destroy">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
														<span class="fa fa-power-off"></span>
													</span>
												</button> -->
											</div>
											<div class="btn-group btn-group-crop">
												<button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Batal <i class="fa fa-close"></i></button>
												<button type="button" data-dismiss="modal" class="btn btn-success" id="btn-modalFoto" disabled data-method="getCroppedCanvas"
												data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
														Selesai <i class="fa fa-check"></i>
													</span>
												</button>
											</div>
										</div>
									</div>
								</div>
								<!-- /.modal-content -->
							</div>
						</div>
						<!-- END MODAL -->

						<!-- MODAL SAMPUL-->
						<div id="m-mds" class="modal fade black-overlay" data-backdrop="false">
							<div class="modal-dialog animate modal-lg" id="animate" data-class="fade-down">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title">Upload Foto Sampul</h5>
									</div>
									<div class="modal-body text-center p-lg">
										<div class="row">
											<div class="col-md-12">
												<!-- <h3>Demo:</h3> -->
												<div class="img-container">
													<img id="images" hidden alt="Picture">
													<div id="nofotos">
														<h1 class="text-mute" style="font-weight: bold;font-size: 140px;">&nbsp;</h1>
														<h1 class="text-mute" style="font-weight: bold;font-size: 112px;">16 : 9</h1>
														<h1 class="text-mute" style="font-weight: bold;font-size: 140px;">&nbsp;</h1>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="modal-footer">

										<div class="col-md-12 docs-buttons">
											<div class="btn-group">
												<button type="button" class="btn btn-primary" data-method="zoom" data-option="0.1" title="Zoom In">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
														<span class="fa fa-search-plus"></span> Perbesar
													</span>
												</button>
												<button type="button" class="btn btn-primary" data-method="zoom" data-option="-0.1" title="Zoom Out">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
														<span class="fa fa-search-minus"></span> Perkecil
													</span>
												</button>
											</div>

											<!-- <div class="btn-group">
												<button type="button" class="btn btn-primary" data-method="rotate" data-option="-45" title="Rotate Left">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
														<span class="fa fa-rotate-left"></span>
													</span>
												</button>
												<button type="button" class="btn btn-primary" data-method="rotate" data-option="45" title="Rotate Right">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
														<span class="fa fa-rotate-right"></span>
													</span>
												</button>
											</div>

											<div class="btn-group">
												<button type="button" class="btn btn-primary" data-method="scaleX" data-option="-1" title="Flip Horizontal">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
														<span class="fa fa-arrows-h"></span>
													</span>
												</button>
												<button type="button" class="btn btn-primary" data-method="scaleY" data-option="-1" title="Flip Vertical">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
														<span class="fa fa-arrows-v"></span>
													</span>
												</button>
											</div> -->

											<div class="btn-group">
												<!-- <button type="button" class="btn btn-primary" data-method="reset" title="Reset">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
														<span class="fa fa-refresh"></span>
													</span>
												</button> -->
												<label class="btn btn-primary btn-upload" for="inputImages" title="Upload image file">
													<input type="file" class="sr-only" id="inputImages" name="fotosampul" accept="image/*">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="Pilih foto anda">
														<span class="fa fa-upload"></span> Upload
													</span>
												</label>
												<!-- <button type="button" class="btn btn-primary" data-method="destroy" title="Destroy">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
														<span class="fa fa-power-off"></span>
													</span>
												</button> -->
											</div>
											<div class="btn-group btn-group-crop">
												<button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Batal <i class="fa fa-close"></i></button>
												<button type="button" data-dismiss="modal" class="btn btn-success" id="btn-modalFotos" disabled data-method="getCroppedCanvas"
												data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }">
													<span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
														Selesai <i class="fa fa-check"></i>
													</span>
												</button>
											</div>
										</div>
									</div>
								</div>
								<!-- /.modal-content -->
							</div>
						</div>
						<!-- END MODAL -->

					</div>
				</div>
				<div class="text-right">
					<button type="submit" class="btn warning">Simpan</button>
					<a href="{site_url}" class="btn danger">Batal</a>
				</div>
				<?= form_hidden('idanggota', $idanggota) ?>
					<?= form_close() ?>
		</div>
	</div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB5OAXrBY29rOI84KCADn_IAQXUgaqNo4Q&libraries=places"></script>
<script src="{base_url}assets/scripts/plugins/jquery.geocomplete.js"></script>

<script>
	$('input[name="noktp"]').bind('keypress', function(e) {
		var keyCode = (e.which) ? e.which : event.keyCode
		return !(keyCode > 31 && (keyCode < 48 || keyCode > 57));
	});
	$('input[name="telepon"]').bind('keypress', function(e) {
		var keyCode = (e.which) ? e.which : event.keyCode
		return !(keyCode > 31 && (keyCode < 48 || keyCode > 57));
	});
	$('input[name="hp"]').bind('keypress', function(e) {
		var keyCode = (e.which) ? e.which : event.keyCode
		return !(keyCode > 31 && (keyCode < 48 || keyCode > 57));
	});
	$('input[name="hpwa"]').bind('keypress', function(e) {
		var keyCode = (e.which) ? e.which : event.keyCode
		return !(keyCode > 31 && (keyCode < 48 || keyCode > 57));
	});
</script>

<script>
	$(document).on('change', '#pilihalamat',function(){
		if($('#pilihalamat').val() == '0'){
			$('#alamatsbaru').css('display','block');
		}else{
			$.ajax({
				url:'{site_url}manggota/alamat_tujuan/'+$('#pilihalamat').val(),
				success:function(data){
				}
			});
			$('#alamatsbaru').css('display','none');
		}
	});
</script>

<script>
	$(document).on('change', '#idprovinsi', function () { // Get Kota
		var provinsi = $('#idprovinsi').val();
		$.ajax({
			url: '{site_url}referensi_ekspedisi/getKota/' + provinsi,
			dataType: 'json',
			success: function (data) {
				$('#idkota').empty();
				$('#idkota').append('<option value="0" disabled selected>Kota</option>');
				for (var i = 0; i < data.length; i++) {
					$('#idkota').append('<option value="' + data[i].id + '">' + data[i].nama + '</option>');
				}
			}
		});
	});

	$(document).on('change', '#idkota', function () { // Get Kecamatan
		var kota = $('#idkota').val();
		$.ajax({
			url: '{site_url}referensi_ekspedisi/getKecamatan/' + kota,
			dataType: 'json',
			success: function (data) {
				$('#idkecamatan').empty();
				$('#idkecamatan').append('<option value="0" disabled selected>Kecamatan</option>');
				for (var i = 0; i < data.length; i++) {
					$('#idkecamatan').append('<option value="' + data[i].id + '">' + data[i].nama + '</option>');
				}
			}
		});
	});

	$(document).on('change', '#idkecamatan', function () { // Get Desa
		var kecamatan = $('#idkecamatan').val();
		$.ajax({
			url: '{site_url}referensi_ekspedisi/getDesa/' + kecamatan,
			dataType: 'json',
			success: function (data) {
				$('#iddesa').empty();
				$('#iddesa').append('<option value="0" disabled selected>Desa</option>');
				for (var i = 0; i < data.length; i++) {
					$('#iddesa').append('<option value="' + data[i].id + '">' + data[i].nama + '</option>');
				}
			}
		});
	});

	$(document).on('change', '#iddesa', function () { // Get Desa
		var desa = $('#iddesa').val();
		var provinsi = $('#idprovinsi').val();
		var kota = $('#idkota').val();
		var kecamatan = $('#idkecamatan').val();

		var desa1 = $('#iddesa option:selected').text();
		var provinsi1 = $('#idprovinsi option:selected').text();
		var kota1 = $('#idkota option:selected').text();
		var kecamatan1 = $('#idkecamatan option:selected').text();

		$.ajax({
			url: '{site_url}referensi_ekspedisi/getKodePos/' + provinsi + '/' + kota + '/' + kecamatan + '/' + desa,
			dataType: 'json',
			success: function (data) {
				$('#idkodepos').val(data[0].kodepos);
				alert(data[0].kodepos);
			}
		});
		$('#geocompletes').val(provinsi1+' '+kota1+' '+kecamatan1+' '+desa1);
	});

	$(document).on('click', '#tambah_alamat', function () {
		// alert($('#alamat_baru').val());
		var alamat = $('#alamat_baru').val();
		var desa = $('#iddesa').val();
		var provinsi = $('#idprovinsi').val();
		var kota = $('#idkota').val();
		var kecamatan = $('#idkecamatan').val();
		var lat = $("#lat").val();
		var long = $("#long").val();
		var kodepos = $('#idkodepos').val();
		var geocompletes = $('#geocompletes').val();

		if (alamat != '' && kodepos != '' && geocompletes != '') {
			var tabel = '<tr class="details">';
			tabel += '<td>' + $('#alamat_baru').val() + '</td>'; // 4
			tabel += '<td>' + $('#idprovinsi option:selected').text() + '</td>'; //0
			tabel += '<td>' + $('#idkota option:selected').text() + '</td>'; //1
			tabel += '<td>' + $('#idkecamatan option:selected').text() + '</td>'; // 2
			tabel += '<td>' + $('#iddesa option:selected').text() + '</td>'; // 3
			tabel += '<td>' + $('#idkodepos').val() + '</td>'; // 4
			tabel += '</tr>';
			$('table tbody#ekspedisi_detail').append(tabel);
			// detaildata();
			$.ajax({
				url: '{site_url}manggota/tambahAlamat/' + provinsi + '/' + kota + '/' + kecamatan + '/' + desa + '/' + lat +
					'/' + long + '/' + kodepos,
				method: 'POST',
				data: {
					alamat: alamat
				},
				success: function (data) {
					alert('Berhasil');
				}
			});

		} else {
			alert('Pastikan Data Terisi Dengan Benar !');
		}

	});

</script>

<script type="text/javascript">
	$(document).on('change', '#provinsi', function () {
		var idprovinsi = $('#provinsi').val()
		$('#kecamatan,#desa').empty()
		$('#kodepos').val('')
		var options;
		$.getJSON('{site_url}mpartner/getKota/' + idprovinsi, function (result) {
			$.each(result, function (i, item) {
				options += '<option value="' + i + '" selected>' + item + '</option>';
			})
			$('#kota').append(options)
		})
	})

	$(document).on('change', '#kota', function () {
		var n = $('#kota').val()
		$('#kecamatan,#desa').empty()
		$('#kodepos').val('')
		var options;
		$.getJSON('{site_url}mpartner/getKecamatan/' + n, function (result) {
			$.each(result, function (i, item) {
				options += '<option value="' + i + '" selected>' + item + '</option>';
			})
			$('#kecamatan').append(options)
		})
	})

	$(document).on('change', '#kecamatan', function () {
		var n = $('#kecamatan').val()
		$('#desa').empty()
		$('#kodepos').val('')
		var options;
		$.getJSON('{site_url}mpartner/getDesa/' + n, function (result) {
			$.each(result, function (i, item) {
				options += '<option value="' + i + '" selected>' + item + '</option>';
			})
			$('#desa').append(options)
		})
	})

	$(document).on('change', '#desa', function () {
		var n = $('#desa').val()
		$('#kodepos').val('')
		var options;
		$.getJSON('{site_url}mpartner/getKodepos/' + n, function (result) {
			$('#kodepos').val(result.kodepos)
		})
	})

</script>

<script>
	$(function () {
		$("#geocomplete").geocomplete({
			map: ".map_canvas",
			details: "form ",
			markerOptions: {
				draggable: true
			},
			mapOptions: {
				zoom: 9999,
			},
			maxZoom: 9999,
		});

		$("#geocomplete").bind("geocode:dragged", function (event, latLng) {
			$("input[name=lat]").val(latLng.lat());
			$("input[name=lng]").val(latLng.lng());
			$("input[name=formatted_address]").val(responses[0].formatted_address);
			$("#reset").show();
		});


		$("#reset").click(function () {
			$("#geocomplete").geocomplete("resetMarker");
			$("#reset").hide();
			return false;
		});

		$("#find").click(function () {
			$("#geocomplete").trigger("geocode");
		}).click();


	});

	function initMap() {
		var map = new google.maps.Map(document.getElementById('map'), {
			center: {
				lat: '{lat}',
				lng: '{lng}'
			},
			zoom: 15
		});

		var infowindow = new google.maps.InfoWindow();
		var service = new google.maps.places.PlacesService(map);
		service.getDetails({
			placeId: 'ChIJN1t_tDeuEmsRUsoyG83frY4'
		}, function (place, status) {
			if (status === google.maps.places.PlacesServiceStatus.OK) {
				var marker = new google.maps.Marker({
					map: map,
					position: place.geometry.location
				});
				google.maps.event.addListener(marker, 'click', function () {
					infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
						'Place ID: ' + place.place_id + '<br>' +
						place.formatted_address + '</div>');
					infowindow.open(map, this);
				});
			}
		});
	}

</script>


<script>
	$(function () {
		$("#geocompletes").geocomplete({
			map: ".map_canvass",
			details: "form ",
			markerOptions: {
				draggable: true
			},
			mapOptions: {
				zoom: 9999,
			},
			maxZoom: 9999,
		});

		$("#geocomplete").bind("geocode:dragged", function (event, latLng) {
			$("input[name=lat]").val(latLng.lat());
			$("input[name=lng]").val(latLng.lng());
			$("input[name=formatted_address]").val(responses[0].formatted_address);
			$("#reset").show();
		});


		$("#reset").click(function () {
			$("#geocompletes").geocomplete("resetMarker");
			$("#reset").hide();
			return false;
		});

		$("#finds").click(function () {
			$("#geocompletes").trigger("geocode");
		}).click();


	});

	function initMap() {
		var map = new google.maps.Map(document.getElementById('map'), {
			center: {
				lat: '{lat}',
				lng: '{lng}'
			},
			zoom: 15
		});

		var infowindow = new google.maps.InfoWindow();
		var service = new google.maps.places.PlacesService(map);
		service.getDetails({
			placeId: 'ChIJN1t_tDeuEmsRUsoyG83frY4'
		}, function (place, status) {
			if (status === google.maps.places.PlacesServiceStatus.OK) {
				var marker = new google.maps.Marker({
					map: map,
					position: place.geometry.location
				});
				google.maps.event.addListener(marker, 'click', function () {
					infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
						'Place ID: ' + place.place_id + '<br>' +
						place.formatted_address + '</div>');
					infowindow.open(map, this);
				});
			}
		});
	}

</script>
