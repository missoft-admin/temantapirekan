<div class="padding">
	<?php echo ErrorSuccess($this->session)?>
	<?php if($error != '') echo ErrorMessage($error)?>
	<div class="box"> 
		<div class="box-header d-flex"> 
			<h3>{title}</h3>
		</div> 
		<div style="padding-top:0px;padding:1rem">
			<div class="table-responsive"> 
				<table id="datatable-kulcimart" class="table v-middle p-0 m-0 box" data-plugin="dataTable"> 
					<thead> 
						<tr> 
							<th>No</th> 
							<th>No KTP</th> 
							<th>Nama Anggota</th> 
							<th>Alamat</th> 
							<th>Telepon/HP</th> 
							<th>Status</th> 
							<th>Aksi</th> 
						</tr> 
					</thead> 
					<tbody> 
						<? $number=0; ?> 
						<? foreach($list_index as $r) : ?> 
						<? $number = $number + 1; ?>
						<tr> 
							<td><?= $number ?></td> 
							<td><?= $r->noktp ?></td> 
							<td><?= $r->namaanggota ?></td> 
							<td><?= $r->alamat ?></td>  
							<td><?= $r->telepon.' / '.$r->hp ?></td>  
							<td><?= statusDataPribadi($r->staktif) ?></td>  
							<td> 
								<div class="dropdown"> 
									<button class="btn white dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-gear"></i> </button> 
									<div class="dropdown-menu" x-placement="bottom-start"> 
										<a class="dropdown-item" href="{site_url}manggota/manage">Edit Informasi</a>
										<a class="dropdown-item" href="{site_url}manggota/rekening">Edit Rekening</a>
										<a class="dropdown-item" href="{site_url}" onclick="return confirm('Anda yakin akan lihat peta jaringan ?');">Lihat Peta Jaringan</a>
									</div> 
								</div>
							</td> 
						</tr> 
						<? endforeach ?>
					</tbody> 
				</table> 
			</div> 
		</div> 
	</div> 
</div>

<script type="text/javascript"> 
	$('#datatable-kulcimart').dataTable();
</script>