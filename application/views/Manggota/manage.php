<div class="padding">
	<?php echo ErrorSuccess($this->session)?>
	<?php if($error != '') echo ErrorMessage($error)?>
	<div class="box"> 
		<div class="box-header d-flex"> 
			<h3>{title}</h3>
		</div>
		<?php foreach ($list_index as $r):?>
		<?= form_open('manggota/edit');?>
		<div style="padding-top:0px;padding:1rem">
			<div class="row">
				<div class="col-6">
					
					<div class="form-group row">
		              <label for="inputEmail3" class="col-md-3 col-form-label">ID. PARTNER</label>
		              <div class="col-md-9">
		                <input type="text" class="form-control" name="idanggota" value="<?= $r->idanggota?>" readonly>
		              </div>
		            </div>

					<div class="form-group row">
		              <label for="inputEmail3" class="col-md-3 col-form-label">KTP</label>
		              <div class="col-md-9">
		                <input type="text" class="form-control" name="noktp" value="<?= $r->noktp?>">
		              </div>
		            </div>
		            <div class="form-group row">
		              <label for="inputEmail3" class="col-md-3 col-form-label">Nama</label>
		              <div class="col-md-9">
		                <input type="text" class="form-control" name="namaanggota" value="<?= $r->namaanggota?>">
		              </div>
		            </div>
		            <div class="form-group row">
		              <label for="inputEmail3" class="col-md-3 col-form-label">Alamat</label>
		              <div class="col-md-9">
		                <textarea class="form-control" name="alamat"><?= $r->alamat?></textarea>
		              </div>
		            </div>
		            <div class="form-group row">
		              <label for="inputEmail3" class="col-md-3 col-form-label">Desa</label>
		              <div class="col-md-9">
		                <input type="text" class="form-control" name="desa" value="<?= $r->desa?>">
		              </div>
		            </div>
		            <div class="form-group row">
		              <label for="inputEmail3" class="col-md-3 col-form-label">Kecamatan</label>
		              <div class="col-md-9">
		                <input type="text" class="form-control" name="kecamatan" value="<?= $r->kecamatan?>">
		              </div>
		            </div>
		            <div class="form-group row">
		              <label for="inputEmail3" class="col-md-3 col-form-label">Kota</label>
		              <div class="col-md-9">
		                <input type="text" class="form-control" name="kota" value="<?= $r->kota?>">
		              </div>
		            </div>
		            <div class="form-group row">
		              <label for="inputEmail3" class="col-md-3 col-form-label">Kode Pos</label>
		              <div class="col-md-9">
		                <input type="text" class="form-control" name="kodepos" value="<?= $r->kodepos?>">
		              </div>
		            </div>

				</div>
				<div class="col-6">
					
		            <div class="form-group row">
		              <label for="inputEmail3" class="col-md-3 col-form-label">Telepon</label>
		              <div class="col-md-9">
		                <input type="text" class="form-control" name="telepon" value="<?= $r->telepon?>">
		              </div>
		            </div>
					<div class="form-group row">
		              <label for="inputEmail3" class="col-md-3 col-form-label">Hp</label>
		              <div class="col-md-9">
		                <input type="text" class="form-control" name="hp" value="<?= $r->hp?>">
		              </div>
		            </div>
		            <div class="form-group row">
		              <label for="inputEmail3" class="col-md-3 col-form-label">Hp Wa</label>
		              <div class="col-md-9">
		                <input type="text" class="form-control" name="hpwa" value="<?= $r->hpwa?>">
		              </div>
		            </div>
		            <!-- hidden file -->
		            <input type="hidden" class="form-control" name="tempatlahir" value="<?= $r->tempatlahir?>">
	                <input type="hidden" class="form-control" name="tanggallahir" value="<?= $r->tanggallahir?>">
	                <input type="hidden" class="form-control" name="agama" value="<?= $r->agama?>">
	                <select class="form-control" name="jeniskelamin" hidden>
	                	<option>Pilih Opsi</option>
	                	<option value="1" <?= ($r->jeniskelamin == 1 ? 'selected:"selected"':'')?>>Laki-Laki</option>
	                	<option value="2" <?= ($r->jeniskelamin == 2 ? 'selected:"selected"':'')?>>Perempuan</option>
	                </select>
	                <select class="form-control" name="statuspernikahan" hidden>
	                	<option>Pilih Opsi</option>
	                	<option value="1" <?= ($r->statuspernikahan == 1 ? 'selected:"selected"':'')?>>Menikah</option>
	                	<option value="2" <?= ($r->statuspernikahan == 2 ? 'selected:"selected"':'')?>>Belum Menikah</option>
	                </select>
	                <!-- end hidden file -->

	                <div class="form-group row">
		              <label for="inputEmail3" class="col-md-3 col-form-label">Ahli Waris</label>
		              <div class="col-md-9">
		                <input type="text" class="form-control" name="ahliwaris" value="">
		              </div>
		            </div>
		            <div class="form-group row">
		              <label for="inputEmail3" class="col-md-3 col-form-label">Hubungan Waris</label>
		              <div class="col-md-9">
		                <input type="text" class="form-control" name="hubunganwaris" value="">
		              </div>
		            </div>

		            <div class="form-group row">
		              <label for="inputEmail3" class="col-md-3 col-form-label">Tipe Partner</label>
		              <div class="col-md-9">
		                <select class="form-control" name="tipepartner">
		                	<option>Pilih Opsi</option>
		                	<option value="1">Kulcim 1 - Kirim Saja</option>
		                	<option value="2">Kulcim 2 - Diambil</option>
		                	<option value="3">Kulcim 3 - Ambil & Kirim</option>
	                	</select>
		              </div>
		            </div>
		            <div class="form-group row">
		              <label for="inputEmail3" class="col-md-3 col-form-label">Jenis Partner</label>
		              <div class="col-md-9">
		                <input type="text" class="form-control" name="jenispartner" value="PREMIUM" readonly>
		              </div>
		            </div>

		            <div class="form-group row">
		              <label for="inputEmail3" class="col-md-3 col-form-label">Email</label>
		              <div class="col-md-9">
		                <input type="text" class="form-control" name="email" value="<?= $r->email?>">
		              </div>
		            </div>

				</div>
			</div>

			<hr>
			<div class="row">
				<div class="col-8"></div>
				<div class="col-2">
					<button type="submit" class="btn btn-sm btn-primary" style="width: 100%"><i class="fa fa-pencil"></i> Edit</button>
				</div>
				<?= form_close();?>
				<div class="col-2">
					<a href="{site_url}"><button type="button" class="btn btn-sm btn-danger" style="width: 100%"><i class="fa fa-close"></i> Batal</button></a>
				</div>
			</div>
			<?php endforeach;?>

		</div> 
	</div> 
</div>

<script type="text/javascript"> 
</script>