<div class="padding">
	<?php echo ErrorSuccess($this->session)?>
	<?php if($error != '') echo ErrorMessage($error)?>
	<div class="box"> 
		<div class="box-header d-flex"> 
			<h3>{title}</h3>
		</div>
		<?php foreach ($list_index as $r):?>
		<div style="padding-top:0px;padding:1rem">
			<div class="container">
			<form action="{site_url}manggota/editRekening" method="POST">
				<div class="row">
		        	<div class="col-12">
			            <div class="form-group row">
			              <label for="inputEmail3" class="col-md-3 col-form-label">Nama Bank</label>
			              <div class="col-md-9">
			                <input type="text" class="form-control" name="namabank" value="<?= $r->namabank?>">
			              </div>
			            </div>
		        	</div>
					<div class="col-12">
						<div class="form-group row">
			              <label for="inputEmail3" class="col-md-3 col-form-label">No Rekening</label>
			              <div class="col-md-9">
			                <input type="text" class="form-control" name="norekening" value="<?= $r->norekening?>">
			              </div>
			            </div>
		        	</div>
		        	<div class="col-12">
						<div class="form-group row">
			              <label for="inputEmail3" class="col-md-3 col-form-label">Atas Nama</label>
			              <div class="col-md-9">
			                <input type="text" class="form-control" name="atasnama" value="<?= $r->atasnama?>">
			              </div>
			            </div>
		        	</div>
		        	<div class="col-12">
			            <div class="form-group row">
			              <label for="inputEmail3" class="col-md-3 col-form-label">Cabang Bank</label>
			              <div class="col-md-9">
			                <input type="text" class="form-control" name="cabangbank" value="<?= $r->cabangbank?>">
			              </div>
			            </div>
		        	</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-8"></div>
					<div class="col-2">
						<button type="submit" class="btn btn-sm btn-primary" style="width: 100%"><i class="fa fa-pencil"></i> Edit</button>
					</div>
					<div class="col-2">
						<a href="<?= base_url('')?>"><button type="button" class="btn btn-sm btn-danger" style="width: 100%"><i class="fa fa-close"></i> Batal</button></a>
					</div>
				</div>
			</form>
		<?php endforeach;?>
		</div>
	  </div>
	</div>
</div>