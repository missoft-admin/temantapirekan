<div class="padding">
	<?php echo ErrorSuccess($this->session)?>
	<?php if($error != '') echo ErrorMessage($error)?>
	<div class="box"> 
		<div class="box-header d-flex"> 
			<h3>{title}</h3>
		</div> 
		<div style="padding-top:0px;padding:1rem">
			<div class="box">
				<div class="box-body">
				<?php
					$cek = $this->uri->segment(2);
				?>
					<form class="form-inline" method="post" action="{base_url}manggota/<?= $cek?>/1" role="form">
						<label class="sr-only" for="exampleInputEmail2">Bulan</label>
						<select name="bulan" class="form-control input-c">
							<option <?= ($bulan == '01')? 'selected' : ''?> value="01">Januari</option>
							<option <?= ($bulan == '02')? 'selected' : ''?> value="02">Februari</option>
							<option <?= ($bulan == '03')? 'selected' : ''?> value="03">Maret</option>
							<option <?= ($bulan == '04')? 'selected' : ''?> value="04">April</option>
							<option <?= ($bulan == '05')? 'selected' : ''?> value="05">Mei</option>
							<option <?= ($bulan == '06')? 'selected' : ''?> value="06">Juni</option>
							<option <?= ($bulan == '07')? 'selected' : ''?> value="07">Juli</option>
							<option <?= ($bulan == '08')? 'selected' : ''?> value="08">Agustus</option>
							<option <?= ($bulan == '09')? 'selected' : ''?> value="09">September</option>
							<option <?= ($bulan == '10')? 'selected' : ''?> value="10">Oktober</option>
							<option <?= ($bulan == '11')? 'selected' : ''?> value="11">November</option>
							<option <?= ($bulan == '12')? 'selected' : ''?> value="12">Desember</option>
			      </select> &nbsp;
						<label class="sr-only" for="exampleInputPassword2">Tahun</label>
						<select name="tahun" class="form-control input-c">
							<?php for($i=date("Y");$i>=2018;$i--){?>
								<option <?= ($i == $tahun)? 'selected' : ''?> value="<?=$i?>"><?=$i?></option>
							<?php } ?>
			      </select> &nbsp;
						<button type="submit" class="btn white">Filter</button>
					</form>
				</div>
			</div>

			<div class="box">
				<div class="box-body">
					<a href="{site_url}manggota/petaPembeli/0"><button type="button" class="btn btn-md btn-primary {pembeli}" style="width:33%;">PEMBELI&nbsp;<span><i class="{faPembeli}"></i></span></button></a>
					<a href="{site_url}manggota/petaPenjual/0"><button type="button" class="btn btn-md btn-success {penjual}" style="width:33%;">PENJUAL&nbsp;<span><i class="{faPenjual}"></i></span></button></a>
					<a href="{site_url}manggota/peta_jaringan/0"><button type="button" class="btn btn-md btn-warning {semua}" style="width:33%;">SEMUA &nbsp;<span><i class="{faSemua}"></i></span></button></a>
				</div>
			</div>
			<div class="table-responsive"> 
				<table id="datatable-kulcimart" class="table v-middle p-0 m-0 box" data-plugin="dataTable" style="text-transform: uppercase;"> 
					<thead> 
						<tr> 
							<th>No</th> 
							<th>Id {anggota}</th>
							<th>Nama {anggota}</th>
							<th>Alamat</th>
							<th>Nominal Transaksi</th>
						</tr> 
					</thead> 
					<tbody>
						<?php $no = 1;?>
						

						<?php if($peta == 'semua'){ ?>
							<?php foreach($list_index1 as $r):?>
								<tr>
									<td><?= $no++?></td>
									<td><?= $r->idanggota?> <b><?= ($peta == 'semua' ? '(PEMBELI)' : $anggota)?></b></td>
									<td><?= $r->namaanggota?></td>
									<td><?= $r->alamat?></td>
									<td>Rp. <?= ($r->nominaltransaksi != '' ? number_format($r->nominaltransaksi) : '0')?></td>
								</tr>
							<?php endforeach;?>

							<?php foreach($list_index2 as $s):?>
								<tr>
									<td><?= $no++?></td>
									<td><?= $s->idanggotapartner?> <b><?= ($peta == 'semua' ? '(PENJUAL)' : $anggota)?></b></td>
									<td><?= get_by_field('idanggota',$s->idanggotapartner,'manggota')->namaanggota?></td>
									<td><?= get_by_field('idanggota',$s->idanggotapartner,'manggota')->alamat?></td>
									<td>Rp. <?= ($s->nominaltransaksi != '' ? number_format($s->nominaltransaksi) : '0')?></td>
								</tr>
							<?php endforeach;

							}elseif($peta == 'pembeli'){?>								
								<?php foreach($list_index1 as $r):?>
									<tr>
										<td><?= $no++?></td>
										<td><?= $r->idanggota?> <b><?= ($peta == 'semua' ? '(PEMBELI)' : $anggota)?></b></td>
										<td><?= $r->namaanggota?></td>
										<td><?= $r->alamat?></td>
										<td>Rp. <?= ($r->nominaltransaksi != '' ? number_format($r->nominaltransaksi) : '0')?></td>
									</tr>
								<?php endforeach;?>
								
							<?php }else{?>
								<?php foreach($list_index2 as $s):?>
									<tr>
										<td><?= $no++?></td>
										<td><?= $s->idanggotapartner?> <b><?= ($peta == 'semua' ? '(PENJUAL)' : $anggota)?></b></td>
										<td><?= get_by_field('idanggota',$s->idanggotapartner,'manggota')->namaanggota?></td>
										<td><?= get_by_field('idanggota',$s->idanggotapartner,'manggota')->alamat?></td>
										<td>Rp. <?= ($s->nominaltransaksi != '' ? number_format($s->nominaltransaksi) : '0')?></td>
									</tr>
								<?php endforeach;
							 }?>
					</tbody>
				</table> 
			</div> 
		</div> 
	</div> 
</div>

<script type="text/javascript"> 
	$('#datatable-kulcimart').dataTable();
</script>