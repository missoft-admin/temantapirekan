<div class="padding">
	<?php echo ErrorSuccess($this->session)?>
	<?php if($error != '') echo ErrorMessage($error)?>
	<div class="box"> 
		<div class="box-header d-flex"> 
			<h3>{title}</h3>
		</div> 
		<div style="padding-top:0px;padding:1rem">
			<div class="table-responsive"> 
				<table id="datatable-kulcimart" class="table v-middle p-0 m-0 box" data-plugin="dataTable"> 
					<thead> 
						<tr> 
							<th>No</th> 
							<th>Id</th>
							<th>Periode</th>
							<th>Tanggal Bayar</th> 
							<th>Jenis Penjual</th> 
							<th>Nominal Transaksi</th> 
						</tr> 
					</thead> 
					<tbody>
						<?php $number = 1;?>
						<?php foreach($list_index as $r):?>
							<tr>
								<td><?= $number++?></td>
								<td><?= $r->idbayarregpartner?></td>
								<td><?= substr($r->periodebayar,0,4)?> - <?= bulan(substr($r->periodebayar,4,6))?></td>
								<td><?= $r->tanggalbayar?></td>
								<td><?= jenisPartner($r->jenispartner)?></td>
								<td><?= number_format($r->nominaltransaksi)?></td>
							</tr>
						<?php endforeach;?>
						<tr>
							<td colspan="5" align="center"><b>TOTAL</b></td>
							<td colspan="1"><b>Rp. {sumDetail}</b></td>
						</tr>
					</tbody> 
				</table> 
			</div> 
		</div> 
	</div> 
</div>

<script type="text/javascript"> 
	$('#datatable-kulcimart').dataTable();
</script>