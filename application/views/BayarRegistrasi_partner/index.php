<div class="padding">
	<?php echo ErrorSuccess($this->session)?>
	<?php if($error != '') echo ErrorMessage($error)?>
	<div class="box"> 
		<div class="box-header d-flex"> 
			<h3>{title}</h3>
		</div>
		<?= form_open_multipart('bayarRegistrasi_partner/save');?>
		<div style="padding-top:0px;padding:1rem">
			<div class="row">
				<div class="col-6">
					<input type="hidden" name="detaildata" id="detaildata">
		            <div class="form-group row">
		              <label for="inputEmail3" class="col-md-3 col-form-label">KODE REGISTRASI</label>
		              <div class="col-md-9">
		                <input type="text" class="form-control" id="kode" name="idbayarregpartner" value="<?= $kode?>" readonly>
		              </div>
		            </div>
		            <div class="form-group row">
		              <label for="inputEmail3" class="col-md-3 col-form-label">FOTO BUKTI TRANSFER</label>
		              <div class="col-md-9">
		                <input type="file" class="form-control" name="foto" id="kelurahan" value="" onchange="readURL(this);"><div id="asd"><input readonly type="text" name="" class="form-control" value="250x250 Pixel" style="text-align: center;font-size: 30px;height: 200px;"></div>
		              </div>
		            </div>

				</div>
				<div class="col-6">

		            <div class="form-group row">
		              <label for="inputEmail3" class="col-md-3 col-form-label">NOMINAL TRANSAKSI</label>
		              <div class="col-md-9">
		                <input type="text" class="form-control number" id="nominaltransaksi" name="nominaltransaksi" value="0" required>
		              </div>
		            </div>
		            <div class="form-group row">
		              <label for="inputEmail3" class="col-md-3 col-form-label">NAMA BANK</label>
		              <div class="col-md-9">
		                <input type="text" class="form-control" name="namabank" id="harga" value="" required>
		              </div>
		            </div>
		            <div class="form-group row">
		              <label for="inputEmail3" class="col-md-3 col-form-label">NO REKENING</label>
		              <div class="col-md-9">
		                <input type="text" class="form-control" name="norekening" value="" required>
		              </div>
		            </div>
		            <div class="form-group row">
		              <label for="inputEmail3" class="col-md-3 col-form-label">ATAS NAMA</label>
		              <div class="col-md-9">
		                <input type="text" class="form-control" name="atasnama" value="" required>
		              </div>
		            </div>
<!-- 
		            <input type='file' onchange="readURL(this);" />
    				<img id="blah" src="#" alt="your image" />
 -->
				</div>
			</div>
			<br>
			<a href="#" data-toggle="modal" data-target="#tambahRegis" style="float: right;font-size: 35px;"><i class="fa fa-plus-circle"></i></a>
			<div class="table-responsive"> 
				<table id="datatable-kulcimart" class="table v-middle p-0 m-0 box" data-plugin="dataTable" style="text-transform: uppercase;"> 
					<thead>
						<tr>
							<th>Periode</th>
							<th>Jenis Penjual</th>
							<th>Nominal</th>
						</tr>
					</thead>
					<tbody id="regis">
					</tbody>
					<tfoot>
						<tr>
							<td colspan="2" align="center"><b>TOTAL JUMLAH</b></td>
							<td colspan="1"><b id="totalJumlah"></b></td>
						</tr>
					</tfoot>
				</table>
			</div>

			<hr>
			<div class="row">
				<div class="col-8"></div>
				<div class="col-2">
					<button type="submit" class="btn btn-sm btn-primary" style="width: 100%"><i class="fa fa-pencil"></i> Simpan</button>
				</div>
				<?= form_close();?>
				<div class="col-2">
					<a href="{site_url}"><button type="button" class="btn btn-sm btn-danger" style="width: 100%"><i class="fa fa-close"></i> Batal</button></a>
				</div>
			</div>

		</div> 
	</div> 
</div>


<!-- MODAL -->
<div id="tambahRegis" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h4 class="modal-title" style="color: white;">Tambah Registrasi</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">

      	<div class="form-group row">
          <label for="" class="col-md-3 col-form-label">Periode</label>
          <div class="col-md-9">
            <select class="form-control" name="tipeproduk" id="periode">
            	<option value="0">Pilih Opsi ...</option>
            	<option value="201804">April - 2018</option>
            	<option value="201805">Mei - 2018</option>
            	<option value="201806">Juni - 2018</option>
            	<option value="201807">Juli - 2018</option>
            	<option value="201808">Agustus - 2018</option>
            	<option value="201809">September - 2018</option>
            	<option value="201810">Oktober - 2018</option>
            	<option value="201811">November - 2018</option>
            	<option value="201812">Desember - 2018</option>
			</select>
          </div>
        </div>
        <div class="form-group row">
          <label for="" class="col-md-3 col-form-label">Jenis Penjual</label>
          <div class="col-md-9">
            <select class="form-control" name="tipeproduk" id="jenispenjual">
            	<option value="0">Pilih Opsi ...</option>
            	<option value="1">Premium</option>
            	<option value="2">Gold</option>
			</select>
          </div>
        </div>
        <div class="form-group row">
          <label for="" class="col-md-3 col-form-label">Nominal</label>
          <div class="col-md-9">
            <input type="text" class="form-control number" name="" readonly id="nominal">
          </div>
        </div>

	  </div>
      <div class="clearfix"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="addrowregis">Tambah</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
	<!-- End Modal content-->
  </div>
</div>


<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<!-- <link rel="stylesheet" href="{css_path}sweetalert2.min.css" type="text/css" />
<script src="{scripts_path}sweetalert2.js"></script> -->

<script>

	$(document).ready(function(){
		$('.number').number( true, 0 );
	});

	function number_format (number, decimals, decPoint, thousandsSep) {
      number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
      var n = !isFinite(+number) ? 0 : +number
      var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
      var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
      var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
      var s = ''

      var toFixedFix = function (n, prec) {
        var k = Math.pow(10, prec)
        return '' + (Math.round(n * k) / k)
          .toFixed(prec)
      }

       s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
       if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
       }
       if ((s[1] || '').length < prec) {
        s[1] = s[1] || ''
        s[1] += new Array(prec - s[1].length + 1).join('0')
       }
     return s.join(dec)
    }

	function detaildata() {
		var detail_tbl = $('table#datatable-kulcimart tbody#regis tr.bayarRegis').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					return $(cell).html();
				});
			});
		$("#detaildata").val(JSON.stringify(detail_tbl));
	}
	detaildata();
	
	$(document).on('change','#jenispenjual', function(){
		var jenis = $('#jenispenjual').val();
		if(jenis == '1'){
			$('#nominal').val(150000);
		}else if(jenis == '0'){
			$('#nominal').val('');
		}
		else{
			$('#nominal').val(50000);
		}
	});

	var id;
	function jenisP(id){
		var jenispenjual;
		if(id == '1'){
			jenispenjual = 'Premium';
			return jenispenjual;
		}else{
			jenispenjual = 'Gold';
			return jenispenjual;
		}
	}

	function total(){
		var totalharga2 = 0;
		$("table tbody#regis tr.bayarRegis").each(function() {
			totalharga2 = totalharga2 +  parseFloat($(this).find('td:eq(2)').text().replace(/\,/g, ""));
		});
		$('#totalJumlah').html(number_format(totalharga2));
		return totalharga2;
	}
	total();

	$(document).on('click','#addrowregis', function(){
		var nominaltransaksi = parseInt($('#nominaltransaksi').val());
		var nominal = parseInt($('#nominal').val());
		if($('#nominal').val() == '' || $('#jenispenjual').val() == '0'){
			// alert('Data Jenis Penjual Harap Dipilih Terlebih Dahulu');
			swal({
				title: "Peringatan!",
				text: "Data Jenis Penjual Harap Dipilih Terlebih Dahulu.",
				type: "warning",
				timer: 1500,
				showConfirmButton: false
			});

		}else{

			if((total() + nominal) > nominaltransaksi){
				// alert('Transaksi Melebihi Jumlah Nominal Yang Anda Transfer !');
				swal({
					title: "Peringatan!",
					text: "Transaksi Melebihi Jumlah Nominal Yang Anda Transfer !",
					type: "warning",
					timer: 1500,
					showConfirmButton: false
				});
			}else{				
				var jenis = $('#jenispenjual').val();
				var tabel = '<tr class="bayarRegis">';
					tabel += '<td>'+$('#periode').val()+'</td>';
					tabel += '<td>'+jenisP(jenis)+'</td>';
					tabel += '<td>'+$('#nominal').val()+'</td>';
					tabel += '<td hidden>'+$('#jenispenjual').val()+'</td>';
					tabel += '</tr>';
					$('#regis').append(tabel);
					$('#tambahRegis').modal('hide');
					total();
					detaildata();
			}

		}

	});

</script>

<script>
function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#asd').html('<img id="blah" src="#" alt="your image" />')
                $('#blah')
                    .attr('src', e.target.result)
                    .width(370)
                    .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    } 
</script>