<div class="padding">
	<?php echo ErrorSuccess($this->session)?>
	<?php if($error != '') echo ErrorMessage($error)?>
	<div class="box"> 
		<div class="box-header d-flex"> 
			<h3>{title}</h3>
		</div> 
		<div style="padding-top:0px;padding:1rem">
			<div class="table-responsive"> 
				<table id="datatable-kulcimart" class="table v-middle p-0 m-0 box" data-plugin="dataTable"> 
					<thead> 
					<tr style="text-transform: uppercase;background-color:#315fae;color:#ffffff;"> 
							<th>No</th>
							<th hidden>idtranss</th>
							<th>ID Transaksi</th>
							<th>Tanggal Transaksi</th>
							<!-- <th>ID Pembeli</th> -->
							<th>Nama Pembeli</th> 
							<th>Ekspedisi</th> 
							<th>Total Harga</th>
							<th>Biaya Ekspedisi</th>
							<th>Total Bayar</th>
							<th>Alamat Tujuan Kirim</th>						
							<th>Aksi</th>
						</tr> 
					</thead> 
					<tbody> 
						<? $number=0; ?> 
						<? foreach($list_index as $r) : ?> 
						<? $number = $number + 1; ?>
						<tr> 
							<td width="5%"><?= $number ?></td>  
							<td hidden><?= $r->idtransaksi ?></td>
							<td><?= $r->idtransaksi ?></td>
							<td><?= $r->tanggaltransaksi?></td>
							<!-- <td><?= $r->idanggota?></td> -->
							<td><?= get_by_field('idanggota',$r->idanggota,'manggota')->namaanggota ?></td>
							<td><?= get_by_field('idekspedisi', $r->idekspedisi, 'mekspedisi')->namaekspedisi ?></td>
							<td><?= number_format($r->totalharga)?></td>
							<td><?= number_format($r->biayaekspedisi)?></td>
							<td><?= number_format($r->totalbayar)?></td>
							<!-- <td><?= get_all_where('manggotaalamattujuan', array('idanggota' => $r->idanggota, 'nourut' => $r->nourutalamat))->alamat ?></td> -->
							<td><?= get_all_where('manggota', array('idanggota' => $r->idanggota))->alamat ?></td>
							<td> 
								<div class="dropdown"> 
									<button class="btn white dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-gear"></i> </button> 
									<div class="dropdown-menu" x-placement="bottom-start"> 
										<a class="dropdown-item pengiriman" href="#" data-toggle="modal" data-target="#packingPengiriman">Lakukan Pengiriman</a>
									</div> 
								</div>
							</td>
						</tr> 
						<? endforeach ?>
					</tbody> 
				</table> 
			</div> 
		</div> 
	</div> 
</div>

<!-- Modal Packing Pengiriman -->
<div id="packingPengiriman" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" style="max-width: 1100px;">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="text-transform: uppercase;background-color:#315fae;color:#ffffff;">
        <h4 class="modal-title" style="color: white;">Detail Transaksi</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
      	<div class="autoHeight" style="height:auto;">
      		
      		<div class="list inset">
				<table class="table v-middle p-0 m-0 box">
					<tr style="text-transform: uppercase;background-color:#315fae;color:#ffffff;">						
						<th>Foto</th>
						<th>Produk</th>
						<th>Catatan</th>
						<th>Jumlah</th>
						<th>Harga</th>
						<th>Total Harga</th>
					</tr>
					<tbody id="tabelDetail">
						
					</tbody>
					<tr style="text-transform: uppercase;background-color:#315fae;color:#ffffff;">
						<td colspan="5" align="center"><b>TOTAL</b></td>
						<td colspan="1" ><b id="total"></b></td>
					</tr>
				</table>
			</div>
			<input type="hidden" id="idtransaksi">

      	</div>
	  </div>
      <div class="clearfix"></div>
      <div class="modal-footer">
        <button type="button" class="btn" id="verifikasi" data-dismiss="modal" style="text-transform: uppercase;background-color:#315fae;color:#ffffff;">Lanjutkan Pengiriman</button><button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
	<!-- End Modal content-->
  </div>
</div>
<!-- EndModal Packing Pengiriman -->

<script type="text/javascript"> 
	$('#datatable-kulcimart').dataTable();
</script>
<script type="text/javascript">

	function number_format (number, decimals, decPoint, thousandsSep) {
      number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
      var n = !isFinite(+number) ? 0 : +number
      var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
      var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
      var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
      var s = ''

      var toFixedFix = function (n, prec) {
        var k = Math.pow(10, prec)
        return '' + (Math.round(n * k) / k)
          .toFixed(prec)
      }

       s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
       if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
       }
       if ((s[1] || '').length < prec) {
        s[1] = s[1] || ''
        s[1] += new Array(prec - s[1].length + 1).join('0')
       }
     return s.join(dec)
    }

	$(document).on('click','.pengiriman',function(){
		var idtransaksi = ($(this).closest('tr').find("td:eq(1)").html());
		$('#idtransaksi').val(($(this).closest('tr').find("td:eq(1)").html()));
		$.ajax({
			url:'{site_url}tjual_prosespacking/getDetailTransaksi/'+idtransaksi,
			dataType:'json',
			success:function(data){
				$('#tabelDetail').empty();
				for(var i = 0;i < data.length;i++){
					var tabel = '<tr style="text-transform:uppercase;">';
						tabel += '<td><img src="{gambarProduk}/sedang/'+data[i].foto+'" width="100px" height="100px" class="img img-responsive img-rounded"></td>';
						tabel += '<td>'+data[i].namaproduk+'</td>';
						tabel += '<td width="30%">-</td>';
						tabel += '<td>'+data[i].jumlahjual+'</td>';
						tabel += '<td>Rp. '+number_format(data[i].hargajual)+'</td>';
						tabel += '<td>Rp. '+number_format(data[i].jumlahjual * data[i].hargajual)+'</td>';
						tabel += '</tr>';
					$('#tabelDetail').append(tabel);
				}
			}
		});

		$.ajax({
			url:'{site_url}tjual_prosespacking/getSum/'+idtransaksi,
			dataType:'json',
			success:function(data){
				$('#total').html('Rp. '+number_format(data[0].total));
			}
		});

	});
		$(document).on('click','#verifikasi', function(){
			$.ajax({
				url:'{site_url}tjual_packingpengiriman/prosesPengiriman/'+$('#idtransaksi').val(),
				success:function(data){
					location.reload();
				}
			});
		});
</script>