<?php $tipePartner = get_by_field('idanggota', $this->session->userdata('idanggota'),'manggota')->tipepartner;?>
<?php $jenisAnggota = get_by_field('idanggota', $this->session->userdata('idanggota'),'manggota')->jenisanggota;?>
<?php $fotoProfil = get_by_field('idanggota', $this->session->userdata('idanggota'),'manggota')->fotoprofil;?>
<div class="p-3 light lt box-shadow-0 d-flex" style="background-color:#f0f1f3;">
	<div class="flex">
		<div class="col-6" style="float:left;">
			<h1 class="text-md mb-1 _400">Selamat Datang {namaanggota}.</h1>
			<!-- <small class="text-muted">Terakhir Login : 10:29 AM, 02 Februari 2018</small> -->
			<small class="text-muted">Terakhir Login : {date_time}</small><br><br>
			<b><h1 class="text-md">Tipe Pengiriman : <?= tipePenjuals($tipePartner)?></h1>
			<h1 class="text-md">Jenis Penjual&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?= jenisPenjuals($jenisAnggota)?></h1></b>
		</div>			
		<div class="col-6" style="float:left;">
			<div style="float:right;">
				<img class="img img-responsive img-thumbnail" src="{site_url}assets/files/anggota/sedang/<?= $fotoProfil?>" alt="">
			</div>
		</div>
	</div>
</div>
<div class="box">
	<div class="box-body">
		<form class="form-inline" method="post" action="{base_url}dashboard/filter" role="form">
			<label class="sr-only" for="exampleInputEmail2">Bulan</label>
			<select name="bulan" class="form-control input-c">
				<option <?= ($bulan == '01')? 'selected' : ''?> value="01">Januari</option>
				<option <?= ($bulan == '02')? 'selected' : ''?> value="02">Februari</option>
				<option <?= ($bulan == '03')? 'selected' : ''?> value="03">Maret</option>
				<option <?= ($bulan == '04')? 'selected' : ''?> value="04">April</option>
				<option <?= ($bulan == '05')? 'selected' : ''?> value="05">Mei</option>
				<option <?= ($bulan == '06')? 'selected' : ''?> value="06">Juni</option>
				<option <?= ($bulan == '07')? 'selected' : ''?> value="07">Juli</option>
				<option <?= ($bulan == '08')? 'selected' : ''?> value="08">Agustus</option>
				<option <?= ($bulan == '09')? 'selected' : ''?> value="09">September</option>
				<option <?= ($bulan == '10')? 'selected' : ''?> value="10">Oktober</option>
				<option <?= ($bulan == '11')? 'selected' : ''?> value="11">November</option>
				<option <?= ($bulan == '12')? 'selected' : ''?> value="12">Desember</option>
      </select> &nbsp;
			<label class="sr-only" for="exampleInputPassword2">Tahun</label>
			<select name="tahun" class="form-control input-c">
				<?php for($i=date("Y");$i>=2018;$i--){?>
					<option <?= ($i == $tahun)? 'selected' : ''?> value="<?=$i?>"><?=$i?></option>
				<?php } ?>
      </select> &nbsp;
			<button type="submit" class="btn white">Filter</button>
		</form>
	</div>
</div>
<div class="padding">
	<div class="row">
		<div class="col-sm-6 col-lg-3">
			<div class="box list-item">
				<span class="avatar w-40 text-center rounded primary">
          <span class="">Rp</span>
				</span>
				<div class="list-body">
					<h4 class="m-0 text-md"><a href="{base_url}pendapatan/sponsor"><?= number_format($sumSponsor)?></a></h4>
					<small class="text-muted">Total Bonus Sponsor</small>
				</div>
			</div>
		</div>

		<div class="col-sm-6 col-lg-3">
			<div class="box list-item">
				<span class="avatar w-40 text-center rounded info theme">
	            <span class="">Rp</span>
				</span>
				<div class="list-body">
					<h4 class="m-0 text-md"><a href="{base_url}pendapatan/partnership1"><?= number_format($sumPartnership1)?></a></h4>
					<small class="text-muted">Total Bonus Royalti 1</small>
				</div>
			</div>
		</div>

		<div class="col-sm-6 col-lg-3">
			<div class="box list-item">
				<span class="avatar w-40 text-center rounded success">
	            <span class="">Rp</span>
				</span>
				<div class="list-body">
					<h4 class="m-0 text-md"><a href="{base_url}pendapatan/partnership2"><?= number_format($sumPartnership2)?></a></h4>
					<small class="text-muted">Total Bonus Royalti 2</small>
				</div>
			</div>
		</div>

		<div class="col-sm-6 col-lg-3">
			<div class="box list-item">
				<span class="avatar w-40 text-center rounded danger">
	            <span class="">Rp</span>
				</span>
				<div class="list-body">
					<h4 class="m-0 text-md"><a href="{base_url}pendapatan/total"><?= number_format($sum)?></a></h4>
					<small class="text-muted">Total Bonus</small>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="row no-gutters align-items-stretch">
	<div class="col-md-12 light bg">
		<div class="padding">
			<span class="badge success float-right"><?= $jumlahOrder?></span>
			<h6 class="mb-3">ORDER PENJUALAN (STATUS : MENUNGGU)</h6>
			<div class="list inset">
				<table class="table v-middle p-0 m-0 box">
					<tr class="" style="text-transform: uppercase;background-color:#315fae;color:#ffffff;">						
						<th>NO.</th>
						<th>ID TRANSAKSI</th>
						<th>TANGGAL</th>
						<th>NAMA PEMBELI</th>
						<th>ALAMAT TUJUAN</th>
						<th>KOTA TUJUAN</th>
						<th>EKSPEDISI</th>
						<th>TOTAL BAYAR</th>
					</tr>
					<?php $no = 1;?>
					<?php foreach($listStatusMenunggu as $r):?>
						<tr <?= ($no % 2 == 0 ? 'style="background-color:#53a6fa;"':'')?>>
						<!-- <?php $kota = get_all_where('manggotaalamattujuan',array('idanggota' => $r->idanggota, 'nourut' => $r->nourutalamat))->kota;?> -->
							<?php $kota = get_all_where('manggota',array('idanggota' => $r->idanggota))->kota;?>
							<td><?= $no++;?></td>
							<td><?= $r->idtransaksi?></td>
							<td><?= $r->tanggaltransaksi?></td>
							<td><?= $r->namaanggota?></td>
							<!-- <td><?= get_all_where('manggotaalamattujuan',array('idanggota' => $r->idanggota, 'nourut' => $r->nourutalamat))->alamat;?></td> -->
							<td><?= get_all_where('manggota',array('idanggota' => $r->idanggota))->alamat;?></td>
							<td><?= get_by_field('id',$kota,'mkota')->nama?></td>
							<td><?= get_by_field('idekspedisi',$r->idekspedisi,'mekspedisi')->namaekspedisi;?></td>
							<td>Rp. <?= number_format($r->totalbayar)?></td>
						</tr>

					<?php endforeach;?>
				</table>
			</div>

			<div class="p-2">
				<a href="{base_url}tjual_rekapitulasi/index/0" class="btn btn-sm btn-block white">Lihat Selengkapnya</a>
			</div>
		</div>
	</div>
</div>


<div class="row no-gutters align-items-stretch">
	<div class="col-md-6 light bg">
		<div class="padding">
			<span class="badge success float-right"><?= $jumlahProdukPenjualanTerbanyak?></span>
			<h6 class="mb-3">Produk Terbanyak Terjual</h6>
			<div class="list inset">
				<table class="table v-middle p-0 m-0 box">
					<tr style="text-transform: uppercase;background-color:#315fae;color:#ffffff;" class="">						
						<th>Foto</th>
						<th>Produk</th>
						<th>Jumlah</th>
					</tr>
					<?php foreach($produkPenjualanTerbanyak as $r):?>
						<tr>
							<td><img src="{gambarProduk}/kecil/<?= $r->foto?>" class="img-responsive img-thumbnail" style="width: 40px;height: 40px;"></td>
							<td><?= get_by_field('idproduk',$r->idproduk,'manggotaproduk')->namaproduk?></td>
							<td><?= $r->total_jual?></td>
						</tr>
				<?php endforeach;?>
				</table>
			</div>
		</div>
	</div>

	<div class="col-md-6 light bg">
		<div class="padding">
			<span class="badge success float-right"><?= $jumlahProdukPenjualanSedikit?></span>
			<h6 class="mb-3">Produk Sedikit Terjual</h6>
			<div class="list inset">
				<table class="table v-middle p-0 m-0 box">
					<tr style="text-transform: uppercase;background-color:#315fae;color:#ffffff;" class="">						
						<th>Foto</th>
						<th>Produk</th>
						<th>Jumlah</th>
					</tr>
					<?php foreach($produkPenjualanSedikit as $r):?>
						<tr>
							<td><img src="{gambarProduk}/kecil/<?= $r->foto?>" class="img-responsive img-thumbnail" style="width: 40px;height: 40px;"></td>
							<td><?= get_by_field('idproduk',$r->idproduk,'manggotaproduk')->namaproduk?></td>
							<td><?= $r->total_jual?></td>
						</tr>
				<?php endforeach;?>
				</table>
			</div>
		</div>
	</div>
</div>


<div class="row no-gutters align-items-stretch">
	<div class="col-md-6 white bg">
		<div class="padding">
			<span class="badge success float-right"><?= $jumlahProdukTerbanyakPendapatan?></span>
			<h6 class="mb-3">Produk Terbanyak Pendapatan</h6>
			<div class="list inset">
				<table class="table v-middle p-0 m-0 box">
					<tr style="text-transform: uppercase;background-color:#315fae;color:#ffffff;" class="">						
						<th>Produk</th>
						<th>Nominal Pendapatan</th>
					</tr>
					<?php foreach($produkTerbanyakPendapatan as $r){?>
						<tr>
							<td><?= get_by_field('idproduk',$r->idproduk,'manggotaproduk')->namaproduk?></td>
							<td>Rp. <?= number_format($r->nominalpendapatan)?></td>
						</tr>
					<?php }?>
				</table>
			</div>

			<div class="p-2">
				<!-- <a href="#" class="btn btn-sm btn-block white">Load More</a> -->
			</div>
		</div>
	</div>

	<div class="col-md-6 white bg">
		<div class="padding">
			<span class="badge success float-right"><?= $jumlahProdukSedikitPendapatan?></span>
			<h6 class="mb-3">Produk Sedikit Pendapatan</h6>
			<div class="list inset">
				<table class="table v-middle p-0 m-0 box">
					<tr style="text-transform: uppercase;background-color:#315fae;color:#ffffff;" class="">						
						<th>Produk</th>
						<th>Nominal Pendapatan</th>
					</tr>
					<?php foreach($produkSedikitPendapatan as $r){?>
						<tr>
							<td><?= get_by_field('idproduk',$r->idproduk,'manggotaproduk')->namaproduk?></td>
							<td>Rp. <?= number_format($r->nominalpendapatan)?></td>
						</tr>
					<?php }?>
				</table>
			</div>

			<div class="p-2">
				<!-- <a href="#" class="btn btn-sm btn-block white">Load More</a> -->
			</div>
		</div>
	</div>
</div>



<div class="row no-gutters align-items-stretch">
	<div class="col-md-3 dk">
		<div class="padding">
			<span class="badge success float-right"><?= $jumlahMemberSponsorPembelianTerbanyak?></span>
			<h6 class="mb-3">Pembeli Sponsor Pembelian Terbanyak</h6>
			<div class="list inset">
            		<?php foreach($memberSponsorPembelianTerbanyak as $r){?>
				<div class="list-item " data-id="item-2">
					<span class="w-24 avatar circle light-blue">
                <img src="{img_path}a2.jpg" alt=".">
            </span>
					<div class="list-body">
						<a href="#" class="item-title _500"><?= get_by_field('idanggota',$r->idanggota,'manggota')->namaanggota?></a>
						<div class="item-tag tag hide">
						</div>
					</div>
				</div>
				<?php }?>
			</div>

			<div class="p-2">
				<!-- <a href="#" class="btn btn-sm btn-block white">Load More</a> -->
			</div>
		</div>
	</div>

	<div class="col-md-3 dk">
		<div class="padding">
			<span class="badge success float-right"><?= $jumlahPartnerSponsorPenjualanTerbanyak?></span>
			<h6 class="mb-3">Penjual Sponsor Penjualan Terbanyak</h6>
			<div class="list inset">
				<?php foreach($partnerSponsorPenjualanTerbanyak as $r){?>
				<div class="list-item " data-id="item-2">
					<span class="w-24 avatar circle light-blue">
                <img src="{img_path}a2.jpg" alt=".">
            </span>
					<div class="list-body">
						<a href="" class="item-title _500"><?= get_by_field('idanggota',$r->idanggotapartner,'manggota')->namaanggota?></a>
						<div class="item-tag tag hide">
						</div>
					</div>
				</div>
				<?php }?>
			</div>

			<div class="p-2">
				<!-- <a href="#" class="btn btn-sm btn-block white">Load More</a> -->
			</div>
		</div>
	</div>

	<div class="col-md-3 dk">
		<div class="padding">
			<span class="badge success float-right"><?= $jumlahMemberSponsorPembelianSedikt?></span>
			<h6 class="mb-3">Pembeli Sponsor Pembelian Paling Sedikit</h6>
			<div class="list inset">
				<?php foreach($memberSponsorPembelianSedikt as $r){?>
				<div class="list-item " data-id="item-2">
					<span class="w-24 avatar circle light-blue">
                <img src="{img_path}a2.jpg" alt=".">
            </span>
					<div class="list-body">
						<a href="#" class="item-title _500"><?= get_by_field('idanggota',$r->idanggota,'manggota')->namaanggota?></a>
						<div class="item-tag tag hide">
						</div>
					</div>
				</div>
				<?php }?>
			</div>

			<div class="p-2">
				<!-- <a href="#" class="btn btn-sm btn-block white">Load More</a> -->
			</div>
		</div>
	</div>
	<div class="col-md-3 dk">
		<div class="padding">
			<span class="badge success float-right"><?= $jumlahPartnerSponsorPenjualanSedikit?></span>
			<h6 class="mb-3">Penjual Sponsor Penjualan Paling Sedikit</h6>
			<div class="list inset">
				<?php foreach($partnerSponsorPenjualanSedikit as $r){?>
				<div class="list-item " data-id="item-2">
					<span class="w-24 avatar circle light-blue">
                <img src="{img_path}a2.jpg" alt=".">
            </span>
					<div class="list-body">
						<a href="" class="item-title _500"><?= get_by_field('idanggota',$r->idanggotapartner,'manggota')->namaanggota?></a>
						<div class="item-tag tag hide">
						</div>
					</div>
				</div>
				<?php }?>
			</div>

			<div class="p-2">
				<!-- <a href="#" class="btn btn-sm btn-block white">Load More</a> -->
			</div>
		</div>
	</div>
</div>

<script type="text/javascript"> 
	$('#datatable-kulcimart').dataTable();
</script>
