<div class="padding">
	<?php echo ErrorSuccess($this->session)?>
	<?php if($error != '') echo ErrorMessage($error)?>
	<div class="box"> 
		<div class="box-header d-flex"> 
			<h3>{title}</h3>
		</div> 
		<div style="padding-top:0px;padding:1rem">
			<div class="box">
				<div class="box-body">
					<form class="form-inline" method="post" action="{base_url}pendapatan/filterPartnership1" role="form">
						<label class="sr-only" for="exampleInputEmail2">Bulan</label>
						<select name="bulan" class="form-control input-c">
							<option <?= ($bulan == '01')? 'selected' : ''?> value="01">Januari</option>
							<option <?= ($bulan == '02')? 'selected' : ''?> value="02">Februari</option>
							<option <?= ($bulan == '03')? 'selected' : ''?> value="03">Maret</option>
							<option <?= ($bulan == '04')? 'selected' : ''?> value="04">April</option>
							<option <?= ($bulan == '05')? 'selected' : ''?> value="05">Mei</option>
							<option <?= ($bulan == '06')? 'selected' : ''?> value="06">Juni</option>
							<option <?= ($bulan == '07')? 'selected' : ''?> value="07">Juli</option>
							<option <?= ($bulan == '08')? 'selected' : ''?> value="08">Agustus</option>
							<option <?= ($bulan == '09')? 'selected' : ''?> value="09">September</option>
							<option <?= ($bulan == '10')? 'selected' : ''?> value="10">Oktober</option>
							<option <?= ($bulan == '11')? 'selected' : ''?> value="11">November</option>
							<option <?= ($bulan == '12')? 'selected' : ''?> value="12">Desember</option>
				  </select> &nbsp;
						<label class="sr-only" for="exampleInputPassword2">Tahun</label>
						<select name="tahun" class="form-control input-c">
							<?php for($i=date("Y");$i>=2018;$i--){?>
								<option <?= ($i == $tahun)? 'selected' : ''?> value="<?=$i?>"><?=$i?></option>
							<?php } ?>
				  </select> &nbsp;
						<button type="submit" class="btn white">Filter</button>
					</form>
				</div>
			</div>
			<div class="table-responsive"> 
				<table id="datatable-kulcimart" class="table v-middle p-0 m-0 box" data-plugin="dataTable"> 
					<thead> 
						<tr> 
							<th>No</th> 
							<th>Id Transaksi</th> 
							<th>Periode Transaksi</th> 
							<th>Tanggal Transaksi</th> 
							<th>Nominal Bonus</th> 
						</tr> 
					</thead> 
					<tbody> 
						<? $number=0; ?> 
						<? foreach($list_index as $r) : ?> 
						<? $number = $number + 1; ?>
						<tr> 
							<td><?= $number ?></td> 
							<td><?= $r->idtransaksi ?></td> 
							<td><?= substr($r->periodetransaksi,0,4)?> - <?= bulan(substr($r->periodetransaksi,4,6))?></td> 
							<td><?= $r->tanggaltransaksi ?></td>  
							<td>Rp. <?= number_format($r->nominalbonus)?></td>   
						</tr>
						<? endforeach ?>
					</tbody>
					<tfoot>
						<tr class="info">
							<td colspan="4" align="center"><b>Total</b></td>
							<td colspan="1"><b>Rp. <?= number_format($sum)?></b></td>
						</tr>
					</tfoot> 
				</table> 
			</div> 
		</div> 
	</div> 
</div>

<script type="text/javascript"> 
	$('#datatable-kulcimart').dataTable();
</script>