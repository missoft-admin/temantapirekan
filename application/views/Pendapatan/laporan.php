<style>
	.table-responsive{
		/* overflow-x:scroll; */
		width:1000px;
	}
	.table{
		/* overflow-x:scroll; */
		/* width:100%; */
	}
</style>
<div class="padding">
	<?php echo ErrorSuccess($this->session)?>
	<?php if($error != '') echo ErrorMessage($error)?>
	<div class="box"> 
		<div class="box-header d-flex"> 
			<h3>{title}</h3>
		</div>
		<div class="box">
			<div class="box-body">
				<form class="form-inline" method="post" action="{base_url}pendapatan/laporan/0" role="form">
					<label class="sr-only" for="exampleInputEmail2">Bulan</label>
					<select name="bulan" class="form-control input-c">
						<option <?= ($bulan == '01')? 'selected' : ''?> value="01">Januari</option>
						<option <?= ($bulan == '02')? 'selected' : ''?> value="02">Februari</option>
						<option <?= ($bulan == '03')? 'selected' : ''?> value="03">Maret</option>
						<option <?= ($bulan == '04')? 'selected' : ''?> value="04">April</option>
						<option <?= ($bulan == '05')? 'selected' : ''?> value="05">Mei</option>
						<option <?= ($bulan == '06')? 'selected' : ''?> value="06">Juni</option>
						<option <?= ($bulan == '07')? 'selected' : ''?> value="07">Juli</option>
						<option <?= ($bulan == '08')? 'selected' : ''?> value="08">Agustus</option>
						<option <?= ($bulan == '09')? 'selected' : ''?> value="09">September</option>
						<option <?= ($bulan == '10')? 'selected' : ''?> value="10">Oktober</option>
						<option <?= ($bulan == '11')? 'selected' : ''?> value="11">November</option>
						<option <?= ($bulan == '12')? 'selected' : ''?> value="12">Desember</option>
				</select> &nbsp;
					<label class="sr-only" for="exampleInputPassword2">Tahun</label>
					<select name="tahun" class="form-control input-c">
						<?php for($i=date("Y");$i>=2018;$i--){?>
							<option <?= ($i == $tahun)? 'selected' : ''?> value="<?=$i?>"><?=$i?></option>
						<?php } ?>
				</select> &nbsp;
					<button type="submit" class="btn white">Filter</button>
				</form>
			</div>
			<div class="box-body">
				<h6>TOTAL NOMINAL TRANSAKSI : Rp. <?= number_format($nominalTransaksi)?></h6>
				<h6>JUMLAH TRANSAKSI &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?= $jumlahTransaksi?></h6>
			</div>
		</div>
		<div style="padding-top:0px;padding:1rem">
			<div class="table-responsive"> 
				<table id="datatable-kulcimart" class="table v-middle p-0 m-0 box" data-plugin="dataTable"> 
					<thead> 
						<tr> 
							<th>No</th>  
							<th>ID Transaksi</th>
							<th>Tanggal Transaksi</th>
							<th>ID Pembeli</th>
							<th>Nama Pembeli</th> 
							<th>Ekspedisi</th> 
							<th>Total Harga</th>
							<th>Biaya Ekspedisi</th>
							<th>Total Bayar</th>
							<th width="50%">Alamat Tujuan Kirim</th>
						</tr> 
					</thead> 
					<tbody> 
						<? $number=0; ?> 
						<? foreach($list_index as $r) : ?> 
						<? $number = $number + 1; ?>
						<tr> 
							<td><?= $number ?></td>  
							<td><?= $r->idtransaksi ?></td>
							<td><?= $r->tanggaltransaksi?></td>
							<td><?= $r->idanggota?></td>
							<td><?= get_by_field('idanggota',$r->idanggota,'manggota')->namaanggota ?></td>
							<td><?= get_by_field('idekspedisi', $r->idekspedisi, 'mekspedisi')->namaekspedisi ?></td>
							<td>Rp. <?= number_format($r->totalharga)?></td>
							<td>Rp. <?= number_format($r->biayaekspedisi)?></td>
							<td>Rp. <?= number_format($r->totalbayar)?></td>
							<!-- <td><?= get_all_where('manggotaalamattujuan', array('idanggota' => $r->idanggota, 'nourut' => $r->nourutalamat))->alamat ?></td> -->
							<!-- <td><?= get_all_where('manggota', array('idanggota' => $r->idanggota))->alamat ?></td> -->
						</tr> 
						<? endforeach ?>
					</tbody> 
				</table> 
			</div> 
		</div> 
	</div> 
</div>

<!-- Modal Rekapitulasi -->
<div id="rekapitulasi" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" style="max-width: 1100px;">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-default">
        <h4 class="modal-title" style="color: black;">Detail Transaksi</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
      	<div class="autoHeight">
      		
      		<div class="list inset">
				<table class="table v-middle p-0 m-0 box">
					<tr style="text-transform: uppercase;" class="warning">						
						<th>Foto</th>
						<th>Produk</th>
						<th>Catatan</th>
						<th>Kategori</th>
						<th>Jumlah</th>
						<th>Harga</th>
						<th>Total Harga</th>
					</tr>
					<tbody id="tabelDetail">
						
					</tbody>
					<tr class="warning">
						<td colspan="6" align="center"><b>TOTAL</b></td>
						<td colspan="1" ><b id="total"></b></td>
					</tr>
				</table>
			</div>
			<input type="hidden" id="idtransaksi">

      	</div>
	  </div>
      <div class="clearfix"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
	<!-- End Modal content-->
  </div>
</div>
<!-- End Modal Rekapitulasi -->

<script type="text/javascript"> 
	$('#datatable-kulcimart').DataTable({
		scrollx:true
	});
</script>