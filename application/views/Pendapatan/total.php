<div class="padding">
	<?php echo ErrorSuccess($this->session)?>
	<?php if($error != '') echo ErrorMessage($error)?>
	<div class="box"> 
		<div class="box-header d-flex"> 
			<h3>{title}</h3>
		</div> 
		<div style="padding-top:0px;padding:1rem">
			<div class="table-responsive"> 
				<table id="datatable-kulcimart" class="table v-middle p-0 m-0 box" data-plugin="dataTable"> 
					<thead> 
						<tr> 
							<th>No</th> 
							<th>Bonus</th>
							<th>Nominal Bonus</th> 
						</tr> 
					</thead> 
					<tbody> 

						<tr> 
							<td>1</td>   
							<td><span class="badge success text-u-c" style="font-size: 14px;width: 170px;">Cashback Sponsor</span></td>
							<td><?= number_format($sumSponsor)?></td>   
						</tr>
						<tr> 
							<td>2</td>   
							<td><span class="badge danger text-u-c" style="font-size: 14px;width: 170px;">Cashback Royalti 1</span></td>
							<td><?= number_format($sumPartnership1)?></td>   
						</tr>
						<tr> 
							<td>3</td>   
							<td><span class="badge warning text-u-c" style="font-size: 14px;width: 170px;">Cashback Royalti 2</span></td>
							<td><?= number_format($sumPartnership2)?></td>   
						</tr>
					</tbody>
					<tfoot>
						<tr class="info">
							<td colspan="2" align="center"><b>Total</b></td>
							<td colspan="1"><b>Rp. <?= number_format($sum)?></b></td>
						</tr>
					</tfoot> 
				</table> 
			</div> 
		</div> 
	</div> 
</div>
