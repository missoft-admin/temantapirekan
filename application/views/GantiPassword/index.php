<div class="padding">
	<?php echo ErrorSuccess($this->session)?>
	<?php if($error != '') echo ErrorMessage($error)?>
	<div class="box"> 
		<div class="box-header d-flex"> 
			<h3>{title}</h3>
		</div>
		<?= form_open('GantiPassword/update');?>
		<div style="padding-top:0px;padding:1rem">
			<div class="row">
				<div class="col-3"></div>
				<div class="col-6">
					
					<div class="form-group row">
		              <label for="inputEmail3" class="col-md-3 col-form-label">PASSWORD LAMA</label>
		              <div class="col-md-9">
		                <input type="password" class="form-control" name="passwordlama" value="">
		              </div>
		            </div>
		            <div class="form-group row">
		              <label for="inputEmail3" class="col-md-3 col-form-label">PASSWORD BARU</label>
		              <div class="col-md-9">
		                <input type="password" class="form-control" name="passwordbaru" value="">
		              </div>
		            </div>
		            <div class="form-group row">
		              <label for="inputEmail3" class="col-md-3 col-form-label">KONFIRMASI PASSWORD</label>
		              <div class="col-md-9">
		                <input type="password" class="form-control" name="passwordbarukonfirmasi" value="">
		              </div>
		            </div>

				</div>
				<div class="col-3"></div>
			</div>

			<hr>
			<div class="row">
				<div class="col-8"></div>
				<div class="col-2">
					<button type="submit" class="btn btn-sm btn-primary" style="width: 100%"><i class="fa fa-pencil"></i> Simpan</button>
				</div>
				<?= form_close();?>
				<div class="col-2">
					<a href="{site_url}"><button type="button" class="btn btn-sm btn-danger" style="width: 100%"><i class="fa fa-close"></i> Batal</button></a>
				</div>
			</div>

		</div> 
	</div> 
</div>

<script type="text/javascript"> 
</script>