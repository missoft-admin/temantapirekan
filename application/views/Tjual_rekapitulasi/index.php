<div class="padding">
	<?php echo ErrorSuccess($this->session)?>
	<?php if($error != '') echo ErrorMessage($error)?>
	<div class="box"> 
		<div class="box-header d-flex"> 
			<h3>{title}</h3>
		</div>
		<div class="box">
			<div class="box-body">
				<form class="form-inline" method="post" action="{base_url}tjual_rekapitulasi/index/0" role="form">
					<label class="sr-only" for="exampleInputEmail2">Bulan</label>
					<select name="bulan" class="form-control input-c">
						<option <?= ($bulan == '01')? 'selected' : ''?> value="01">Januari</option>
						<option <?= ($bulan == '02')? 'selected' : ''?> value="02">Februari</option>
						<option <?= ($bulan == '03')? 'selected' : ''?> value="03">Maret</option>
						<option <?= ($bulan == '04')? 'selected' : ''?> value="04">April</option>
						<option <?= ($bulan == '05')? 'selected' : ''?> value="05">Mei</option>
						<option <?= ($bulan == '06')? 'selected' : ''?> value="06">Juni</option>
						<option <?= ($bulan == '07')? 'selected' : ''?> value="07">Juli</option>
						<option <?= ($bulan == '08')? 'selected' : ''?> value="08">Agustus</option>
						<option <?= ($bulan == '09')? 'selected' : ''?> value="09">September</option>
						<option <?= ($bulan == '10')? 'selected' : ''?> value="10">Oktober</option>
						<option <?= ($bulan == '11')? 'selected' : ''?> value="11">November</option>
						<option <?= ($bulan == '12')? 'selected' : ''?> value="12">Desember</option>
				</select> &nbsp;
					<label class="sr-only" for="exampleInputPassword2">Tahun</label>
					<select name="tahun" class="form-control input-c">
						<?php for($i=date("Y");$i>=2018;$i--){?>
							<option <?= ($i == $tahun)? 'selected' : ''?> value="<?=$i?>"><?=$i?></option>
						<?php } ?>
				</select> &nbsp;
					<button type="submit" class="btn white">Filter</button>
				</form>
			</div>
			<?php $uri = $this->uri->segment(3);?>
			<div class="box-body">
				<a href="{site_url}tjual_rekapitulasi/index/0"><button type="button" class="btn btn-default <?= ($uri == '0' ? 'active' : '')?>">SELURUH STATUS</button></a>
				<a href="{site_url}tjual_rekapitulasi/index/1"><button type="button" id="menunggu" class="btn btn-default <?= ($uri == '1' ? 'active' : '')?>">MENUNGGU</button></a>
				<a href="{site_url}tjual_rekapitulasi/index/2"><button type="button" class="btn btn-default <?= ($uri == '2' ? 'active' : '')?>">PROSES</button></a>
				<a href="{site_url}tjual_rekapitulasi/index/3"><button type="button" class="btn btn-default <?= ($uri == '3' ? 'active' : '')?>">EKSEKUSI / PACKING</button></a>
				<a href="{site_url}tjual_rekapitulasi/index/4"><button type="button" class="btn btn-default <?= ($uri == '4' ? 'active' : '')?>">PENGIRIMAN</button></a>
				<a href="{site_url}tjual_rekapitulasi/index/5"><button type="button" class="btn btn-default <?= ($uri == '5' ? 'active' : '')?>">SELESAI</button></a>
				<a href="{site_url}tjual_rekapitulasi/index/6"><button type="button" class="btn btn-default <?= ($uri == '6' ? 'active' : '')?>">DIAMBIL PEMBELI</button></a>
			</div>
		</div>
		<div style="padding-top:0px;padding:1rem">
			<div class="table-responsive"> 
				<table id="datatable-kulcimart" class="table v-middle p-0 m-0 box" data-plugin="dataTable"> 
					<thead> 
						<tr style="text-transform: uppercase;background-color:#315fae;color:#ffffff;"> 
							<th>No</th>
							<th hidden>idtranss</th>
							<th>ID Transaksi</th>
							<th>Tanggal Transaksi</th>
							<!-- <th>ID Pembeli</th> -->
							<th>Nama Pembeli</th> 
							<th>Ekspedisi</th> 
							<th>Total Harga</th>
							<th>Biaya Ekspedisi</th>
							<th>Total Bayar</th>
							<th width="50%">Alamat Tujuan Kirim</th>
							<th>Status Transaksi</th>
							<th hidden>status</th>
						</tr> 
					</thead> 
					<tbody> 
						<? $number=0; ?> 
						<? foreach($list_index as $r) : ?> 
						<? $number = $number + 1; ?>
						<tr> 
							<td><?= $number ?></td>  
							<td hidden><?= $r->idtransaksi ?></td>
							<td><a href="#" class="getDetailTransaksi" style="color:blue;" data-toggle="modal" data-target="#rekapitulasi"><?= $r->idtransaksi ?></a></td>
							<td><?= $r->tanggaltransaksi?></td>
							<!-- <td><?= $r->idanggota?></td> -->
							<td><?= get_by_field('idanggota',$r->idanggota,'manggota')->namaanggota ?></td>
							<td><?= get_by_field('idekspedisi', $r->idekspedisi, 'mekspedisi')->namaekspedisi ?></td>
							<td><?= number_format($r->totalharga)?></td>
							<td><?= number_format($r->biayaekspedisi)?></td>
							<td><?= number_format($r->totalbayar)?></td>
							<!-- <td><?= get_all_where('manggotaalamattujuan', array('idanggota' => $r->idanggota, 'nourut' => $r->nourutalamat))->alamat ?></td> -->
							<td><?= get_all_where('manggota', array('idanggota' => $r->idanggota))->alamat ?></td>
							<!-- <td><?= get_all_where('manggota', array('idanggota' => $r->idanggota))->alamat ?></td> -->
							<td align="center"><?= statusPenjualan($r->statusproses)?></td>
							<td hidden><?= $r->statusproses?></td>
						</tr> 
						<? endforeach ?>
					</tbody> 
				</table> 
			</div> 
		</div> 
	</div> 
</div>

<!-- Modal Rekapitulasi -->
<div id="rekapitulasi" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md" style="max-width: 1100px;">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h4 class="modal-title" style="color: white;">Detail Transaksi</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">

      	<div class="box col-12" style="float:left;">
			<table class="table table-hover b-t">
				<thead>
					<tr>
						<th>Foto Produk</th>
						<th>Produk</th>
						<th>Kategori</th>
						<th>Harga</th>
						<th>Jumlah</th>
						<th>Total</th>
						<th>Catatan</th>
					</tr>
				</thead>
				<tbody id="detailProduk">
				</tbody>
			</table>
      	</div>

		  
		<div class="box col-12" style="float:left;">
		<!-- <div class="box col-4" style="float:left;"> -->
			<table class="table table-hover b-t">
				<thead></thead>
				<tbody>
					<tr>
						<td width="50%" align="center">
							<hr color="#000" size="5" width="100%" style="margin-top:0px;">
							<b class="totalPembayaran"></b>
							<hr color="#000" size="5" width="100%" style="margin-bottom:0px;">
						</td>
						<td width="50%" align="center">
							<hr color="#000" size="5" width="100%" style="margin-top:0px;margin-bottom:9px;">
							<div class="bukTrans"></div>
							<hr color="#000" size="5" width="100%" style="margin-bottom:0px;margin-top:9px;">
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box col-6 buktiTransaksix" style="float:left;display:none;">
			<table class="table table-hover b-t">
				<thead></thead>
				<tbody>
					<tr>
						<td width="100%" align="center">
							<!-- <hr color="#000" size="5" width="100%" style="margin-top:0px;"> -->
							<b>FOTO BUKTI TRANSAKSI</b>
							<!-- <hr color="#000" size="5" width="100%" style="margin-bottom:0px;"> -->
						</td>
					</tr>
					<tr>
						<td height="270px" class="fotoBuktiTransfer">&nbsp;</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box col-6 buktiTransaksix" style="float:left;display:none;">
			<table class="table table-hover b-t">
				<thead></thead>
				<tbody>
					<tr>
						<td hidden>&nbsp;</td>
						<td hidden>&nbsp;</td>
					</tr>
					<tr>
						<td width="100%" align="center" colspan="2">
							<!-- <hr color="#000" size="5" width="100%" style="margin-top:0px;"> -->
							<b>DETAIL BUKTI TRANSAKSI</b>
							<!-- <hr color="#000" size="5" width="100%" style="margin-bottom:0px;"> -->
						</td>
					</tr>
					<tr>
						<td width="50%" colspan="1">
							Id Bukti Transfer
						</td>
						<td width="50%" colspan="1" class="idBuktiTransfer">
							:&nbsp;
						</td>
					</tr>
					<tr>
						<td width="50%" colspan="1">
							Tanggal Bayar
						</td>
						<td width="50%" colspan="1" class="tanggalBayar">
							:&nbsp;
						</td>
					</tr>
					<tr>
						<td width="50%" colspan="1">
							Nominal Bayar
						</td>
						<td width="50%" colspan="1" class="nominalBayar">
							:&nbsp;
						</td>
					</tr>
					<tr>
						<td width="50%" colspan="1">
							Nama Bank
						</td>
						<td width="50%" colspan="1" class="namaBank">
							:&nbsp;
						</td>
					</tr>
					<tr>
						<td width="50%" colspan="1">
							No Rekening
						</td>
						<td width="50%" colspan="1" class="noRekening">
							:&nbsp;
						</td>
					</tr>
					<tr>
						<td width="50%" colspan="1" style="height:300px;">
							Atas Nama
						</td>
						<td width="50%" colspan="1" class="atasNama">
							:&nbsp;
						</td>
					</tr>
				</tbody>
			</table>
		</div>

	  </div>
      <div class="clearfix"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
	<!-- End Modal content-->
  </div>
</div>
<!-- EOF Modal Rekapitulasi -->

<script type="text/javascript"> 
	$('#datatable-kulcimart').DataTable({
		scrollx:true
	});
</script>

<script type="text/javascript">

	function number_format (number, decimals, decPoint, thousandsSep) {
      number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
      var n = !isFinite(+number) ? 0 : +number
      var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
      var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
      var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
      var s = ''

      var toFixedFix = function (n, prec) {
        var k = Math.pow(10, prec)
        return '' + (Math.round(n * k) / k)
          .toFixed(prec)
      }

       s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
       if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
       }
       if ((s[1] || '').length < prec) {
        s[1] = s[1] || ''
        s[1] += new Array(prec - s[1].length + 1).join('0')
       }
     return s.join(dec)
    }

	$(document).on('click', '.buktiTransaksi',function(){
		$('.buktiTransaksix').toggle();
	});
    
	var catatan;
	function cekCatatan(catatan){
		if(catatan != '' || catatan != null){
			return catatan;
		}else{
			return '-';
		}
	}

	$(document).on('click', '.getDetailTransaksi', function(){
		$('.buktiTransaksix').hide();
		var idtransaksi = ($(this).closest('tr').find("td:eq(1)").html());
		var status = ($(this).closest('tr').find("td:eq(11)").html());
		$.ajax({
			url:'{site_url}tjual_rekapitulasi/getDetailProdukTrans/'+idtransaksi,
			dataType:'json',
			success:function(data){
				$('#detailProduk').empty();
				for(var i = 0; i < data.length; i++){
					var tabel = '<tr>';
						tabel += '<td><img src="{gambarProduk}/kecil/'+data[i].fotoproduk+'"></td>';
						tabel += '<td>'+data[i].namaproduk+'</td>';
						tabel += '<td>'+data[i].kategori+'</td>';
						tabel += '<td>'+number_format(data[i].harga)+'</td>';
						tabel += '<td>'+data[i].jumlah+'</td>';
						tabel += '<td>'+number_format(data[i].total)+'</td>';
						tabel += '<td>'+cekCatatan(data[i].catatan)+'</td>';
						tabel += '</tr>';
					$('#detailProduk').append(tabel);
				}
			}
		});
		$.ajax({
			url:'{site_url}tjual_rekapitulasi/totalBayar/'+idtransaksi,
			dataType:'json',
			success:function(data){
				$('.totalPembayaran').html('TOTAL PEMBAYARAN : '+number_format(data[0].total));
			}
		});
		if(status == '1'){
			$('.bukTrans').html('<button class="btn btn-danger">~ Bukti Transaksi Tidak Ada ~</button>');
		}else{
			$('.bukTrans').html('<button class="btn btn-primary buktiTransaksi"><i class="fa fa-search"></i> Bukti Transaksi</button>');
			$.ajax({
				url:'{site_url}tjual_rekapitulasi/buktiTransfer/'+idtransaksi,
				dataType:'json',
				success:function(data){
					$('.idBuktiTransfer').html(': '+data[0].idtransaksibayar);
					$('.tanggalBayar').html(': '+data[0].tanggalbayar);
					$('.nominalBayar').html(': '+number_format(data[0].nominaltransaksi));
					$('.namaBank').html(': '+data[0].namabank);
					$('.noRekening').html(': '+data[0].norekening);
					$('.atasNama').html(': '+data[0].atasnama);
					$('.fotoBuktiTransfer').html('<img src="https://member.kulcimart.com/assets/buktiverifbayar/'+data[0].fotobuktitransfer+'" style="width:478px;height:500px;">');
				}
			});
		}
	});

</script>