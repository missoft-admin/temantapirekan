<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tjual_prosespacking_model extends CI_Model {

	function getAll(){
		$this->db->select('tjual.*');
		$this->db->order_by('statusproses', 'asc');
		$this->db->where('idanggotapartner', $this->session->userdata('idanggota'));
		$this->db->where('statusproses','2');
		return $this->db->get('tjual')->result();
	}

	function prosesPacking($id){
		$data = array('statusproses' => 3);
		$this->db->set($data);
		$this->db->where('idtransaksi', $id);
		return $this->db->update('tjual');
	}

	function prosesSelesai($id){
		$data = array('statusproses' => 6);
		$this->db->set($data);
		$this->db->where('idtransaksi', $id);
		return $this->db->update('tjual');
	}

	function getDetailTransaksi($id){
		$this->db->select('tjualdetail.*, manggotaproduk.foto');
		$this->db->join('manggotaproduk', 'manggotaproduk.idproduk = tjualdetail.idproduk', 'inner');
		$this->db->where('tjualdetail.idtransaksi',$id);
		return $this->db->get('tjualdetail')->result();
	}

	function getSum($id){
		$query = "SELECT SUM(tjualdetail.jumlahjual * tjualdetail.hargajual) as total FROM `tjualdetail` WHERE idtransaksi = '".$id."'";
		return $this->db->query($query)->result();
	}

}

/* End of file Tjual_prosespacking_model.php */
/* Location: ./application/models/Tjual_prosespacking_model.php */