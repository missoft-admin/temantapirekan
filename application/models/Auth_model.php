<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {

	public function signIn($username, $password)
    {
        $this->db->where('username', $username);
        $this->db->where('passkey', hash('sha256',md5($password)));
        $this->db->where('tipeanggota', '2');
        $query = $this->db->get('manggota');

        if ($query->num_rows() > 0) {
            $row = $query->row();
            $data = array(
                      'idanggota'         => $row->idanggota,
                      'namaanggota'       => $row->namaanggota,
                      'username'   		  => $row->username,
                      'passkey'   		  => $row->passkey,
                      'logged_in'         => true,
                    );
            $this->session->set_userdata($data);
            return true;
        } else {
            return false;
        }
    }

    public function signOut()
    {
        $data = array(
                   'idanggota'         	=> $this->session->userdata('idanggota'),
                   'namaanggota'     	=> $this->session->userdata('namaanggota'),
                   'username'       	=> $this->session->userdata('username'),
                   'passkey'   			=> $this->session->userdata('passkey'),
                   'logged_in'       	=> $this->session->userdata('logged_in'),
                );
        $this->session->unset_userdata($data);
        $this->session->sess_destroy();
    }

    function getPassword(){
      $this->db->where('idanggota', $this->session->userdata('idanggota'));
      return $this->db->get('manggota')->row();
    }

    function updatePass($passwordbaru){
      $data = array('passkey' => hash('sha256',md5($passwordbaru)));
      $this->db->set($data);
      $this->db->where('idanggota', $this->session->userdata('idanggota'));
      return $this->db->update('manggota');
    }

}

/* End of file Auth_model.php */
/* Location: ./application/models/Auth_model.php */