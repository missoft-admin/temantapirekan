<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manggota_produk_model extends CI_Model {

	function getAll(){
		$this->db->where('idanggota',$this->session->userdata('idanggota'));
		return $this->db->get('manggotaproduk')->result();
	}

	function getId($id){
		$this->db->where('idproduk', $id);
		$this->db->where('idanggota', $this->session->userdata('idanggota'));
		return $this->db->get('manggotaproduk')->row();
	}

	function getKode(){
		$this->db->where('idanggota',$this->session->userdata('idanggota'));
		return $this->db->get('manggotaproduk')->num_rows();
	}

	function saveProduk($data){
		return $this->db->insert('manggotaproduk',$data);
	}

	function nonaktifProduk($id){
		$data = array('sttampil' => 0,'sthotdeals' => 0,'sttampilhotdeals' => 0);
		$this->db->set($data);
		$this->db->where('idproduk', $id);
		$this->db->where('idanggota', $this->session->userdata('idanggota'));
		return $this->db->update('manggotaproduk');
	}

	function aktifProduk($id){
		$data = array('sttampil' => 1);
		$this->db->set($data);
		$this->db->where('idproduk', $id);
		$this->db->where('idanggota', $this->session->userdata('idanggota'));
		return $this->db->update('manggotaproduk');
	}

	function hotDeals($id){
		$data = array('sthotdeals' => 1, 'sttampilhotdeals' => 1);
		$this->db->set($data);
		$this->db->where('sttampil', '1');
		$this->db->where('idproduk', $id);
		$this->db->where('idanggota', $this->session->userdata('idanggota'));
		return $this->db->update('manggotaproduk');
	}

	function hotDealsPending($id){
		$data = array('sthotdeals' => 1, 'sttampilhotdeals' => 0);
		$this->db->set($data);
		$this->db->where('sttampil', '1');
		$this->db->where('idproduk', $id);
		$this->db->where('idanggota', $this->session->userdata('idanggota'));
		return $this->db->update('manggotaproduk');
	}

	function hotDealsNonaktif($id){
		$data = array('sthotdeals' => 0, 'sttampilhotdeals' => 0);
		$this->db->set($data);
		$this->db->where('sttampil', '1');
		$this->db->where('idproduk', $id);
		$this->db->where('idanggota', $this->session->userdata('idanggota'));
		return $this->db->update('manggotaproduk');
	}

	function updateProduk($id,$data){
		$this->db->set($data);
		$this->db->where('idanggota', $this->session->userdata('idanggota'));
		$this->db->where('idproduk', $id);
		return $this->db->update('manggotaproduk');
	}

	function getSettingHarga(){
		return $this->db->get('msettingharga', 1)->row();
	}

	function cekJumlahTampil(){
		$this->db->where('idanggota',$this->session->userdata('idanggota'));
		$this->db->where('sttampil','1');
		return $this->db->get('manggotaproduk')->num_rows();
	}

	function cekJenisAnggota(){
		$this->db->where('idanggota',$this->session->userdata('idanggota'));
		return $this->db->get('manggota')->row()->jenisanggota;
	}

}

/* End of file Manggota_produk_model.php */
/* Location: ./application/models/Manggota_produk_model.php */
