<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tjual_packingpengiriman_model extends CI_Model {

	function getAll(){
		$this->db->select('tjual.*');
		$this->db->order_by('statusproses', 'asc');
		$this->db->where('idanggotapartner', $this->session->userdata('idanggota'));
		$this->db->where('statusproses','3');
		return $this->db->get('tjual')->result();
	}

	function prosesPengiriman($id){
		$data = array('statusproses' => 4);
		$this->db->set($data);
		$this->db->where('idtransaksi', $id);
		return $this->db->update('tjual');
	}

}

/* End of file Tjual_packingpengiriman_model.php */
/* Location: ./application/models/Tjual_packingpengiriman_model.php */