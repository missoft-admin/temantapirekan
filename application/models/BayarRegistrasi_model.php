<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BayarRegistrasi_model extends CI_Model {

		function getKode(){
			$this->db->where('idanggotapartner', $this->session->userdata('idanggota'));
			return $this->db->get('tbayarregistrasipartner')->num_rows();
		}

		function simpanBayaran($data){
			return $this->db->insert('tbayarregistrasipartner', $data);
		}

		function getDetail(){
			$this->db->select('tbayarregistrasipartnerdetail.idbayarregpartner, tbayarregistrasipartner.tanggalbayar, tbayarregistrasipartnerdetail.periodebayar, tbayarregistrasipartnerdetail.nominaltransaksi, tbayarregistrasipartnerdetail.jenispartner');
			$this->db->join('tbayarregistrasipartnerdetail', 'tbayarregistrasipartnerdetail.idbayarregpartner = tbayarregistrasipartner.idbayarregpartner', 'inner');
			$this->db->where('idanggotapartner', $this->session->userdata('idanggota'));
			return $this->db->get('tbayarregistrasipartner')->result();
		}

		function getSumDetail(){
			$this->db->select('SUM(tbayarregistrasipartnerdetail.nominaltransaksi) as nominal');
			$this->db->join('tbayarregistrasipartnerdetail', 'tbayarregistrasipartnerdetail.idbayarregpartner = tbayarregistrasipartner.idbayarregpartner', 'inner');
			$this->db->where('idanggotapartner', $this->session->userdata('idanggota'));
			return $this->db->get('tbayarregistrasipartner')->row()->nominal;
		}

		function getRegistrasi(){
			$this->db->where('idanggotapartner', $this->session->userdata('idanggota'));
			return $this->db->get('tbayarregistrasipartner')->result();
		}

}

/* End of file BayarRegistrasi_model.php */
/* Location: ./application/models/BayarRegistrasi_model.php */