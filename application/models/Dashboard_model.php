<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
    
    public function getTBonus()
    {
        $this->db->select('SUM(tjual.bsponsor) AS tbsponsor,
          SUM(tjual.bpartner1) AS tbpartner1,
          SUM(tjual.bpartner2) AS tbpartner2,
          SUM(tjual.bloyalti) AS tbloyalti');
        $this->db->where('tjual.periodetransaksi', date("Ym"));
        $query = $this->db->get('tjual');
        return $query->row();
    }

    public function getTransaksiTerbanyak($start)
    {
        $this->db->select('tjual.idanggota, manggota.namaanggota, COUNT(tjual.idtransaksi) AS totaltransaksis');
        $this->db->join('manggota', 'manggota.idanggota = tjual.idanggota');
        $this->db->where('tjual.periodetransaksi', date("Ym"));
        $this->db->order_by('COUNT(tjual.idtransaksi)', 'DESC');
        $this->db->group_by('tjual.idanggota');
        $this->db->limit(5, $start);
        $query = $this->db->get('tjual');
        return $query->result();
    }

    function orderPenjualan(){
        $query = "SELECT
                    * 
                FROM
                    datapenjualan_detail 
                WHERE
                    periodetransaksi = '".date('Ym')."' 
                    AND idanggotapartner = '".$this->session->userdata('idanggota')."' 
                    AND (statusproses = '1' OR statusproses = '2') 
                ORDER BY
                    tanggaltransaksi,
                    totalbayar DESC 
                    LIMIT 0,10";
        return $this->db->query($query);
    }

    function produkPenjualanTerbanyak(){
        $query = "SELECT tjual.*, SUM(jumlahjual) as total_jual, tjualdetail.idproduk, manggotaproduk.foto
                    FROM `tjual` 
                    INNER JOIN tjualdetail ON tjualdetail.idtransaksi = tjual.idtransaksi 
                    INNER JOIN manggotaproduk ON manggotaproduk.idproduk = tjualdetail.idproduk
                    WHERE idanggotapartner = '".$this->session->userdata('idanggota')."'
                    GROUP BY tjualdetail.idproduk
                    ORDER BY total_jual DESC LIMIT 10";
        return $this->db->query($query);
    }

    function produkPenjualanSedikit(){
        $query = "SELECT tjual.*, SUM(jumlahjual) as total_jual, tjualdetail.idproduk, manggotaproduk.foto
                    FROM `tjual` 
                    INNER JOIN tjualdetail ON tjualdetail.idtransaksi = tjual.idtransaksi 
                    INNER JOIN manggotaproduk ON manggotaproduk.idproduk = tjualdetail.idproduk
                    WHERE idanggotapartner = '".$this->session->userdata('idanggota')."'
                    GROUP BY tjualdetail.idproduk
                    ORDER BY total_jual ASC LIMIT 10";
        return $this->db->query($query);
    }

    function produkTerbanyakPendapatan(){
        $query = "SELECT tjual.*, tjualdetail.idproduk, tjualdetail.jumlahjual, tjualdetail.hargajual, SUM(tjualdetail.jumlahjual * tjualdetail.hargajual) As nominalpendapatan     FROM tjual
            INNER JOIN tjualdetail ON(tjualdetail.idtransaksi=tjual.idtransaksi)
            WHERE idanggotapartner = '".$this->session->userdata('idanggota')."'
            GROUP BY tjualdetail.idproduk
            ORDER BY nominalpendapatan DESC LIMIT 10";

        return $this->db->query($query);
    }

    function produkSedikitPendapatan(){
        $query = "SELECT tjual.*, tjualdetail.idproduk, tjualdetail.jumlahjual, tjualdetail.hargajual, SUM(tjualdetail.jumlahjual * tjualdetail.hargajual) As nominalpendapatan     FROM tjual
            INNER JOIN tjualdetail ON(tjualdetail.idtransaksi=tjual.idtransaksi)
            WHERE idanggotapartner = '".$this->session->userdata('idanggota')."'
            GROUP BY tjualdetail.idproduk
            ORDER BY nominalpendapatan ASC LIMIT 10";

        return $this->db->query($query);
    }

    function memberSponsorPembelianTerbanyak(){
        $query = "SELECT tjual.*, manggota.idsponsor, SUM(tjualdetail.hargajual * tjualdetail.jumlahjual) As totaltransaksi FROM tjual
            INNER JOIN manggota ON(manggota.idanggota=tjual.idanggota)
            INNER JOIN tjualdetail ON(tjualdetail.idtransaksi = tjual.idtransaksi)
            WHERE manggota.idsponsor = '".$this->session->userdata('idanggota')."'
            GROUP BY tjual.idanggota
            ORDER BY totaltransaksi DESC LIMIT 10";

        return $this->db->query($query);
    }

    function memberSponsorPembelianSedikt(){
        $query = "SELECT tjual.*, manggota.idsponsor, SUM(tjualdetail.hargajual * tjualdetail.jumlahjual) As totaltransaksi FROM tjual
            INNER JOIN manggota ON(manggota.idanggota=tjual.idanggota)
            INNER JOIN tjualdetail ON(tjualdetail.idtransaksi = tjual.idtransaksi)
            WHERE manggota.idsponsor = '".$this->session->userdata('idanggota')."'
            GROUP BY tjual.idanggota
            ORDER BY totaltransaksi ASC LIMIT 10";

        return $this->db->query($query);
    }

    function partnerSponsorPenjualanTerbanyak(){
        $query = "SELECT tjual.*, manggota.idsponsor, SUM(tjualdetail.hargajual * tjualdetail.jumlahjual) As totaltransaksi FROM tjual
            INNER JOIN manggota ON(manggota.idanggota=tjual.idanggota)
            INNER JOIN tjualdetail ON(tjualdetail.idtransaksi = tjual.idtransaksi)
            WHERE manggota.idsponsor = '".$this->session->userdata('idanggota')."'
            GROUP BY tjual.idanggotapartner
            ORDER BY totaltransaksi DESC LIMIT 10";

        return $this->db->query($query);
    }

    function partnerSponsorPenjualanSedikit(){
        $query = "SELECT tjual.*, manggota.idsponsor, SUM(tjualdetail.hargajual * tjualdetail.jumlahjual) As totaltransaksi FROM tjual
            INNER JOIN manggota ON(manggota.idanggota=tjual.idanggota)
            INNER JOIN tjualdetail ON(tjualdetail.idtransaksi = tjual.idtransaksi)
            WHERE manggota.idsponsor = '".$this->session->userdata('idanggota')."'
            GROUP BY tjual.idanggotapartner
            ORDER BY totaltransaksi ASC LIMIT 10";

        return $this->db->query($query);
    }


    // FILTER FUNCTION

}
