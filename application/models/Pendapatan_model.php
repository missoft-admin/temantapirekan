<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendapatan_model extends CI_Model {

	public function getSponsor(){
		$this->db->where('idanggota', $this->session->userdata('idanggota'));
		$this->db->where('tipebonus', '1');
		return $this->db->get('manggotabonus')->result();
	}

	public function getPartnership1(){
		$this->db->where('idanggota', $this->session->userdata('idanggota'));
		$this->db->where('tipebonus', '2');
		return $this->db->get('manggotabonus')->result();
	}

	public function getPartnership2(){
		$this->db->where('idanggota', $this->session->userdata('idanggota'));
		$this->db->where('tipebonus', '3');
		return $this->db->get('manggotabonus')->result();
	}

	public function getTotal(){
		$this->db->where('idanggota', $this->session->userdata('idanggota'));
		return $this->db->get('manggotabonus')->result();
	}

	public function sumSponsor(){
		$idanggota = $this->session->userdata('idanggota');
		$query = "SELECT SUM(nominalbonus) As nominalbonus FROM manggotabonus WHERE idanggota='".$idanggota."' AND tipebonus = '1'";
		return $this->db->query($query)->row();
	}

	public function sumPartnership1(){
		$idanggota = $this->session->userdata('idanggota');
		$query = "SELECT SUM(nominalbonus) As nominalbonus FROM manggotabonus WHERE idanggota='".$idanggota."' AND tipebonus = '2'";
		return $this->db->query($query)->row();
	}

	public function sumPartnership2(){
		$idanggota = $this->session->userdata('idanggota');
		$query = "SELECT SUM(nominalbonus) As nominalbonus FROM manggotabonus WHERE idanggota='".$idanggota."' AND tipebonus = '3'";
		return $this->db->query($query)->row();
	}

	public function sumTotal(){
		$idanggota = $this->session->userdata('idanggota');
		$query = "SELECT SUM(nominalbonus) As nominalbonus FROM manggotabonus WHERE idanggota='".$idanggota."'";
		return $this->db->query($query)->row();
	}

	public function getLaporan($id){
		if($id != '0'){
			$this->db->select('tjual.*');
			$this->db->order_by('statusproses', 'asc');
			$this->db->where('idanggotapartner', $this->session->userdata('idanggota'));
			$this->db->where('periodetransaksi',$id);
			$this->db->where('statusproses', '5');
			$this->db->or_where('statusproses', '6');
			// $this->db->where('statusproses', '6');
			return $this->db->get('tjual')->result();
		}else{
			$date = ''.$this->input->post('tahun').''.$this->input->post('bulan').'';
			$this->db->select('tjual.*');
			$this->db->order_by('statusproses', 'asc');
			$this->db->where('idanggotapartner', $this->session->userdata('idanggota'));
			$this->db->where('periodetransaksi',$date);
			$this->db->where('statusproses', '5');
			$this->db->or_where('statusproses', '6');
			return $this->db->get('tjual')->result();
		}
	}

	public function getJumlahTransaksi($id){
		if($id != '0'){
			$this->db->select('tjual.*');
			$this->db->order_by('statusproses', 'asc');
			$this->db->where('idanggotapartner', $this->session->userdata('idanggota'));
			$this->db->where('periodetransaksi',$id);
			$this->db->where('statusproses', '5');
			$this->db->or_where('statusproses', '6');
			// $this->db->where('statusproses', '6');
			return $this->db->get('tjual')->num_rows();
		}else{
			$date = ''.$this->input->post('tahun').''.$this->input->post('bulan').'';
			$this->db->select('tjual.*');
			$this->db->order_by('statusproses', 'asc');
			$this->db->where('idanggotapartner', $this->session->userdata('idanggota'));
			$this->db->where('periodetransaksi',$date);
			$this->db->where('statusproses', '5');
			$this->db->or_where('statusproses', '6');
			return $this->db->get('tjual')->num_rows();
		}
	}

	public function getTotalNominalTransaksi($id){
		if($id != '0'){
			$this->db->select('tjual.*, SUM(tjual.totalbayar) as totaltransaksi');
			$this->db->order_by('statusproses', 'asc');
			$this->db->where('idanggotapartner', $this->session->userdata('idanggota'));
			$this->db->where('periodetransaksi',$id);
			$this->db->where('statusproses', '5');
			$this->db->or_where('statusproses', '6');
			// $this->db->where('statusproses', '6');
			return $this->db->get('tjual')->row()->totaltransaksi;
		}else{
			$date = ''.$this->input->post('tahun').''.$this->input->post('bulan').'';
			$this->db->select('tjual.*, SUM(tjual.totalbayar) as totaltransaksi');
			$this->db->order_by('statusproses', 'asc');
			$this->db->where('idanggotapartner', $this->session->userdata('idanggota'));
			$this->db->where('periodetransaksi',$date);
			$this->db->where('statusproses', '5');
			$this->db->or_where('statusproses', '6');
			return $this->db->get('tjual')->row()->totaltransaksi;
		}
	}

}

/* End of file Pendapatan_model.php */
/* Location: ./application/models/Pendapatan_model.php */