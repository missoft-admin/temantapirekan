<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Referensi_ekspedisi_model extends CI_Model {

	function getAll(){
		$this->db->select('manggotaekspedisiharga.idekspedisi, manggotaekspedisiharga.harga, mprovinsi.nama as nama_provinsi, ,mprovinsi.id as idprovinsi, mkota.nama as nama_kota, mkota.id as idkota, mkecamatan.nama as nama_kecamatan, mkecamatan.id as idkecamatan, mdesa.nama as nama_desa, mdesa.id as iddesa');
		$this->db->join('mprovinsi', 'mprovinsi.id = manggotaekspedisiharga.provinsi', 'left');
		$this->db->join('mkota', 'mkota.id = manggotaekspedisiharga.kota', 'left');
		$this->db->join('mkecamatan', 'mkecamatan.id = manggotaekspedisiharga.kecamatan', 'left');
		$this->db->join('mdesa', 'mdesa.id = manggotaekspedisiharga.desa', 'left');
		$this->db->where('manggotaekspedisiharga.idanggota', $this->session->userdata('idanggota'));
		return $this->db->get('manggotaekspedisiharga')->result();
	}

	function saveEkspedisi($data){
		return $this->db->insert('manggotaekspedisi', $data);
	}
	function saveDetail(){
		return $this->db->insert('manggotaekspedisiharga');
	}

	function updateHarga(){
		$this->db->where('idanggota', $this->session->userdata('idanggota'));
		$this->db->where('provinsi', $this->input->post('idprovinsi'));
		$this->db->where('kota', $this->input->post('idkota'));
		$this->db->where('kecamatan', $this->input->post('idkecamatan'));
		$this->db->where('desa', $this->input->post('iddesa'));
		return $this->db->update('manggotaekspedisiharga', array('harga' => RemoveComma($this->input->post('hargaBaru'))));
	}

	function hapusEkspedisi($provinsi,$kota,$kecamatan,$desa){
		$this->db->where('idanggota', $this->session->userdata('idanggota'));
		$this->db->where('provinsi', $provinsi);
		$this->db->where('kota', $kota);
		$this->db->where('kecamatan', $kecamatan);
		$this->db->where('desa', $desa);
		return $this->db->delete('manggotaekspedisiharga');
	}

	function cekData(){
		$this->db->where('idanggota', $this->session->userdata('idanggota'));
		return $this->db->get('manggotaekspedisi')->num_rows();
	}

	function getKecDes($kota){
		$query = "SELECT
						mkecamatan.id as idkecamatan,
						mkecamatan.nama as namakecamatan,
						mkecamatan.kotaid,
						mdesa.id as iddesa,
						mdesa.nama as namadesa,
						mdesa.kecamatanid
					FROM
						mkecamatan
						LEFT JOIN mdesa ON ( mkecamatan.id = mdesa.kecamatanid ) 
					WHERE
						mkecamatan.kotaid = '".$kota."'";
		return $this->db->query($query)->result();
	}

	function getkotKecDes($provinsi){
		$query = "SELECT
						mkota.id AS idkota,
						mkota.nama AS namakota,
						mkecamatan.id AS idkecamatan,
						mkecamatan.nama AS namakecamatan,
						mkecamatan.kotaid,
						mdesa.id AS iddesa,
						mdesa.nama AS namadesa,
						mdesa.kecamatanid 
					FROM
						mkota
						LEFT JOIN mkecamatan ON ( mkecamatan.kotaid = mkota.id )
						LEFT JOIN mdesa ON ( mkecamatan.id = mdesa.kecamatanid ) 
					WHERE
						mkota.provinsiid = '".$provinsi."'";
		return $this->db->query($query)->result();
	}

	function getDes($kecamatan){
		$query = "SELECT
						* 
					FROM
						mdesa 
					WHERE
						kecamatanid = '".$kecamatan."'";
		return $this->db->query($query)->result();

	}

}

/* End of file Referensi_ekspedisi_model.php */
/* Location: ./application/models/Referensi_ekspedisi_model.php */