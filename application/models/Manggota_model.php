<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manggota_model extends CI_Model {

	function getAll(){
		$this->db->where('idanggota', $this->session->userdata('idanggota'));
		return $this->db->get('manggota')->result();
	}

	function edit(){
		$data = array();
		$data['noktp'] = $this->input->post('noktp');
		$data['namaanggota'] = $this->input->post('namaanggota');
		$data['alamat'] = $this->input->post('alamat');
		$data['desa'] = $this->input->post('desa');
		$data['kecamatan'] = $this->input->post('kecamatan');
		$data['kota'] = $this->input->post('kota');
		$data['kodepos'] = $this->input->post('kodepos');
		$data['telepon'] = $this->input->post('telepon');
		$data['hp'] = $this->input->post('hp');
		$data['hpwa'] = $this->input->post('hpwa');
		$data['tempatlahir'] = $this->input->post('tempatlahir');
		$data['tanggallahir'] = $this->input->post('tanggallahir');
		$data['agama'] = $this->input->post('agama');
		$data['jeniskelamin'] = $this->input->post('jeniskelamin');
		$data['statuspernikahan'] = $this->input->post('statuspernikahan');
		$data['email'] = $this->input->post('email');
		$this->db->set($data);
		$this->db->where('idanggota',$this->session->userdata('idanggota'));
		return $this->db->update('manggota');
	}

	function editRek($data){
		$this->db->where('idanggota',$this->session->userdata('idanggota'));
		return $this->db->update('manggota', $data);
	}

	function getPetaMember($id){
		if($id == 0){
			$query = "SELECT tjual.*, manggota.idsponsor, manggota.namaanggota, manggota.alamat, SUM(tjualdetail.hargajual * tjualdetail.jumlahjual) As nominaltransaksi FROM tjual
			LEFT JOIN manggota ON(manggota.idanggota=tjual.idanggota)
			LEFT JOIN tjualdetail ON(tjualdetail.idtransaksi=tjual.idtransaksi)
			WHERE manggota.idsponsor = '".$this->session->userdata('idanggota')."'
			AND tjual.periodetransaksi = '".date('Ym')."'
			GROUP BY tjual.idanggota
			ORDER BY nominaltransaksi DESC";
		}else{
			$periode = $this->input->post('tahun').$this->input->post('bulan');
			$query = "SELECT tjual.*, manggota.idsponsor, manggota.namaanggota, manggota.alamat, SUM(tjualdetail.hargajual * tjualdetail.jumlahjual) As nominaltransaksi FROM tjual
			LEFT JOIN manggota ON(manggota.idanggota=tjual.idanggota)
			LEFT JOIN tjualdetail ON(tjualdetail.idtransaksi=tjual.idtransaksi)
			WHERE manggota.idsponsor = '".$this->session->userdata('idanggota')."'
			AND tjual.periodetransaksi = '".$periode."'
			GROUP BY tjual.idanggota
			ORDER BY nominaltransaksi DESC";
		}
		return $this->db->query($query)->result();
	}

	function getPetaPartner($id){
		if($id == 0){
			$query = "SELECT tjual.*, manggota.idsponsor, manggota.namaanggota, manggota.alamat, SUM(tjualdetail.hargajual * tjualdetail.jumlahjual) As nominaltransaksi FROM tjual
			LEFT JOIN manggota ON(manggota.idanggota=tjual.idanggota)
			LEFT JOIN tjualdetail ON(tjualdetail.idtransaksi=tjual.idtransaksi)
			WHERE manggota.idsponsor = '".$this->session->userdata('idanggota')."'
			AND tjual.periodetransaksi = '".date('Ym')."'
			AND tjual.idanggotapartner NOT IN ( SELECT idanggota FROM manggota WHERE idanggota = '".$this->session->userdata('idanggota')."' )
			GROUP BY tjual.idanggotapartner
			ORDER BY nominaltransaksi DESC";
		}else{
			$periode = $this->input->post('tahun').$this->input->post('bulan');
			$query = "SELECT tjual.*, manggota.idsponsor, manggota.namaanggota, manggota.alamat, SUM(tjualdetail.hargajual * tjualdetail.jumlahjual) As nominaltransaksi FROM tjual
			LEFT JOIN manggota ON(manggota.idanggota=tjual.idanggota)
			LEFT JOIN tjualdetail ON(tjualdetail.idtransaksi=tjual.idtransaksi)
			WHERE manggota.idsponsor = '".$this->session->userdata('idanggota')."'
			AND tjual.periodetransaksi = '".$periode."'
			AND tjual.idanggotapartner NOT IN ( SELECT idanggota FROM manggota WHERE idanggota = '".$this->session->userdata('idanggota')."' )
			GROUP BY tjual.idanggotapartner
			ORDER BY nominaltransaksi DESC";
		}
		return $this->db->query($query)->result();
	}

	function tutupToko($id){
		$this->db->where('idanggota', $id);
		return $this->db->update('manggota', array('staktif' => '2'));
	}

	function bukaToko($id){
		$this->db->where('idanggota', $id);
		return $this->db->update('manggota', array('staktif' => '1'));
	}

	private function _cekData(){
		$this->db->where('idanggota', $this->session->userdata('idanggota'));
		return $this->db->get('manggotaalamattujuan')->num_rows();
	}

	function tambahAlamat($provinsi,$kota,$kecamatan,$desa,$lat,$long,$alamat,$kodepos){
		$data = array();
		$data['idanggota'] = $this->session->userdata('idanggota');
		$data['nourut'] = $this->_cekData() + 1;
		$data['alamat'] = $alamat;
		$data['desa'] = $desa;
		$data['kecamatan'] = $kecamatan;
		$data['kota'] = $kota;
		$data['provinsi'] = $provinsi;
		$data['kodepos'] = $kodepos;
		$data['lat'] = $lat;
		$data['long'] = $long;
		$data['staktif'] = '1';
		$data['stutama'] = '0';

		return $this->db->insert('manggotaalamattujuan', $data);
	}

	function get_alamat_tujuan($nourut){
		$this->db->where('nourut',$nourut);
		$this->db->where('idanggota',$this->session->userdata('idanggota'));
		return $this->db->get('manggotaalamattujuan')->row();
	}

	function update_informasi($data){
		$this->db->where('idanggota',$this->session->userdata('idanggota'));
		return $this->db->update('manggota',$data);
	}

	function clear_stutama(){
		$this->db->where('idanggota',$this->session->userdata('idanggota'));
		$this->db->where('stutama','1');
		return $this->db->update('manggotaalamattujuan',array('stutama' => '0'));
	}

	function update_stutama_alamat($nourut){
		$this->db->where('idanggota',$this->session->userdata('idanggota'));
		$this->db->where('nourut',$nourut);
		return $this->db->update('manggotaalamattujuan',array('stutama' => '1'));
	}

}

/* End of file Manggota_model.php */
/* Location: ./application/models/Manggota_model.php */