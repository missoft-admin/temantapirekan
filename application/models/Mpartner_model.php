<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: IYAN ISYANTO
|--------------------------------------------------------------------------
|
*/

class Mpartner_model extends CI_Model {

	private $table = 'manggota';

    // public function getAll() {
    // 	$this->db->where('tipeanggota', 2);
    //     $this->db->order_by('jenisanggota,staktif,namaanggota', 'desc');
    //     $query = $this->db->get_where($this->table);
    //     return $query->result();
    // }

    public function getPartnerInformasi($idPartner) {
    	$this->db->where('idanggota', $idPartner);
    	return $this->db->get($this->table)->row_array();
    }

    public function saveInformasi() {
        $post = $this->input->post();
        $data = array();
		// $persenHarga = floatval($this->Manggota_produk_model->getSettingHarga()->persenharga);
		$widthex=explode(".",$this->input->post('reswidth'));
		$heightex=explode(".",$this->input->post('resheight'));
		$x_axis=explode(".",$this->input->post('x_axis'));
		$y_axis=explode(".",$this->input->post('y_axis'));
		$namafotoprofil = $this->session->userdata('idanggota').'-'.date('d-m-Y H-m-s');
        
        if($_FILES['fotoprofil']['name'] !== '') { // FOTO PROFIL
            $pijat_plus_plus = get_by_field('idanggota', $this->session->userdata('idanggota'), 'manggota')->fotoprofil;

            if(is_file(FCPATH.'assets/files/anggota/besar/'.$pijat_plus_plus) && is_file(FCPATH.'assets/files/anggota/sedang/'.$pijat_plus_plus) && is_file(FCPATH.'assets/files/anggota/kecil/'.$pijat_plus_plus)){
                unlink(FCPATH.'assets/files/anggota/besar/'.$pijat_plus_plus);
                unlink(FCPATH.'assets/files/anggota/sedang/'.$pijat_plus_plus);
                unlink(FCPATH.'assets/files/anggota/kecil/'.$pijat_plus_plus);
            }

            for($i = 1; $i <= 3; $i++){ //START PERULANGAN FOTO PROFIL

                if($i == 1){ // BESAR
                    $path = './assets/files/anggota/besar/';
					$config['upload_path'] = $path;//Aturan-aturan untuk upload file
					$config['allowed_types'] = 'gif|jpg|png|jpeg|JPG|JPEG|PNG'; 
					$config['max_size'] = 60000;
					$config['max_width'] = 4000;
					$config['max_height'] = 3000;
					$config['file_name'] = $namafotoprofil;
					$this->load->library('upload');//Memanggil library untuk fungsi upload
					$this->upload->initialize($config);//Menetapkan aturan upload

					if(! $this->upload->do_upload('fotoprofil')){//Jika upload gagal
						$data = array('error' => $this->upload->display_errors());
						print_r($data);
					}
					else{
						//Jika upload berhasil

						$upload_data = $this->upload->data();
						$config['image_library'] = 'gd2';
						$config['source_image'] = $upload_data['full_path']; //get original image
						$config['maintain_ratio'] = false;
						$config['quality'] = '100%';
						$config['width']    = $widthex[0];
						$config['height']   = $widthex[0];
						$config['x_axis']   = $x_axis[0];
						$config['y_axis']   = $y_axis[0];
						$config['overwrite'] = TRUE;
						$this->load->library('image_lib', $config);
						$this->image_lib->initialize($config);
						// Crop
						if($this->image_lib->crop()){
							$resize['width'] = 180;
							$resize['height'] = 180;
							$this->image_lib->initialize($resize);
							$this->image_lib->resize();
							$data['fotoprofil'] = $upload_data['file_name'];
						}
					}
                } // EOF BESAR

                elseif($i == 2){ // SEDANG
                    $path = './assets/files/anggota/sedang/';
					$config['upload_path'] = $path;//Aturan-aturan untuk upload file
					$config['allowed_types'] = 'gif|jpg|png|jpeg|JPG|JPEG|PNG'; 
					$config['max_size'] = 60000;
					$config['max_width'] = 4000;
					$config['max_height'] = 3000;
					$config['file_name'] = $namafotoprofil;
					$this->load->library('upload');//Memanggil library untuk fungsi upload
					$this->upload->initialize($config);//Menetapkan aturan upload

					if(! $this->upload->do_upload('fotoprofil')){//Jika upload gagal
						$data = array('error' => $this->upload->display_errors());
						print_r($data);
					}
					else{
						//Jika upload berhasil

						$upload_data = $this->upload->data();
						$config['image_library'] = 'gd2';
						$config['source_image'] = $upload_data['full_path']; //get original image
						$config['maintain_ratio'] = false;
						$config['quality'] = '100%';
						$config['width']    = $widthex[0];
						$config['height']   = $widthex[0];
						$config['x_axis']   = $x_axis[0];
						$config['y_axis']   = $y_axis[0];
						$config['overwrite'] = TRUE;
						$this->load->library('image_lib', $config);
						$this->image_lib->initialize($config);
						// Crop
						if($this->image_lib->crop()){
							$resize['width'] = 118;
							$resize['height'] = 118;
							$this->image_lib->initialize($resize);
							$this->image_lib->resize();
							$data['fotoprofil'] = $upload_data['file_name'];
						}
					}
                } // EOF SEDANG
                
                else{ // KECIL
                    $path = './assets/files/anggota/kecil/';
					$config['upload_path'] = $path;//Aturan-aturan untuk upload file
					$config['allowed_types'] = 'gif|jpg|png|jpeg|JPG|JPEG|PNG'; 
					$config['max_size'] = 60000;
					$config['max_width'] = 4000;
					$config['max_height'] = 3000;
					$config['file_name'] = $namafotoprofil;
					$this->load->library('upload');//Memanggil library untuk fungsi upload
					$this->upload->initialize($config);//Menetapkan aturan upload

					if(! $this->upload->do_upload('fotoprofil')){//Jika upload gagal
						$data = array('error' => $this->upload->display_errors());
						print_r($data);
					}
					else{
						//Jika upload berhasil

						$upload_data = $this->upload->data();
						$config['image_library'] = 'gd2';
						$config['source_image'] = $upload_data['full_path']; //get original image
						$config['maintain_ratio'] = false;
						$config['quality'] = '100%';
						$config['width']    = $widthex[0];
						$config['height']   = $widthex[0];
						$config['x_axis']   = $x_axis[0];
						$config['y_axis']   = $y_axis[0];
						$config['overwrite'] = TRUE;
						$this->load->library('image_lib', $config);
						$this->image_lib->initialize($config);
						// Crop
						if($this->image_lib->crop()){
							$resize['width'] = 100;
							$resize['height'] = 100;
							$this->image_lib->initialize($resize);
							$this->image_lib->resize();
							$data['fotoprofil'] = $upload_data['file_name'];
						}
					}
                } // EOF KECIL 

            } //EOF PERULANGAN FOTO PROFIL
            
        }


        if($_FILES['fotosampul']['name'] !== '') { // FOTO SAMPUL
            $pijat_plus_plus = get_by_field('idanggota', $this->session->userdata('idanggota'), 'manggota')->fotosampul;

            if(is_file(FCPATH.'assets/files/anggota/sampul/'.$pijat_plus_plus)){
                unlink(FCPATH.'assets/files/anggota/sampul/'.$pijat_plus_plus);
            }

            $path = './assets/files/anggota/sampul/';
            $config['upload_path'] = $path;//Aturan-aturan untuk upload file
            $config['allowed_types'] = 'gif|jpg|png|jpeg|JPG|JPEG|PNG'; 
            $config['max_size'] = 60000;
            // $config['max_width'] = 4000;
            // $config['max_height'] = 3000;
            // $config['file_name'] = $namafotoprofil;
            $config['encrypt_name'] = true;
            $this->load->library('upload');//Memanggil library untuk fungsi upload
            $this->upload->initialize($config);//Menetapkan aturan upload

            if(! $this->upload->do_upload('fotosampul')){//Jika upload gagal
                $data = array('error' => $this->upload->display_errors());
                print_r($data);
            }
            else{
                //Jika upload berhasil
                $widthex2=explode(".",$this->input->post('reswidth2'));
                $heightex2=explode(".",$this->input->post('resheight2'));
                $x_axis2=explode(".",$this->input->post('x_axis2'));
                $y_axis2=explode(".",$this->input->post('y_axis2'));

                $upload_data = $this->upload->data();
                $config['image_library'] = 'gd2';
                $config['source_image'] = $upload_data['full_path']; //get original image
                $config['maintain_ratio'] = false;
                $config['quality'] = '100%';
                $config['width']    = $widthex2[0];
                $config['height']   = $widthex2[0];
                $config['x_axis']   = $x_axis2[0];
                $config['y_axis']   = $y_axis2[0];
                $config['overwrite'] = TRUE;
                $this->load->library('image_lib', $config);
                $this->image_lib->initialize($config);
                // Crop                
                if($this->image_lib->crop()){
                    $resize['width'] = 265;
                    $resize['height'] = 156;
                    $this->image_lib->initialize($resize);
                    $this->image_lib->resize();
                    $data['fotosampul'] = $upload_data['file_name'];
                }
            }
        }
        
    	$data['namaanggota'] = $post['namaanggota'];
    	$data['tempatlahir'] = $post['tempatlahir'];
        $data['tanggallahir'] = $post['tanggallahir'];
        $data['noktp'] = $post['noktp'];
        $data['provinsi'] = $post['provinsi'];
    	$data['alamat'] = $post['alamat'];
    	$data['desa'] = $post['desa'];
        $data['kecamatan'] = $post['kecamatan'];
        $data['agama'] = $post['agama'];
    	$data['kota'] = $post['kota'];
    	$data['kodepos'] = $post['kodepos'];
        $data['telepon'] = $post['telepon'];
        $data['stradiusorder'] = '0';
        $data['maksradiusorder'] = '0';
    	$data['hp'] = $post['hp'];
        $data['hpwa'] = $post['hpwa'];
        $data['facebook'] = $post['facebook'];
        $data['twitter'] = $post['twitter'];
        $data['deskripsipartner'] = $post['deskripsipartner'];
        $data['lat'] = $post['lat'];
        $data['lon'] = $post['lng'];
        $data['ahliwaris'] = $post['ahliwaris'];
    	$data['hubunganwaris'] = $post['hubunganwaris'];
        $data['jeniskelamin'] = $post['jeniskelamin'];
        $data['tipepartner'] = $post['tipepartner'];
        $data['statuspernikahan'] = $post['statuspernikahan'];
        
    	$this->db->where('idanggota', $post['idanggota']);
    	return $this->db->update('manggota', $data);
    	// return ($save) ? true : false;
    }

    public function saveRekening() {
    	$post = $this->input->post();
    	$this->db->set('norekening', $post['norekening']);
    	$this->db->set('namabank', $post['namabank']);
    	$this->db->set('cabangbank', $post['cabangbank']);
    	$this->db->where('idanggota', $post['idanggota']);
    	$save = $this->db->update($this->table);
    	return ($save) ? true : false;
    }

    public function getBayarDaftarPartner($id) {
        $this->db->where('idanggotapartner', $id);
        $this->db->order_by('tanggalbayar', 'desc');
        return $this->db->get('tbayarregistrasipartner', 1)->row_array();
    }

    public function saveVerifikasiBayarPendaftaran($id,$periode) {
        $periodeawal    = $this->input->post('periodeawal');
        $periodeakhir   = $this->input->post('periodeakhir');

        $this->db->set('jenisanggota', 3);
        $this->db->set('staktif', 1);
        $this->db->where('idanggota', $id);
        $updateTable = $this->db->update('manggota');
        if($updateTable) {
            $this->db->where('idanggota', $id);
            $updateStMasaAktif = $this->db->get('manggotamasaaktif');
            $anggota = $updateStMasaAktif->row();
            if($updateStMasaAktif->num_rows() > 0) {
                $this->db->set('staktif', 0);
                $this->db->where('idanggota', $anggota->idanggota);
                $this->db->update('manggotamasaaktif');
            }

            $maxNoUrut      = $this->db->order_by('nourut','desc')->get('manggotamasaaktif',1);
            if($maxNoUrut->num_rows() > 0) {
                $nourut = $maxNoUrut->row()->nourut;
            } else {
                $nourut = 0;
            }

            for($i=0;$i<=$periode;$i++){
                $nomor = $i;
                $this->db->set('idanggota', $id);
                $nourut = $nourut;
                $this->db->set('nourut', ($nomor+1)+$nourut );
                $periodepartner = date( 'Ym', strtotime( $periodeawal . ' + ' . $nomor . 'month' ) );
                $this->db->set('periodepartner', $periodepartner);
                $this->db->set('jenisanggota', 3);
                $this->db->set('staktif', 1);
                $this->db->insert('manggotamasaaktif');
            }
            return true;
        } else {
            return false;
        }
    }

    public function blokirPartner($id) {
    	$this->db->set('staktif', 0);
    	$this->db->where('idanggota', $id);
    	$save = $this->db->update($this->table);
    	return ($save) ? true : false;
    }

    public function aktifkanPartner($id) {
    	$this->db->set('staktif', 1);
    	$this->db->where('idanggota', $id);
    	$save = $this->db->update($this->table);
    	return ($save) ? true : false;
    }

    public function getDetailPartner($idAnggota) {
        $this->db->where('tipeanggota', 2);
        $this->db->where('idanggota', $idAnggota);
        $query = $this->db->get('manggota');
        return ($query) ? $query->row_array() : false;
    }

    public function getProdukPartner($idAnggota) {
        $this->db->where('idanggota', $idAnggota);
        $this->db->where('sttampil', 1);
        $query = $this->db->get('manggotaproduk');
        return ($query) ? $query->result() : false;
    }

    public function getProdukPartnerDetail($idAnggota, $idProduk) {
        $this->db->where('idanggota', $idAnggota);
        $this->db->where('idproduk', $idProduk);
        $query = $this->db->get('manggotaproduk');
        return ($query) ? $query->row_array() : false;
    }

    public function saveProdukPartner() {
        $post = $this->input->post();
        if($this->input->post('sthotdeals') == 1) {
            if($_FILES['foto']['name'] !== '') {
                $dir = './assets/files/produk/'.$post['idanggota'];
                if(!is_dir($dir)) {
                    mkdir($dir);
                }
                $config['upload_path']      = $dir;
                $config['allowed_types']    = 'jpg|jpeg|bmp|tiff|png';
                $config['overwrite']        = TRUE;
                $config['encrypt_name']     = TRUE;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if ($this->upload->do_upload('foto')) {
                    $this->db->select('foto');
                    $this->db->where('idanggota', $post['idanggota']);
                    $this->db->where('idproduk', $post['idproduk']);
                    $fotoLalu = $this->db->get('manggotaproduk', 1)->row()->foto;
                    $fileFoto = FCPATH.'assets/files/produk/'.$post['idanggota'].'/'.$fotoLalu;
                    if(is_file($fileFoto)) {
                        if(file_exists($fileFoto)) {
                            unlink($fileFoto);
                        }
                    }

                    $image_upload = $this->upload->data();
                    $this->db->set('foto', $image_upload['file_name']);
                }
            }

            $this->db->set('namaproduk', $post['namaproduk']);
            $this->db->set('namaproduk', $post['namaproduk']);
            $this->db->set('tipeproduk', $post['tipeproduk']);
            $this->db->set('deskripsiproduk', $post['deskripsiproduk']);
            $this->db->set('beratpackproduk', $post['beratpackproduk']);
            $this->db->set('hargahotdeals', $post['hargaproduk']);
            $this->db->set('hargajualhotdeals', $post['hargajual']);
            $this->db->set('keuntunganhotdeals', $post['keuntungan']);
            $this->db->set('bsponsorhotdeals', $post['bsponsor']);
            $this->db->set('bpartner1hotdeals', $post['bpartner1']);
            $this->db->set('bpartner2hotdeals', $post['bpartner2']);
            $this->db->set('bloyalitihotdeals', $post['bloyaliti']);
            $this->db->set('pendapatanhotdeals', $post['pendapatan']);
            $this->db->set('tanggalawalhotdeals', $post['tanggalawalhotdeals']);
            $this->db->set('tanggalakhirhotdeals', $post['tanggalakhirhotdeals']);
            $this->db->set('sttampilhotdeals', 1);
            $this->db->set('sthotdeals', 1);
        } else {
            if($_FILES['foto']['name'] !== '') {
                $config['upload_path']      = './assets/files/produk/'.$post['idanggota'];
                $config['allowed_types']    = 'jpg|jpeg|bmp|tiff|png';
                $config['overwrite']        = TRUE;
                $config['encrypt_name']     = TRUE;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if ($this->upload->do_upload('foto')) {
                    $this->db->select('foto');
                    $this->db->where('idanggota', $post['idanggota']);
                    $this->db->where('idproduk', $post['idproduk']);
                    $fotoLalu = $this->db->get('manggotaproduk', 1)->row()->foto;
                    $fileFoto = FCPATH.'assets/files/produk/'.$post['idanggota'].'/'.$fotoLalu;
                    if(is_file($fileFoto)) {
                        if(file_exists($fileFoto)) {
                            unlink($fileFoto);
                        }
                    }
                    $image_upload = $this->upload->data();
                    $this->db->set('foto', $image_upload['file_name']);
                }
            }
            $this->db->set('namaproduk', $post['namaproduk']);
            $this->db->set('namaproduk', $post['namaproduk']);
            $this->db->set('tipeproduk', $post['tipeproduk']);
            $this->db->set('deskripsiproduk', $post['deskripsiproduk']);
            $this->db->set('beratpackproduk', $post['beratpackproduk']);
            $this->db->set('hargaproduk', $post['hargaproduk']);
            $this->db->set('hargajual', $post['hargajual']);
            $this->db->set('keuntungan', $post['keuntungan']);
            $this->db->set('bsponsor', $post['bsponsor']);
            $this->db->set('bpartner1', $post['bpartner1']);
            $this->db->set('bpartner2', $post['bpartner2']);
            $this->db->set('bloyaliti', $post['bloyaliti']);
            $this->db->set('pendapatan', $post['pendapatan']);
            $this->db->set('sttampil', 1);
            $this->db->set('sthotdeals', 0);
        }

        $this->db->where('idanggota', $post['idanggota']);
        $this->db->where('idproduk', $post['idproduk']);
        return $this->db->update('manggotaproduk');
    }

}

/* End of file Mpartner_model.php */
/* Location: ./application/models/Mpartner_model.php */
