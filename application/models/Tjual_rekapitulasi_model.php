<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tjual_rekapitulasi_model extends CI_Model {

	function getAll($id){
		if($id == '0'){
			$this->db->select('tjual.*');
			$this->db->order_by('statusproses', 'asc');
			$this->db->where('idanggotapartner', $this->session->userdata('idanggota'));
			return $this->db->get('tjual')->result();
		}else{
			$this->db->select('tjual.*');
			$this->db->order_by('statusproses', 'asc');
			$this->db->where('idanggotapartner', $this->session->userdata('idanggota'));
			$this->db->where('statusproses',$id);
			return $this->db->get('tjual')->result();
		}
	}

	function getDetailProdukTrans($id){
		$query = "SELECT
					manggotaproduk.foto AS fotoproduk,
					manggotaproduk.namaproduk AS namaproduk,
					mkategoriproduk.namakategori AS kategori,
					tjualdetail.hargajual AS harga,
					tjualdetail.jumlahjual AS jumlah,
					tjualdetail.totaljual AS total,
					tjualdetail.catatanupenjual AS catatan 
				FROM
					tjual
					INNER JOIN tjualdetail ON ( tjualdetail.idtransaksi = tjual.idtransaksi )
					INNER JOIN manggotaproduk ON ( manggotaproduk.idproduk = tjualdetail.idproduk )
					INNER JOIN mkategoriproduk ON ( mkategoriproduk.idkategoriproduk = manggotaproduk.kategoriproduk ) 
				WHERE
					tjual.idtransaksi = '".$id."' 
					AND manggotaproduk.idanggota = '".$this->session->userdata('idanggota')."'";
		return $this->db->query($query)->result();
	}

	function totalBayar($id){
		$this->db->select('totalbayar As total');
		$this->db->where('idtransaksi',$id);
		return $this->db->get('tjual')->result();
	}

	function buktiTransfer($id){
		$this->db->where('idtransaksi',$id);
		return $this->db->get('tjualbayar')->result();
	}

}

/* End of file Tjual_rekapitulasi_model.php */
/* Location: ./application/models/Tjual_rekapitulasi_model.php */