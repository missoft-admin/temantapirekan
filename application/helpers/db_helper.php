<?php

	function get_by_field($fieldkey, $fieldval, $tbl){
		$CI =& get_instance();
		$CI->db->where($fieldkey,$fieldval);
		$query = $CI->db->get($tbl);
        return $query->row();
	}
	
	function get_by_fields($where, $tbl){
		$CI =& get_instance();
		$CI->db->where($where);
		$query = $CI->db->get($tbl);
        return $query->row();
    }

    function get_all($tbl, $where = array(), $order_by = null) {
    	$CI =& get_instance();
		$CI->db->where($where);
		if($order_by!==null) {
			$CI->db->order_by($order_by, 'ASC');
		}
		$query = $CI->db->get($tbl);		
		return $query->result();	
	}

	function get_all_where($tbl, $where = array(), $order_by = null) {
    	$CI =& get_instance();
		$CI->db->where($where);
		if($order_by!==null) {
			$CI->db->order_by($order_by, 'DESC');
		}
		$query = $CI->db->get($tbl);		
		return $query->row();	
	}

	function get_row($tbl, $where = array(), $order_by = null) {
    	$CI =& get_instance();
		$CI->db->where($where);
		if($order_by!==null) {
			$CI->db->order_by($order_by, 'DESC');
		}
		$query = $CI->db->get($tbl);		
		return $query->num_rows();	
	}

	function get_query($query){
		$CI =& get_instance();
		$result = $CI->db->query($query)->result();
		return $result;
	}

	function get_querys($query){
		$CI =& get_instance();
		$result = $CI->db->query($query);
		return $result;
	}
