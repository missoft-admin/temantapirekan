<?php

function path_variable()
{
    $CI =& get_instance();
    $data = array(
        'site_url'             => site_url(),
        'base_url'             => base_url(),

        'assets_path'          => base_url().'assets/',
        'css_path'             => base_url().'assets/css/',
        'cropperCss'           => base_url().'assets/imgCroper/css/',
        'cropperJs'            => base_url().'assets/imgCroper/js/',
        'img_path'             => base_url().'assets/images/',
        'libs_path'            => base_url().'assets/libs/',
        'scripts_path'         => base_url().'assets/scripts/',
        'files_path'           => base_url().'assets/files/',
        'plugins_path'         => base_url().'assets/scripts/plugins/',
        'gambarProduk'         => base_url().'assets/gambarProduk/',

        'idanggota'            => $CI->session->userdata('idanggota'),
        'namaanggota'          => $CI->session->userdata('namaanggota'),
        'username'             => $CI->session->userdata('username'),
        'passkey'              => $CI->session->userdata('passkey'),
        'logged_in'            => HumanDate($CI->session->userdata('logged_in')),
        'date_time'            => date('d-M-Y H:m:s'),
        // 'date'                 => date('d-M-Y'),
    );
    return $data;
}
