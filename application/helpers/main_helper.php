<?php

function BackendBreadcrum($data)
{
    $result = array();
    foreach ($data as $row) {
        if ($row[1] == '#') {
            $url = '<li><a class="text-muted" href="#">'.$row[0].'</a></li>';
        } elseif ($row[1] == '') {
            $url = '<li><a class="link-effect" href="#">'.$row[0].'</a></li>';
        } else {
            $url = '<li><a class="link-effect" href="'.site_url($row[1]).'">'.$row[0].'</a></li>';
        }
        array_push($result, $url);
    }

    return implode('', $result);
}

function HumanDate($date)
{
    $date = date('j F Y', strtotime($date));

    return $date;
}

function HumanDateMy($date)
{
    $date = date('F Y', strtotime($date));

    return $date;
}

function HumanDateShort($date)
{
    $date = date('d-m-Y', strtotime($date));

    return $date;
}

function HumanDateTime($date)
{
    $date = date('j F Y h:i:s', strtotime($date));

    return $date;
}

function LastLoginDate($date)
{
    if ($date != '' && $date != '0000-00-00 00:00:00') {
        $date = date('j F Y h:i:s', strtotime($date));
    } else {
        $date = 'First Login';
    }

    return $date;
}

function PermissionUserLoggedIn($session)
{
    if (!$session->userdata('username')) {
        $session->set_flashdata('error', true);
        $session->set_flashdata('message_flash', 'Access Denied');
        redirect('Auth');
    }
}

function PermissionUserLogin($session)
{
    if ($session->userdata('username')) {
        $session->set_flashdata('error', true);
        $session->set_flashdata('message_flash', 'Access Denied');
        redirect('dashboard');
    }
}

function ErrorSuccess($session)
{
    if ($session->flashdata('error')) {
        return ErrorMessage($session->flashdata('message_flash'));
    } elseif ($session->flashdata('confirm')) {
        return SuccesMessage($session->flashdata('message_flash'));
    } else {
        return '';
    }
}

function ErrorMessage($message)
{
    return '<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="warning" aria-hidden="true">&times;</button>
        <h3 class="font-w300 push-15">Perhatian!</h3>
        <p>'.$message.'</p>
      </div>';
}

function SuccesMessage($message)
{
    return '<div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h3 class="font-w300 push-15">Berhasil!</h3>
      <p>'.$message.'</p>
    </div>';
}


function GetAvatarName($str)
{
    $ret = '';
    foreach (explode(' ', $str) as $word)
        $ret .= strtoupper($word[0]);
    return substr($ret, 0, 2);
}

function GetColorRandom()
{
    $number = rand(0, 500);
    if($number > 0 && $number < 50){
      return 'primary';
    }else if($number > 51 && $number < 100){
      return 'info';
    }else if($number > 100 && $number < 150){
      return 'success';
    }else if($number > 151 && $number < 200){
      return 'warning';
    }else if($number > 201 && $number < 250){
      return 'primary';
    }else if($number > 251 && $number < 300){
      return 'warn';
    }else if($number > 301 && $number < 350){
      return 'dark';
    }else if($number > 351 && $number < 400){
      return 'accent';
    }else{
      return 'danger';
    }
}

function RemoveComma($number, $delimiter = ',')
{
    return str_replace($delimiter, '', $number);
}

function menuIsActive($menu)
{
    $CI =& get_instance();
    $exMenu = explode('/', $menu);
    $newMenu = (count($exMenu) == 2 ? $exMenu[1] : $menu);
    $newUriSegment = (count($exMenu) == 2 ? $CI->uri->segment(2) : $CI->uri->segment(1));

    return ($newUriSegment == $newMenu) ? "class=\"active\"" : "";
}



function getListFields($table) {
    $ci =& get_instance();
    $fields = $ci->db->list_fields($table);
    foreach($fields as $r) {
        $data[$r] = '';
    }
    return $data;
}

function dropdownAgama($defaultValue='Islam', $attr='class="form-control"') {
    $options = array(
            'Islam'        => 'Islam',
            'Kristen'      => 'Kristen',
            'Katolik'      => 'Katolik',
            'Budha'        => 'Budha',
            'Hindu'        => 'Hindu',
    );

    return form_dropdown('agama', $options, $defaultValue, $attr);
}

function dropdownJenkel($defaultValue='1', $attr='class="form-control"') {
    $options = array(
            '1'     => 'Laki',
            '2'     => 'Perempuan',
    );

    return form_dropdown('jeniskelamin', $options, $defaultValue, $attr);
}

function dropdownTipePartner($defaultValue='1', $attr='class="form-control"') {
    $options = array(
            '1'     => 'KULCIM ANTAR',
            '2'     => 'KULCIM AMBIL',
            '3'     => 'KULCIM ANTAM (Antar dan Ambil)',
    );

    return form_dropdown('tipepartner', $options, $defaultValue, $attr);
}

function dropdownStatusPernikahan($defaultValue='1', $attr='class="form-control"') {
    $options = array(
            '0'     => 'Belum Menikah',
            '1'     => 'Menikah',
    );

    return form_dropdown('statuspernikahan', $options, $defaultValue, $attr);
}

function dropdownBank($defaultValue='', $attr='class="form-control"') {
    $options = array(
            ''              => 'Pilih Opsi',
            'BCA'           => 'BCA',
            'Mandiri'       => 'Mandiri',
            'BNI'           => 'BNI',
            'BRI'           => 'BRI',
    );

    return form_dropdown('namabank', $options, $defaultValue, $attr);
}

function dropdownTipeProduk($defaultValue='', $attr='class="form-control"') {
    $options = array(
            ''              => 'Pilih Opsi',
            'Kering'        => 'Kering',
            'Basah'         => 'Basah',
            'Setengah Jadi' => 'Setengah Jadi',
            'Mentah'        => 'Mentah',
    );

    return form_dropdown('tipeproduk', $options, $defaultValue, $attr);
}

function statusDataProduk($id) {
    if ($id != '') {
        $data = array(
            '1' => '<span class="badge success text-u-c">Active</span>', 
            '0' => '<span class="badge danger text-u-c">NonActive</span>', 
        );
        return $data[$id];
    } else {
        return '';
    }    
}

function tipeBonus($id) {
    if ($id != '') {
        $data = array(
            '1' => '<span class="badge success text-u-c">Cashback Sponsor</span>', 
            '2' => '<span class="badge danger text-u-c">Cashback Royalti 1</span>', 
            '3' => '<span class="badge primary text-u-c">Cashback Royalti 2</span>'
        );
        return $data[$id];
    } else {
        return '';
    }    
}

function statusPenjualan($id) {
    if ($id != '') {
        $data = array(
            '0' => '', 
            '1' => '<span style="font-size:15px;font-weight:700;color:#fdab29;">MENUNGGU TRANSFER</span>', 
            '2' => '<a href="{site_url}tjual_prosespacking"><span style="font-weight:700;font-size:15px;width:100px;color:#ea2e49;">PROSES</span></a>', 
            '3' => '<a href="{site_url}tjual_packingpengiriman"><span style="font-weight:700;font-size:15px;width:100px;color:#14baa8;">PACKING</span></a>', 
            '4' => '<span style="font-size:15px;font-weight:700;color:#53a6fa;">PENGIRIMAN</span>',
            '5' => '<span style="font-size:15px;font-weight:700;color:#22b66e;">SELESAI</span>',
            '6' => '<span style="font-size:15px;font-weight:700;color:#22b66e;">SELESAI DIAMBIL</span>', 
        );
        return $data[$id];
    } else {
        return '';
    }    
}

function statusProses($id) {
    if ($id != '') {
        $data = array(
            '2' => '<a href="{site_url}tjual_prosespacking"><span class="badge danger text-u-c" style="font-size:15px;width:100px;">PROSES</span></a>', 
        );
        return $data[$id];
    } else {
        return '';
    }    
}

function hotDeals($sthotdeals,$sttampilhotdeals,$hargahotdeals,$harga){
    if($sthotdeals == '1' && $sttampilhotdeals == '0'){
        $tampil = '<span class="badge danger text-u-c">PENDING HOTDEALS</span>';
        return $tampil;
    }elseif($sthotdeals == '1' && $sttampilhotdeals == '1') {
        $tampil = '<span class="badge danger text-u-c">HOTDEALS</span>';
        return $tampil;
    }else{
        $tampil = '';
        return $tampil;
    }
}

function jenisAnggotaMember($id) {
    if ($id != '') {
        $data = array(
            '1' => '<span class="badge grey text-u-c">Guest Member</span>', 
            '2' => '<span class="badge warning text-u-c">Premium Member</span>', 
        );
        return $data[$id];
    } else {
        return '';
    }    
}

function jenisAnggotaPartner($id) {
    if ($id != '') {
        $data = array(
            '1' => '<span class="badge grey text-u-c">Guest Partner</span>', 
            '2' => '<span class="badge warning text-u-c">Gold Partner</span>', 
            '3' => '<span class="badge success text-u-c">Premium Partner</span>'
        );
        return $data[$id];
    } else {
        return '';
    }    
}

function jenisKelamin($id) {
    if ($id != '') {
        $data = array(
            '1' => 'Laki-laki', 
            '2' => 'Perempuan', 
        );
        return $data[$id];
    } else {
        return '';
    }    
}

function numberIndo($number) {
    if($number) {
        return number_format($number, 2, ',', '.');
    } else {
        return $number;
    }
}

function tipePenjuals($id){
    if($id == '1'){
        $hasil = '<img src="{site_url}assets/images/penjual-antar-icon.png" class="img-responsive" style="width: 20px;"><span style="">&nbsp;KULCIM ANTAR</span>';
        return $hasil;
    }
    elseif($id == '2'){
        $hasil = '<img src="{site_url}assets/images/penjual-ambil-icon.png" class="img-responsive" style="width: 20px;"><span>&nbsp;KULCIM AMBIL</span>';
        return $hasil;
    }else{
        $hasil = '<img src="{site_url}assets/images/penjual-antam-icon.png" class="img-responsive" style="width: 20px;"><span>&nbsp;KULCIM ANTAM (Antar dan Ambil)</span>';
        return $hasil;
    }
}

function jenisPenjuals($id){
    if($id == '2'){
        $hasil = 'Premium';
        return $hasil;
    }
    elseif($id == '3'){
        $hasil = 'Prioritas';
        return $hasil;
    }else{
        $hasil = 'Spesial';
        return $hasil;
    }
}

function setFlashMsg($type, $msg, $redir) {
    $ci =& get_instance();
    $ci->session->set_flashdata($type,true);
    $ci->session->set_flashdata('message_flash',$msg);
    redirect($redir,'location');
}

function statusDataPribadi($id) {
    if ($id != '') {
        $data = array(
            '1' => '<span class="badge success text-u-c">Active</span>', 
            '0' => '<span class="badge danger text-u-c">Blokir</span>', 
        );
        return $data[$id];
    } else {
        return '';
    }    
}

function dropdownHubunganWaris($defaultValue='0', $attr='class="form-control"') {
    $options = array(
            '0'      => 'Select an Option',
            '1'      => 'Ayah',
            '2'      => 'Ibu',
            '3'      => 'Suami',
            '4'      => 'Istri',
            '5'      => 'Anak',
    );

    return form_dropdown('hubunganwaris', $options, $defaultValue, $attr);
}

function getByFields($table, $where=array()) {
    $ci =& get_instance();
    $ci->db->where($where);
    return $ci->db->get($table)->row();
}

function createOptionsDropdown($table,$key,$val,$where = null) {
    $ci =& get_instance();
    $data = array();
    if($where !== null) {
        $query = $ci->db->get_where($table, $where)->result_array();
    } else {
        $query = $ci->db->get($table)->result_array();
    }
    foreach($query as $r) {
        $data[ $r[$key] ]   = $r[$val];
    }
    return $data;
}

function tipeProduk($id) {
    if ($id != '') {
        $data = get_by_field('idkategoriproduk',$id,'mkategoriproduk')->namakategori;
        return $data;
    } else {
        return '-';
    }    
}

function jenisPartner($id) {
    if ($id != '') {
        $data = array(
            '2' => '<span class="badge warning text-u-c" style="font-size:13px;width:90px;">GOLD</span>', 
            '1' => '<span class="badge success text-u-c" style="font-size:13px;width:90px;">PREMIUM</span>'
        );
        return $data[$id];
    } else {
        return '';
    }    
}

function bulan($bulan){
    $nama_bulan;
    switch ($bulan) {
        case '01':
            $nama_bulan = 'Januari';
            break;
        
        case '02':
            $nama_bulan = 'Februari';
            break;

        case '03':
            $nama_bulan = 'Maret';
            break;

        case '04':
            $nama_bulan = 'April';
            break;
        
        case '05':
            $nama_bulan = 'Mei';
            break;

        case '06':
            $nama_bulan = 'Juni';
            break;
        
        case '07':
            $nama_bulan = 'Juli';
            break;

        case '08':
            $nama_bulan = 'Agustus';
            break;

        case '09':
            $nama_bulan = 'September';
            break;

        case '10':
            $nama_bulan = 'Oktober';
            break;

        case '11':
            $nama_bulan = 'November';
            break;

        default:
            $nama_bulan = 'Desember';
            break;
    }

    return $nama_bulan;
}

function produkHotDeals($sthotdeals,$sttampilhotdeals,$hargahotdeals,$harga){
    if($sthotdeals == '1' && $sttampilhotdeals == '0'){
        $tampil = '<span style="color:red;">'.number_format($hargahotdeals).'</span>';
        return $tampil;
    }elseif($sthotdeals == '1' && $sttampilhotdeals == '1') {
        $tampil = '<span style="color:red;">'.number_format($hargahotdeals).'</span>';
        return $tampil;
    }else{
        $tampil = '<span>'.number_format($harga).'</span>';
        return $tampil;
    }
}

function validasiProduk($cekJumlah,$cekJenisAnggota){
    if($cekJenisAnggota == '1'){
        if($cekJumlah >= 5){
            return '0';
        }else{
            return '1';
        }
    }
    elseif($cekJenisAnggota == '2'){
        if($cekJumlah >= 25){
            return '0';
        }else{
            return '1';
        }
    }
    else{
        return '1';
    }
}

function penjualJenis($id){
    if($id == '1'){
        return 'Penjual Spesial';
    }
    elseif($id == '2'){
        return 'Penjual Premium';
    }
    else{
        return 'Penjual Prioritas';
    }
}

function validasiProdukIndex($jenisAnggota,$prodAktif,$idproduk){
    $a = '<a class="dropdown-item" href="{site_url}manggota_produk/aktifProduk/'.$idproduk.'">Aktifkan</a>';
    $b = '<a class="dropdown-item" id="gagalAktif" href="#">Aktifkan</a>';
    if($jenisAnggota == '1'){
        if($prodAktif >= 5){
            return $b;
        }else{
            return $a;
        }
    }
    elseif($jenisAnggota == '2'){
        if($prodAktif >= 25){
            return $b;
        }else{
            return $a;
        }
    }
    else{
        return $a;
    }
}