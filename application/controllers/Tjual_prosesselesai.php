<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tjual_prosesselesai extends CI_Controller {

	public function __construct(){
		parent::__construct();
		// PermissionUserLoggedIn($this->session);
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tjual_prosespacking_model');
	}

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['toptitle'] 		= 'PENJUALAN';
		$data['title'] 			= 'PENGAMBILAN OLEH PEMBELI';
		$data['content'] 		= 'Tjual_prosesselesai/index';

		$data['list_index'] = $this->Tjual_prosespacking_model->getAll();

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}

	function prosesSelesai($id){
		if($this->Tjual_prosespacking_model->prosesSelesai($id)){
			redirect('Tjual_prosespacking');
			return true;
		}else{
			print_r($this->Tjual_prosespacking_model->prosesSelesai($id));
		}
	}

}

/* End of file Tjual_prosesselesai.php */
/* Location: ./application/controllers/Tjual_prosesselesai.php */