<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    /**
     * Dashboard controller.
     * Developer @RendyIchtiar
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
		$this->load->model('Dashboard_model');
        $this->load->model('Pendapatan_model');
    }

    public function index()
    {
        $data = array();
        $data['toptitle']   = 'Dashboard';
        $data['title']      = 'Dashboard';
        $data['content']    = 'dashboard';
        $data['bulan'] = date('m');
        $data['tahun'] = date('Y');

    		$data['sumSponsor']           = $this->Pendapatan_model->sumSponsor()->nominalbonus;
            $data['sumPartnership1']    = $this->Pendapatan_model->sumPartnership1()->nominalbonus;
            $data['sumPartnership2']    = $this->Pendapatan_model->sumPartnership2()->nominalbonus;
            $data['sum']                = $this->Pendapatan_model->sumTotal()->nominalbonus;

            $data['listStatusMenunggu'] = $this->Dashboard_model->orderPenjualan()->result();
            $data['jumlahOrder'] = $this->Dashboard_model->orderPenjualan()->num_rows();
            $data['produkPenjualanTerbanyak'] = $this->Dashboard_model->produkPenjualanTerbanyak()->result();
            $data['jumlahProdukPenjualanTerbanyak'] = $this->Dashboard_model->produkPenjualanTerbanyak()->num_rows();
            $data['produkPenjualanSedikit'] = $this->Dashboard_model->produkPenjualanSedikit()->result();
            $data['jumlahProdukPenjualanSedikit'] = $this->Dashboard_model->produkPenjualanSedikit()->num_rows();
            $data['produkTerbanyakPendapatan'] = $this->Dashboard_model->produkTerbanyakPendapatan()->result();
            $data['jumlahProdukTerbanyakPendapatan'] = $this->Dashboard_model->produkTerbanyakPendapatan()->num_rows();
            $data['produkSedikitPendapatan'] = $this->Dashboard_model->produkSedikitPendapatan()->result();
            $data['jumlahProdukSedikitPendapatan'] = $this->Dashboard_model->produkSedikitPendapatan()->num_rows();
            $data['memberSponsorPembelianTerbanyak'] = $this->Dashboard_model->memberSponsorPembelianTerbanyak()->result();
            $data['jumlahMemberSponsorPembelianTerbanyak'] = $this->Dashboard_model->memberSponsorPembelianTerbanyak()->num_rows();
            $data['memberSponsorPembelianSedikt'] = $this->Dashboard_model->memberSponsorPembelianSedikt()->result();
            $data['jumlahMemberSponsorPembelianSedikt'] = $this->Dashboard_model->memberSponsorPembelianSedikt()->num_rows();
            $data['partnerSponsorPenjualanTerbanyak'] = $this->Dashboard_model->partnerSponsorPenjualanTerbanyak()->result();
            $data['partnerSponsorPenjualanSedikit'] = $this->Dashboard_model->partnerSponsorPenjualanSedikit()->result();
            $data['jumlahPartnerSponsorPenjualanTerbanyak'] = $this->Dashboard_model->partnerSponsorPenjualanTerbanyak()->num_rows();
            $data['jumlahPartnerSponsorPenjualanSedikit'] = $this->Dashboard_model->partnerSponsorPenjualanSedikit()->num_rows();

        $data = array_merge($data, path_variable());
        $this->parser->parse('page_template', $data);
    }

    function filter(){
        $date = $this->input->post('tahun').$this->input->post('bulan');
        $data = array();
        $data['toptitle']   = 'Dashboard';
        $data['title']      = 'Dashboard';
        $data['content']    = 'dashboard';
        $data['bulan'] = $this->input->post('bulan');
        $data['tahun'] = $this->input->post('tahun');

            $data['sumSponsor']           = $this->Pendapatan_model->sumSponsor()->nominalbonus;
            $data['sumPartnership1']    = $this->Pendapatan_model->sumPartnership1()->nominalbonus;
            $data['sumPartnership2']    = $this->Pendapatan_model->sumPartnership2()->nominalbonus;
            $data['sum']                = $this->Pendapatan_model->sumTotal()->nominalbonus;
            // List Data
            
            $a = "SELECT * FROM datapenjualan_detail WHERE periodetransaksi = '".$date."' AND idanggotapartner = '".$this->session->userdata('idanggota')."' AND (statusproses = '1' OR statusproses = '2') ORDER BY tanggaltransaksi, totalbayar DESC LIMIT 0,10";
            $data['listStatusMenunggu'] = get_querys($a)->result();
            $data['jumlahOrder'] = get_querys($a)->num_rows();

            $b = "SELECT tjual.*, SUM(jumlahjual) as total_jual, tjualdetail.idproduk, manggotaproduk.foto FROM `tjual` INNER JOIN tjualdetail ON tjualdetail.idtransaksi = tjual.idtransaksi INNER JOIN manggotaproduk ON manggotaproduk.idproduk = tjualdetail.idproduk WHERE tjual.periodetransaksi = '".$date."' AND idanggotapartner = '".$this->session->userdata('idanggota')."' GROUP BY tjualdetail.idproduk ORDER BY total_jual DESC LIMIT 10";
            $data['produkPenjualanTerbanyak'] = get_querys($b)->result();
            $data['jumlahProdukPenjualanTerbanyak'] = get_querys($b)->num_rows();

            $c = "SELECT tjual.*, SUM(jumlahjual) as total_jual, tjualdetail.idproduk, manggotaproduk.foto FROM `tjual` INNER JOIN tjualdetail ON tjualdetail.idtransaksi = tjual.idtransaksi INNER JOIN manggotaproduk ON manggotaproduk.idproduk = tjualdetail.idproduk WHERE idanggotapartner = '".$this->session->userdata('idanggota')."' AND tjual.periodetransaksi = '".$date."' GROUP BY tjualdetail.idproduk ORDER BY total_jual ASC LIMIT 10";
            $data['produkPenjualanSedikit'] = get_querys($c)->result();
            $data['jumlahProdukPenjualanSedikit'] = get_querys($c)->num_rows();

            $d = "SELECT tjual.*, tjualdetail.idproduk, tjualdetail.jumlahjual, tjualdetail.hargajual, SUM(tjualdetail.jumlahjual * tjualdetail.hargajual) As nominalpendapatan FROM tjual INNER JOIN tjualdetail ON(tjualdetail.idtransaksi=tjual.idtransaksi) WHERE idanggotapartner = '".$this->session->userdata('idanggota')."' AND tjual.periodetransaksi = '".$date."' GROUP BY tjualdetail.idproduk ORDER BY nominalpendapatan DESC LIMIT 10";
            $data['produkTerbanyakPendapatan'] = get_querys($d)->result();
            $data['jumlahProdukTerbanyakPendapatan'] = get_querys($d)->num_rows();

            $e = "SELECT tjual.*, tjualdetail.idproduk, tjualdetail.jumlahjual, tjualdetail.hargajual, SUM(tjualdetail.jumlahjual * tjualdetail.hargajual) As nominalpendapatan FROM tjual INNER JOIN tjualdetail ON(tjualdetail.idtransaksi=tjual.idtransaksi) WHERE idanggotapartner = '".$this->session->userdata('idanggota')."' AND tjual.periodetransaksi = '".$date."' GROUP BY tjualdetail.idproduk ORDER BY nominalpendapatan ASC LIMIT 10";
            $data['produkSedikitPendapatan'] = get_querys($e)->result();
            $data['jumlahProdukSedikitPendapatan'] = get_querys($e)->num_rows();

            $f = "SELECT tjual.*, manggota.idsponsor, SUM(tjualdetail.hargajual * tjualdetail.jumlahjual) As totaltransaksi FROM tjual INNER JOIN manggota ON(manggota.idanggota=tjual.idanggota) INNER JOIN tjualdetail ON(tjualdetail.idtransaksi = tjual.idtransaksi) WHERE manggota.idsponsor = '".$this->session->userdata('idanggota')."' AND tjual.periodetransaksi = '".$date."' GROUP BY tjual.idanggota ORDER BY totaltransaksi DESC LIMIT 10";
            $data['memberSponsorPembelianTerbanyak'] = get_querys($f)->result();
            $data['jumlahMemberSponsorPembelianTerbanyak'] = get_querys($f)->num_rows();

            $g = "SELECT tjual.*, manggota.idsponsor, SUM(tjualdetail.hargajual * tjualdetail.jumlahjual) As totaltransaksi FROM tjual INNER JOIN manggota ON(manggota.idanggota=tjual.idanggota) INNER JOIN tjualdetail ON(tjualdetail.idtransaksi = tjual.idtransaksi) WHERE manggota.idsponsor = '".$this->session->userdata('idanggota')."' AND tjual.periodetransaksi = '".$date."' GROUP BY tjual.idanggota ORDER BY totaltransaksi ASC LIMIT 10";
            $data['memberSponsorPembelianSedikt'] = get_querys($g)->result();
            $data['jumlahMemberSponsorPembelianSedikt'] = get_querys($g)->num_rows();

            $h = "SELECT tjual.*, manggota.idsponsor, SUM(tjualdetail.hargajual * tjualdetail.jumlahjual) As totaltransaksi FROM tjual INNER JOIN manggota ON(manggota.idanggota=tjual.idanggota) INNER JOIN tjualdetail ON(tjualdetail.idtransaksi = tjual.idtransaksi) WHERE manggota.idsponsor = '".$this->session->userdata('idanggota')."' AND tjual.periodetransaksi = '".$date."' GROUP BY tjual.idanggotapartner ORDER BY totaltransaksi DESC LIMIT 10";
            $data['partnerSponsorPenjualanTerbanyak'] = get_querys($h)->result();
            $data['jumlahPartnerSponsorPenjualanTerbanyak'] = get_querys($h)->num_rows();

            $i = "SELECT tjual.*, manggota.idsponsor, SUM(tjualdetail.hargajual * tjualdetail.jumlahjual) As totaltransaksi FROM tjual INNER JOIN manggota ON(manggota.idanggota=tjual.idanggota) INNER JOIN tjualdetail ON(tjualdetail.idtransaksi = tjual.idtransaksi) WHERE manggota.idsponsor = '".$this->session->userdata('idanggota')."' AND tjual.periodetransaksi = '".$date."' GROUP BY tjual.idanggotapartner ORDER BY totaltransaksi ASC LIMIT 10";
            $data['partnerSponsorPenjualanSedikit'] = get_querys($i)->result();
            $data['jumlahPartnerSponsorPenjualanSedikit'] = get_querys($i)->num_rows();

        $data = array_merge($data, path_variable());
        $this->parser->parse('page_template', $data);
    }
}
