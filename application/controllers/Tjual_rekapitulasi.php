<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tjual_rekapitulasi extends CI_Controller {

	public function __construct(){
		parent::__construct();
		// PermissionUserLoggedIn($this->session);
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tjual_rekapitulasi_model');
	}

	function index($id){
		$data = array();
		$data['error'] 			= '';
		$data['toptitle'] 		= 'PENJUALAN';
		$data['title'] 			= 'REKAP PROSES TRANSAKSI';
		$data['content'] 		= 'Tjual_rekapitulasi/index';
		$data['bulan']			= date('m');
		$data['tahun']			= date('Y');

		$data['list_index'] = $this->Tjual_rekapitulasi_model->getAll($id);

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}

	function getDetailProdukTrans($id){
		$data = $this->Tjual_rekapitulasi_model->getDetailProdukTrans($id);
		$this->output->set_output(json_encode($data));
	}

	function totalBayar($id){
		$data = $this->Tjual_rekapitulasi_model->totalBayar($id);
		$this->output->set_output(json_encode($data));
	}

	function buktiTransfer($id){
		$data = $this->Tjual_rekapitulasi_model->buktiTransfer($id);
		$this->output->set_output(json_encode($data));
	}

}

/* End of file Tjual_rekapitulasi.php */
/* Location: ./application/controllers/Tjual_rekapitulasi.php */