<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendapatan extends CI_Controller {

	function __construct(){
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->model('Pendapatan_model');
	}

	function sponsor(){
		$data = array();
		$data['error'] 			= '';
		$data['toptitle'] 		= 'Sponsor';
		$data['title'] 			= 'Cashback Sponsor';
		$data['content'] 		= 'Pendapatan/sponsor';

		$data['list_index'] = $this->Pendapatan_model->getSponsor();
		$data['sum']		= $this->Pendapatan_model->sumSponsor()->nominalbonus;
		$data['tahun']		= date('Y');
		$data['bulan']		= date('m');

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}

	function laporan($id){
		if($id != '0'){
			$data = array();
			$data['error'] 			= '';
			$data['toptitle'] 		= 'Laporan';
			$data['title'] 			= 'Laporan Hasil Penjualan';
			$data['content'] 		= 'Pendapatan/laporan';

			$data['list_index'] = $this->Pendapatan_model->getLaporan($id);
			$data['jumlahTransaksi'] = $this->Pendapatan_model->getJumlahTransaksi($id);
			$data['nominalTransaksi'] = $this->Pendapatan_model->getTotalNominalTransaksi($id);
			$data['tahun']		= date('Y');
			$data['bulan']		= date('m');

			$data = array_merge($data, path_variable());
			$this->parser->parse('page_template', $data);
		}else{
			$data = array();
			$data['error'] 			= '';
			$data['toptitle'] 		= 'Laporan';
			$data['title'] 			= 'Laporan Hasil Penjualan';
			$data['content'] 		= 'Pendapatan/laporan';

			$data['list_index'] = $this->Pendapatan_model->getLaporan($id);
			$data['jumlahTransaksi'] = $this->Pendapatan_model->getJumlahTransaksi($id);
			$data['nominalTransaksi'] = $this->Pendapatan_model->getTotalNominalTransaksi($id);
			$data['tahun']		= $this->input->post('tahun');
			$data['bulan']		= $this->input->post('bulan');

			$data = array_merge($data, path_variable());
			$this->parser->parse('page_template', $data);
		}
	}

	function filterSponsor(){

		$data = array();
		$data['tahun'] = $this->input->post('tahun');
		$data['bulan'] = $this->input->post('bulan');
		$data['error'] 			= '';
		$data['toptitle'] 		= 'Sponsor';
		$data['title'] 			= 'Cashback Sponsor';
		$data['content'] 		= 'Pendapatan/sponsor';

		$periode = $data['tahun'].$data['bulan'];

		$data['list_index'] = get_all('manggotabonus',array('idanggota'=>$this->session->userdata('idanggota'),'tipebonus'=>'1','periodetransaksi'=>$periode));

		$query = "SELECT SUM(nominalbonus) As nominalbonus FROM manggotabonus WHERE idanggota='".$this->session->userdata('idanggota')."' AND tipebonus = '1' AND periodetransaksi='".$periode."'";
		$a = $this->db->query($query)->row();

		$data['sum']		= $a->nominalbonus;

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}

	function partnership1(){
		$data = array();
		$data['error'] 			= '';
		$data['toptitle'] 		= 'Royalti 1';
		$data['title'] 			= 'Cashback Royalti 1';
		$data['content'] 		= 'Pendapatan/partnership1';

		$data['list_index'] = $this->Pendapatan_model->getPartnership1();
		$data['sum']		= $this->Pendapatan_model->sumPartnership1()->nominalbonus;
		$data['tahun']		= date('Y');
		$data['bulan']		= date('m');

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}

	function filterPartnership1(){
		$data = array();
		$data['tahun'] = $this->input->post('tahun');
		$data['bulan'] = $this->input->post('bulan');
		$data['error'] 			= '';
		$data['toptitle'] 		= 'Royalti 1';
		$data['title'] 			= 'Cashback Royalti 1';
		$data['content'] 		= 'Pendapatan/sponsor';

		$periode = $data['tahun'].$data['bulan'];

		$data['list_index'] = get_all('manggotabonus',array('idanggota'=>$this->session->userdata('idanggota'),'tipebonus'=>'2','periodetransaksi'=>$periode));

		$query = "SELECT SUM(nominalbonus) As nominalbonus FROM manggotabonus WHERE idanggota='".$this->session->userdata('idanggota')."' AND tipebonus = '2' AND periodetransaksi='".$periode."'";
		$a = $this->db->query($query)->row();

		$data['sum']		= $a->nominalbonus;

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}

	function partnership2(){
		$data = array();
		$data['error'] 			= '';
		$data['toptitle'] 		= 'Royalti 2';
		$data['title'] 			= 'Cashback Royalti 2';
		$data['content'] 		= 'Pendapatan/partnership2';

		$data['list_index'] = $this->Pendapatan_model->getPartnership2();
		$data['sum']		= $this->Pendapatan_model->sumPartnership2()->nominalbonus;
		$data['tahun']		= date('Y');
		$data['bulan']		= date('m');

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}

	function filterPartnership2(){
		$data = array();
		$data['tahun'] = $this->input->post('tahun');
		$data['bulan'] = $this->input->post('bulan');
		$data['error'] 			= '';
		$data['toptitle'] 		= 'Royalti 2';
		$data['title'] 			= 'Cashback Royalti 2';
		$data['content'] 		= 'Pendapatan/sponsor';

		$periode = $data['tahun'].$data['bulan'];

		$data['list_index'] = get_all('manggotabonus',array('idanggota'=>$this->session->userdata('idanggota'),'tipebonus'=>'3','periodetransaksi'=>$periode));

		$query = "SELECT SUM(nominalbonus) As nominalbonus FROM manggotabonus WHERE idanggota='".$this->session->userdata('idanggota')."' AND tipebonus = '3' AND periodetransaksi='".$periode."'";
		$a = $this->db->query($query)->row();

		$data['sum']		= $a->nominalbonus;

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}

	function total(){
		$data = array();
		$data['error'] 			= '';
		$data['toptitle'] 		= 'Pendapatan';
		$data['title'] 			= 'Total Pendapatan';
		$data['content'] 		= 'Pendapatan/total';

		$data['list_index'] 		= $this->Pendapatan_model->getTotal();
		$data['sumSponsor']			= $this->Pendapatan_model->sumSponsor()->nominalbonus;
		$data['sumPartnership1']	= $this->Pendapatan_model->sumPartnership1()->nominalbonus;
		$data['sumPartnership2']	= $this->Pendapatan_model->sumPartnership2()->nominalbonus;
		$data['sum']				= $this->Pendapatan_model->sumTotal()->nominalbonus;
		$data['tahun']		= date('Y');
		$data['bulan']		= date('m');

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}

}

/* End of file Pendapatan.php */
/* Location: ./application/controllers/Pendapatan.php */