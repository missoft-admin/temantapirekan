<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	// Auth controllers
	// Developer : @RendyIchtiarSaputra

	function __construct(){
		parent::__construct();
		$this->load->model('Auth_model');
	}

	public function index(){
		PermissionUserLogin($this->session);
		$data['title'] = 'SignIn';
		$data['error'] = '';
		$this->load->view('Auth/index',$data);
	}

	function dosign_in(){

		$username	= $this->input->post('username');
	 	$password = $this->input->post('password');

		if($this->Auth_model->signIn($username, $password)){
			redirect('dashboard','location');
		}else{
			$data['title'] = 'SignIn';
			$data['error'] = 'Username Atau Password Salah! Silahkan Coba Lagi.';
			$this->load->view('Auth/index',$data);
		}
	}

	function sign_out(){

		$this->Auth_model->signOut();
		redirect('auth','location');
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */