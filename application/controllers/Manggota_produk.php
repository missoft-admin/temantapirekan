<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manggota_produk extends CI_Controller {

	/**
	 * Master Anggota Produk controller.
	 * Developer @RendyIchtiarSaputra
	 */

	public function __construct()
  {
		parent::__construct();
		// PermissionUserLoggedIn($this->session);
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Manggota_produk_model');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['toptitle'] 		= 'Data Produk';
		$data['title'] 			= 'Data Produk';
		$data['content'] 		= 'Manggota_produk/index';

		$data['list_index'] = $this->Manggota_produk_model->getAll();

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}

	function tambah(){
		$codex = $this->Manggota_produk_model->getKode();
		$nomor1 = $codex + 1;
		$kode = 'PRD'.date('y').date('m').date('d').str_pad($nomor1, '6',"0",STR_PAD_LEFT);
		$data = array();
		$data['idproduk'] = $kode;
		$data['namaProduk'] = '';
		$data['kategoriProduk'] = '';
		$data['deskripsiProduk'] = '';
		$data['beratProduk'] = '';
		$data['hargaProduk'] = 0;
		$data['hargaJual'] = 0;
		$data['keuntungan'] = 0;
		$data['sttampil'] = '';
		$data['foto'] = '';
		$data['hargahotdeals'] = 0;
		$data['hargajualhotdeals'] = 0;
		$data['error'] 			= '';
		$data['toptitle'] 		= 'Tambah Data Produk';
		$data['title'] 			= 'Tambah Data Produk';
		$data['content'] 		= 'Manggota_produk/manage';

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}

	private function uploadData($path,$persenHarga,$widthex,$heightex,$x_axis,$y_axis,$namaprod){
		
	}

	function save(){
		$data = array();
		$persenHarga = floatval($this->Manggota_produk_model->getSettingHarga()->persenharga);
		$widthex=explode(".",$this->input->post('reswidth'));
		$heightex=explode(".",$this->input->post('resheight'));
		$x_axis=explode(".",$this->input->post('x_axis'));
		$y_axis=explode(".",$this->input->post('y_axis'));
		$namaprod = $this->session->userdata('idanggota').'-'.$this->input->post('idproduk');

		if($this->input->post('sttampil') == ''){
			for($i = 1;$i <= 3; $i++){ //Perulangan Gambar
				if($i == 1){ //BESAR
					$path = './assets/gambarProduk/besar/';
					$config['upload_path'] = $path;//Aturan-aturan untuk upload file
					$config['allowed_types'] = 'gif|jpg|png|jpeg|JPG|JPEG|PNG'; 
					$config['max_size'] = 60000;
					$config['max_width'] = 4000;
					$config['max_height'] = 3000;
					$config['file_name'] = $namaprod;
					$this->load->library('upload');//Memanggil library untuk fungsi upload
					$this->upload->initialize($config);//Menetapkan aturan upload

					if(! $this->upload->do_upload('produk_file')){//Jika upload gagal
						$data = array('error' => $this->upload->display_errors());
						print_r($data);
					}
					else{
						//Jika upload berhasil

						$upload_data = $this->upload->data();
						$config['image_library'] = 'gd2';
						$config['source_image'] = $upload_data['full_path']; //get original image
						$config['maintain_ratio'] = false;
						$config['quality'] = '100%';
						$config['width']    = $widthex[0];
						$config['height']   = $widthex[0];
						$config['x_axis']   = $x_axis[0];
						$config['y_axis']   = $y_axis[0];
						$config['overwrite'] = TRUE;
						$this->load->library('image_lib', $config);
						$this->image_lib->initialize($config);
						// Crop
						if($this->image_lib->crop()){
							$resize['width'] = 413;
							$resize['height'] = 413;
							$this->image_lib->initialize($resize);
							$this->image_lib->resize();
							$data['foto'] = $upload_data['file_name'];
						}
					}
				}
				elseif($i == 2){ //SEDANG
					$path = './assets/gambarProduk/sedang/';
					$config['upload_path'] = $path;//Aturan-aturan untuk upload file
					$config['allowed_types'] = 'gif|jpg|png|jpeg|JPG|JPEG|PNG'; 
					$config['max_size'] = 60000;
					$config['max_width'] = 4000;
					$config['max_height'] = 3000;
					$config['file_name'] = $namaprod;
					$this->load->library('upload');//Memanggil library untuk fungsi upload
					$this->upload->initialize($config);//Menetapkan aturan upload

					if(! $this->upload->do_upload('produk_file')){//Jika upload gagal
						$data = array('error' => $this->upload->display_errors());
						print_r($data);
					}
					else{
						//Jika upload berhasil

						$upload_data = $this->upload->data();
						$config['image_library'] = 'gd2';
						$config['source_image'] = $upload_data['full_path']; //get original image
						$config['maintain_ratio'] = false;
						$config['quality'] = '100%';
						$config['width']    = $widthex[0];
						$config['height']   = $widthex[0];
						$config['x_axis']   = $x_axis[0];
						$config['y_axis']   = $y_axis[0];
						$config['overwrite'] = TRUE;
						$this->load->library('image_lib', $config);
						$this->image_lib->initialize($config);
						// Crop
						if($this->image_lib->crop()){
							$resize['width'] = 292;
							$resize['height'] = 292;
							$this->image_lib->initialize($resize);
							$this->image_lib->resize();
							$data['foto'] = $upload_data['file_name'];
						}
					}
				}
				else{ // KECIL
					$path = './assets/gambarProduk/kecil/';
					$config['upload_path'] = $path;//Aturan-aturan untuk upload file
					$config['allowed_types'] = 'gif|jpg|png|jpeg|JPG|JPEG|PNG'; 
					$config['max_size'] = 60000;
					$config['max_width'] = 4000;
					$config['max_height'] = 3000;
					$config['file_name'] = $namaprod;
					$this->load->library('upload');//Memanggil library untuk fungsi upload
					$this->upload->initialize($config);//Menetapkan aturan upload

					if(! $this->upload->do_upload('produk_file')){//Jika upload gagal
						$data = array('error' => $this->upload->display_errors());
						print_r($data);
					}
					else{
						//Jika upload berhasil

						$upload_data = $this->upload->data();
						$config['image_library'] = 'gd2';
						$config['source_image'] = $upload_data['full_path']; //get original image
						$config['maintain_ratio'] = false;
						$config['quality'] = '100%';
						$config['width']    = $widthex[0];
						$config['height']   = $widthex[0];
						$config['x_axis']   = $x_axis[0];
						$config['y_axis']   = $y_axis[0];
						$config['overwrite'] = TRUE;
						$this->load->library('image_lib', $config);
						$this->image_lib->initialize($config);
						// Crop
						if($this->image_lib->crop()){
							$resize['width'] = 152;
							$resize['height'] = 152;
							$this->image_lib->initialize($resize);
							$this->image_lib->resize();
							$data['foto'] = $upload_data['file_name'];
						}
					}
				}

			} //End Perulangan Gambar
			
				// $data['foto'] = $namaprod;
				$data['idanggota'] = $this->session->userdata('idanggota');
				$data['idproduk'] = $this->input->post('idproduk');
				$data['namaproduk'] = $this->input->post('namaproduk');
				$data['kategoriproduk'] = $this->input->post('tipeproduk');
				$data['deskripsiproduk'] = $this->input->post('deskripsiproduk');
				$data['beratpackproduk'] = $this->input->post('beratpackproduk');
				// PRODUK BIASA
				$data['hargaproduk'] = RemoveComma($this->input->post('hargaproduk'));
				$data['hargajual'] = RemoveComma($this->input->post('hargaproduk'));
				$data['keuntungan'] = floatval($data['hargajual']) * ($persenHarga / 100);
				$data['bsponsor'] = floatval($data['keuntungan']) * (20 / 100);
				$data['broyalti1'] = floatval($data['keuntungan']) * (12.5 / 100);
				$data['broyalti2'] = floatval($data['keuntungan']) * (7.5 / 100);
				$data['bloyaliti'] = floatval($data['keuntungan']) * (15 / 100);
				$data['pendapatan'] = floatval($data['keuntungan']) * (45 / 100);
				// // PRODUK HOTDEALS
				$data['hargahotdeals'] = RemoveComma($this->input->post('hargahotdeals'));
				$data['hargajualhotdeals'] = RemoveComma($this->input->post('hargahotdeals'));
				$data['keuntunganhotdeals'] = floatval($data['hargajualhotdeals']) * ($persenHarga / 100);
				$data['bsponsorhotdeals'] = floatval($data['keuntunganhotdeals']) * (20 / 100);
				$data['broyalti1hotdeals'] = floatval($data['keuntunganhotdeals']) * (12.5 / 100);
				$data['broyalti2hotdeals'] = floatval($data['keuntunganhotdeals']) * (7.5 / 100);
				$data['bloyalitihotdeals'] = floatval($data['keuntunganhotdeals']) * (15 / 100);
				$data['pendapatanhotdeals'] = floatval($data['keuntunganhotdeals']) * (45 / 100);
				$data['stblokir'] = 0;
				$data['idblokir'] = '-';
				$cekJumlah = $this->Manggota_produk_model->cekJumlahTampil();
				$cekJenisAnggota = $this->Manggota_produk_model->cekJenisAnggota();
				$data['sttampil'] = validasiProduk($cekJumlah,$cekJenisAnggota);
				$data['sttampilhotdeals'] = 0;
				$data['sthotdeals'] = 0;
				$data['updateterakhiroleh'] = '-';
				$data['tanggalbuat'] = date('Y-m-d');
				$data['alasanblokir'] = '-';
				$data['tanggalblokir'] = '0000-00-00';
				$data['tanggalawalhotdeals'] = '0000-00-00';
				$data['tanggalakhirhotdeals'] = '0000-00-00';

				$this->Manggota_produk_model->saveProduk($data);
				redirect('Manggota_produk');

		}
		else{
			$id = $this->input->post('idproduk');
			if($_FILES['produk_file']['size'] == null){
				// $data = array();
				$data['idanggota'] = $this->session->userdata('idanggota');
				$data['idproduk'] = $this->input->post('idproduk');
				$data['namaproduk'] = $this->input->post('namaproduk');
				$data['kategoriproduk'] = $this->input->post('tipeproduk');
				$data['deskripsiproduk'] = $this->input->post('deskripsiproduk');
				$data['beratpackproduk'] = $this->input->post('beratpackproduk');
				// PRODUK BIASA
				$data['hargaproduk'] = RemoveComma($this->input->post('hargaproduk'));
				$data['hargajual'] = RemoveComma($this->input->post('hargaproduk'));
				$data['keuntungan'] = floatval($data['hargajual']) * ($persenHarga / 100);
				$data['bsponsor'] = floatval($data['keuntungan']) * (20 / 100);
				$data['broyalti1'] = floatval($data['keuntungan']) * (12.5 / 100);
				$data['broyalti2'] = floatval($data['keuntungan']) * (7.5 / 100);
				$data['bloyaliti'] = floatval($data['keuntungan']) * (15 / 100);
				$data['pendapatan'] = floatval($data['keuntungan']) * (45 / 100);
				// // PRODUK HOTDEALS
				$data['hargahotdeals'] = RemoveComma($this->input->post('hargahotdeals'));
				$data['hargajualhotdeals'] = RemoveComma($this->input->post('hargahotdeals'));
				$data['keuntunganhotdeals'] = floatval($data['hargajualhotdeals']) * ($persenHarga / 100);
				$data['bsponsorhotdeals'] = floatval($data['keuntunganhotdeals']) * (20 / 100);
				$data['broyalti1hotdeals'] = floatval($data['keuntunganhotdeals']) * (12.5 / 100);
				$data['broyalti2hotdeals'] = floatval($data['keuntunganhotdeals']) * (7.5 / 100);
				$data['bloyalitihotdeals'] = floatval($data['keuntunganhotdeals']) * (15 / 100);
				$data['pendapatanhotdeals'] = floatval($data['keuntunganhotdeals']) * (45 / 100);
				$data['stblokir'] = 0;
				$data['idblokir'] = '-';
				$data['sttampil'] = $this->input->post('sttampil');
				$data['sttampilhotdeals'] = 0;
				$data['sthotdeals'] = 0;
				$data['tanggalbuat'] = date('Y-m-d');
				$data['updateterakhiroleh'] = $data['idanggota'];
				$data['alasanblokir'] = '-';
				$data['tanggalblokir'] = '0000-00-00';
				$data['tanggalawalhotdeals'] = '0000-00-00';
				$data['tanggalakhirhotdeals'] = '0000-00-00';
				$this->Manggota_produk_model->updateProduk($id,$data);
				redirect('Manggota_produk','refresh');
			}else{
				$row = $this->Manggota_produk_model->getId($id);
				if(is_file(FCPATH.'assets/gambarProduk/besar/'.$row->foto) || is_file(FCPATH.'assets/gambarProduk/sedang/'.$row->foto) || is_file(FCPATH.'assets/gambarProduk/kecil/'.$row->foto)){
					unlink('assets/gambarProduk/besar/'.$row->foto);
					unlink('assets/gambarProduk/sedang/'.$row->foto);
					unlink('assets/gambarProduk/kecil/'.$row->foto);
				}

				for($i = 1;$i <= 3; $i++){ //Perulangan Gambar
					if($i == 1){ //BESAR
						$path = './assets/gambarProduk/besar/';
						$config['upload_path'] = $path;//Aturan-aturan untuk upload file
						$config['allowed_types'] = 'gif|jpg|png|jpeg|JPG|JPEG|PNG'; 
						$config['max_size'] = 60000;
						$config['max_width'] = 4000;
						$config['max_height'] = 3000;
						$config['file_name'] = $namaprod;
						$this->load->library('upload');//Memanggil library untuk fungsi upload
						$this->upload->initialize($config);//Menetapkan aturan upload
	
						if(! $this->upload->do_upload('produk_file')){//Jika upload gagal
							$data = array('error' => $this->upload->display_errors());
							print_r($data);
						}
						else{
							//Jika upload berhasil
	
							$upload_data = $this->upload->data();
							$config['image_library'] = 'gd2';
							$config['source_image'] = $upload_data['full_path']; //get original image
							$config['maintain_ratio'] = false;
							$config['quality'] = '100%';
							$config['width']    = $widthex[0];
							$config['height']   = $widthex[0];
							$config['x_axis']   = $x_axis[0];
							$config['y_axis']   = $y_axis[0];
							$config['overwrite'] = TRUE;
							$this->load->library('image_lib', $config);
							$this->image_lib->initialize($config);
							// Crop
							if($this->image_lib->crop()){
								$resize['width'] = 413;
								$resize['height'] = 413;
								$this->image_lib->initialize($resize);
								$this->image_lib->resize();
								$data['foto'] = $upload_data['file_name'];
							}
						}
					}
					elseif($i == 2){ //SEDANG
						$path = './assets/gambarProduk/sedang/';
						$config['upload_path'] = $path;//Aturan-aturan untuk upload file
						$config['allowed_types'] = 'gif|jpg|png|jpeg|JPG|JPEG|PNG'; 
						$config['max_size'] = 60000;
						$config['max_width'] = 4000;
						$config['max_height'] = 3000;
						$config['file_name'] = $namaprod;
						$this->load->library('upload');//Memanggil library untuk fungsi upload
						$this->upload->initialize($config);//Menetapkan aturan upload
	
						if(! $this->upload->do_upload('produk_file')){//Jika upload gagal
							$data = array('error' => $this->upload->display_errors());
							print_r($data);
						}
						else{
							//Jika upload berhasil
	
							$upload_data = $this->upload->data();
							$config['image_library'] = 'gd2';
							$config['source_image'] = $upload_data['full_path']; //get original image
							$config['maintain_ratio'] = false;
							$config['quality'] = '100%';
							$config['width']    = $widthex[0];
							$config['height']   = $widthex[0];
							$config['x_axis']   = $x_axis[0];
							$config['y_axis']   = $y_axis[0];
							$config['overwrite'] = TRUE;
							$this->load->library('image_lib', $config);
							$this->image_lib->initialize($config);
							// Crop
							if($this->image_lib->crop()){
								$resize['width'] = 292;
								$resize['height'] = 292;
								$this->image_lib->initialize($resize);
								$this->image_lib->resize();
								$data['foto'] = $upload_data['file_name'];
							}
						}
					}
					else{ // KECIL
						$path = './assets/gambarProduk/kecil/';
						$config['upload_path'] = $path;//Aturan-aturan untuk upload file
						$config['allowed_types'] = 'gif|jpg|png|jpeg|JPG|JPEG|PNG'; 
						$config['max_size'] = 60000;
						$config['max_width'] = 4000;
						$config['max_height'] = 3000;
						$config['file_name'] = $namaprod;
						$this->load->library('upload');//Memanggil library untuk fungsi upload
						$this->upload->initialize($config);//Menetapkan aturan upload
	
						if(! $this->upload->do_upload('produk_file')){//Jika upload gagal
							$data = array('error' => $this->upload->display_errors());
							print_r($data);
						}
						else{
							//Jika upload berhasil
	
							$upload_data = $this->upload->data();
							$config['image_library'] = 'gd2';
							$config['source_image'] = $upload_data['full_path']; //get original image
							$config['maintain_ratio'] = false;
							$config['quality'] = '100%';
							$config['width']    = $widthex[0];
							$config['height']   = $widthex[0];
							$config['x_axis']   = $x_axis[0];
							$config['y_axis']   = $y_axis[0];
							$config['overwrite'] = TRUE;
							$this->load->library('image_lib', $config);
							$this->image_lib->initialize($config);
							// Crop
							if($this->image_lib->crop()){
								$resize['width'] = 152;
								$resize['height'] = 152;
								$this->image_lib->initialize($resize);
								$this->image_lib->resize();
								$data['foto'] = $upload_data['file_name'];
							}
						}
					}
	
				} //End Perulangan Gambar

				$data['idanggota'] = $this->session->userdata('idanggota');
				$data['idproduk'] = $this->input->post('idproduk');
				$data['namaproduk'] = $this->input->post('namaproduk');
				$data['kategoriproduk'] = $this->input->post('tipeproduk');
				$data['deskripsiproduk'] = $this->input->post('deskripsiproduk');
				$data['beratpackproduk'] = $this->input->post('beratpackproduk');
				// PRODUK BIASA
				$data['hargaproduk'] = RemoveComma($this->input->post('hargaproduk'));
				$data['hargajual'] = RemoveComma($this->input->post('hargaproduk'));
				$data['keuntungan'] = floatval($data['hargajual']) * ($persenHarga / 100);
				$data['bsponsor'] = floatval($data['keuntungan']) * (20 / 100);
				$data['broyalti1'] = floatval($data['keuntungan']) * (12.5 / 100);
				$data['broyalti2'] = floatval($data['keuntungan']) * (7.5 / 100);
				$data['bloyaliti'] = floatval($data['keuntungan']) * (15 / 100);
				$data['pendapatan'] = floatval($data['keuntungan']) * (45 / 100);
				// // PRODUK HOTDEALS
				$data['hargahotdeals'] = RemoveComma($this->input->post('hargahotdeals'));
				$data['hargajualhotdeals'] = RemoveComma($this->input->post('hargahotdeals'));
				$data['keuntunganhotdeals'] = floatval($data['hargajualhotdeals']) * ($persenHarga / 100);
				$data['bsponsorhotdeals'] = floatval($data['keuntunganhotdeals']) * (20 / 100);
				$data['broyalti1hotdeals'] = floatval($data['keuntunganhotdeals']) * (12.5 / 100);
				$data['broyalti2hotdeals'] = floatval($data['keuntunganhotdeals']) * (7.5 / 100);
				$data['bloyalitihotdeals'] = floatval($data['keuntunganhotdeals']) * (15 / 100);
				$data['pendapatanhotdeals'] = floatval($data['keuntunganhotdeals']) * (45 / 100);
				$data['stblokir'] = 0;
				$data['idblokir'] = '-';
				$data['sttampil'] = $this->input->post('sttampil');
				$data['sttampilhotdeals'] = 0;
				$data['sthotdeals'] = 0;
				$data['updateterakhiroleh'] = $data['idanggota'];
				$data['tanggalbuat'] = date('Y-m-d');
				$data['alasanblokir'] = '-';
				$data['tanggalblokir'] = '0000-00-00';
				$data['tanggalawalhotdeals'] = '0000-00-00';
				$data['tanggalakhirhotdeals'] = '0000-00-00';
				$this->Manggota_produk_model->updateProduk($id,$data);
				redirect('Manggota_produk');
				// print_r($this->Manggota_produk_model->updateProduk($id,$data));
			}
		}
	}

	function nonaktifProduk(){
		$id = $this->uri->segment(3);
		if($this->Manggota_produk_model->nonaktifProduk($id)){
			redirect('Manggota_produk','refresh');
		}else{
			print_r($this->Manggota_produk_model->nonaktifProduk($id));
		}
	}

	function aktifProduk(){
		$id = $this->uri->segment(3);
		if($this->Manggota_produk_model->aktifProduk($id)){
			redirect('Manggota_produk','refresh');
		}else{
			print_r($this->Manggota_produk_model->aktifProduk($id));
		}
	}

	function hotDeals(){
		$id = $this->uri->segment(3);
		if($this->Manggota_produk_model->hotDeals($id)){
			redirect('Manggota_produk','refresh');
		}else{
			print_r($this->Manggota_produk_model->hotDeals($id));
		}
	}

	function hotDealsPending(){
		$id = $this->uri->segment(3);
		if($this->Manggota_produk_model->hotDealsPending($id)){
			redirect('Manggota_produk','refresh');
		}else{
			print_r($this->Manggota_produk_model->hotDealsPending($id));
		}
	}

	function hotDealsNonaktif(){
		$id = $this->uri->segment(3);
		if($this->Manggota_produk_model->hotDealsNonaktif($id)){
			redirect('Manggota_produk','refresh');
		}else{
			print_r($this->Manggota_produk_model->hotDealsNonaktif($id));
		}
	}


	function edit(){
		$id = $this->uri->segment(3);
		$r = $this->Manggota_produk_model->getId($id);
		$data = array();
		$data['idproduk'] = $r->idproduk;
		$data['namaProduk'] = $r->namaproduk;
		$data['kategoriProduk'] = $r->kategoriproduk;
		$data['deskripsiProduk'] = $r->deskripsiproduk;
		$data['beratProduk'] = $r->beratpackproduk;
		$data['hargaProduk'] = $r->hargaproduk;
		$data['hargaJual'] = $r->hargajual;
		$data['keuntungan'] = $r->keuntungan;
		$data['sttampil'] = $r->sttampil;
		$data['foto'] = $r->foto;
		$data['hargahotdeals'] = $r->hargahotdeals;
		$data['hargajualhotdeals'] = $r->hargajualhotdeals;
		$data['error'] 			= '';
		$data['toptitle'] 		= 'Edit Data Produk';
		$data['title'] 			= 'Edit Data Produk';
		$data['content'] 		= 'Manggota_produk/manage';

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}

	function cekJumlahTampil(){
		$data = array();
		$data['jumlah'] = $this->Manggota_produk_model->cekJumlahTampil();
		$data['jenisanggota'] = $this->Manggota_produk_model->cekJenisAnggota();
		$this->output->set_output(json_encode($data));
	}


}