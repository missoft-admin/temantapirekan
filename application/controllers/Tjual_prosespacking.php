<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tjual_prosespacking extends CI_Controller {

	/**
	 * Master Anggota Produk controller.
	 * Developer @RendyIchtiarSaputra
	 */

	public function __construct(){
		parent::__construct();
		// PermissionUserLoggedIn($this->session);
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tjual_prosespacking_model');
	}

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['toptitle'] 		= 'PENJUALAN';
		$data['title'] 			= 'EKSEKUSI PACKING';
		$data['content'] 		= 'Tjual_prosespacking/index';

		$data['list_index'] = $this->Tjual_prosespacking_model->getAll();

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}

	function prosesPacking($id){
		if($this->Tjual_prosespacking_model->prosesPacking($id)){
			redirect('Tjual_prosespacking');
			return true;
		}else{
			print_r($this->Tjual_prosespacking_model->prosesPacking($id));
		}
	}

	function getDetailTransaksi($id){
		$this->output->set_output(json_encode($this->Tjual_prosespacking_model->getDetailTransaksi($id)));
	}

	function getSum($id){
		$this->output->set_output(json_encode($this->Tjual_prosespacking_model->getSum($id)));
	}

	function detailBayar($idtransaksi){
		$data = get_all('tjualbayar',array('idtransaksi' => $idtransaksi));
		$this->output->set_output(json_encode($data));
	}

}

/* End of file Tjual_prosespacking.php */
/* Location: ./application/controllers/Tjual_prosespacking.php */