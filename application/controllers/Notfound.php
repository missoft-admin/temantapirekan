<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Notfound extends CI_Controller
{

    /**
     * Master Anggota Produk controller.
     * Developer @RendyIchtiarSaputra
     */

    public function __construct()
    {
        parent::__construct();
        // PermissionUserLoggedIn($this->session);
    }

    public function index()
    {
        $data = array();
        $data = array_merge($data, path_variable());
        $this->load->view('page_notfound', $data);
    }
}
