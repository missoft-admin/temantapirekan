<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GantiPassword extends CI_Controller {

	function __construct(){
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->model('Auth_model');
	}

	public function index(){
		$data = array();
		$data['error'] 			= '';
		$data['toptitle'] 		= 'Ganti Password';
		$data['title'] 			= 'Ganti Password';
		$data['content'] 		= 'GantiPassword/index';

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}

	function update(){
		$passwordlama = $this->input->post('passwordlama');
		$passwordbaru = $this->input->post('passwordbaru');
		$passwordbarukonfirmasi = $this->input->post('passwordbarukonfirmasi');
		$row = $this->Auth_model->getPassword();
		if(hash('sha256',md5($passwordlama)) == $row->passkey){
			if($passwordbaru == $passwordbarukonfirmasi){
				$this->Auth_model->updatePass($passwordbaru);
				redirect('dashboard');
			}else{
				$data = array();
				$data['error'] 			= 'Password Baru Tidak Sesuai Dengan Password Konfirmasi';
				$data['toptitle'] 		= 'Ganti Password';
				$data['title'] 			= 'Ganti Password';
				$data['content'] 		= 'GantiPassword/index';

				$data = array_merge($data, path_variable());
				$this->parser->parse('page_template', $data);	
			}
		}else{
			$data = array();
			$data['error'] 			= 'Password Lama Salah';
			$data['toptitle'] 		= 'Ganti Password';
			$data['title'] 			= 'Ganti Password';
			$data['content'] 		= 'GantiPassword/index';

			$data = array_merge($data, path_variable());
			$this->parser->parse('page_template', $data);	
		}

	}

}

/* End of file GantiPassword.php */
/* Location: ./application/controllers/GantiPassword.php */