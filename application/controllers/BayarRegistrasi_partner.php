<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BayarRegistrasi_partner extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('BayarRegistrasi_model');
	}

	public function index(){
		$data = array();
		$data['error'] 			= '';
		$data['toptitle'] 		= 'Registrasi Partner';
		$data['title'] 			= 'Bayar Registrasi Partner';
		$data['content'] 		= 'BayarRegistrasi_partner/index';
		$codex = $this->BayarRegistrasi_model->getKode();
		$nomor1 = $codex + 1;
		$kode = 'BRP'.date('y').date('m').date('d').str_pad($nomor1, '6',"0",STR_PAD_LEFT);
		$data['kode'] = $kode;
		$data['list_detail'] = $this->BayarRegistrasi_model->getDetail();
		$data['sumDetail'] = $this->BayarRegistrasi_model->getSumDetail();

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}

	function save(){
		$config['upload_path'] = './assets/gambarProduk/';//Aturan-aturan untuk upload file
		$config['allowed_types'] = 'gif|jpg|png|jpeg|JPG|JPEG|PNG'; 
		$config['max_size'] = 60000;
		$config['max_width'] = 4000;
		$config['max_height'] = 3000;
		$config['encrypt_name'] = true;

		$this->load->library('upload');//Memanggil library untuk fungsi upload
		$this->upload->initialize($config);//Menetapkan aturan upload

		if(! $this->upload->do_upload('foto')){//Jika upload gagal
			$data = array('error' => $this->upload->display_errors());
			print_r($data);
		}
		else{//Jika upload berhasil
			
			$foto_instructor = $this->upload->data();

			$data = array();
			$data['fotobuktitransfer'] = $foto_instructor['file_name'];
			$data['idbayarregpartner'] = $this->input->post('idbayarregpartner');
			$data['nominaltransaksi'] = RemoveComma($this->input->post('nominaltransaksi'));
			$data['namabank'] = $this->input->post('namabank');
			$data['norekening'] = $this->input->post('norekening');
			$data['atasnama'] = $this->input->post('atasnama');
			$data['idanggotapartner'] = $this->session->userdata('idanggota');
			$data['tanggalbayar'] = date('Y-m-d');

			if($this->BayarRegistrasi_model->simpanBayaran($data)){
				$detail = json_decode($this->input->post('detaildata'));
				foreach($detail as $r){
					$this->db->set('idbayarregpartner', $data['idbayarregpartner']);
					$this->db->set('periodebayar', $r[0]);
					$this->db->set('jenispartner', $r[3]);
					$this->db->set('nominaltransaksi', RemoveComma($r[2]));
					$this->db->insert('tbayarregistrasipartnerdetail');
				}
				
				$no_urut = 1;
				foreach($detail as $s){
					$this->db->set('idanggota', $data['idanggotapartner']);
					$this->db->set('nourut', $no_urut++);
					$this->db->set('periodepartner', $s[0]);
					$this->db->set('jenisanggota', $s[3]);
					$this->db->set('staktif', 1);
					$this->db->insert('manggotamasaaktif');
				}

				redirect('BayarRegistrasi_partner/riwayat_registrasi');
			}
		}
	}

	function riwayat_registrasi(){
		$data = array();
		$data['error'] 			= '';
		$data['toptitle'] 		= 'Riwayat Registrasi';
		$data['title'] 			= 'Riwayat Registrasi';
		$data['content'] 		= 'BayarRegistrasi_partner/riwayat_registrasi';		
		$data['list_index'] = $this->BayarRegistrasi_model->getDetail();
		$data['sumDetail'] = number_format($this->BayarRegistrasi_model->getSumDetail());

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}

}

/* End of file bayarRegistrasi_partner.php */
/* Location: ./application/controllers/bayarRegistrasi_partner.php */