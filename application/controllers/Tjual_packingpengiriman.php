<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tjual_packingpengiriman extends CI_Controller {

	public function __construct(){
		parent::__construct();
		// PermissionUserLoggedIn($this->session);
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Tjual_packingpengiriman_model');
	}

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['toptitle'] 		= 'PENJUALAN';
		$data['title'] 			= 'PENGIRIMAN PRODUK';
		$data['content'] 		= 'Tjual_packingpengiriman/index';

		$data['list_index'] = $this->Tjual_packingpengiriman_model->getAll();

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}

	function prosesPengiriman($id){
		if($this->Tjual_packingpengiriman_model->prosesPengiriman($id)){
			redirect('Tjual_packingpengiriman');
			return true;
		}else{
			print_r($this->Tjual_packingpengiriman_model->prosesPengiriman($id));
		}
	}

}

/* End of file Tjual_packingpengiriman.php */
/* Location: ./application/controllers/Tjual_packingpengiriman.php */