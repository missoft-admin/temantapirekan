<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Referensi_ekspedisi extends CI_Controller {

	/**
	 * Master Anggota Produk controller.
	 * Developer @RendyIchtiarSaputra
	 */

	public function __construct()
  {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		// PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Referensi_ekspedisi_model');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['toptitle'] 		= 'Referensi Ekspedisi';
		$data['title'] 			= 'Data Referensi Ekspedisi';
		$data['content'] 		= 'Referensi_ekspedisi/index';

		$data['list_index'] = $this->Referensi_ekspedisi_model->getAll();

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}

	function tambah(){
		$data['error'] 			= '';
		$data['toptitle'] 		= 'Referensi Ekspedisi';
		$data['title'] 			= 'Tambah Referensi Ekspedisi';
		$data['content'] 		= 'Referensi_ekspedisi/manage';

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);	
	}

	function edit(){
		$id = $this->uri->segment(3);
		$r = $this->Referensi_ekspedisi_model->getId($id);
		$data = array();
		$data['idekspedisi'] = $r->idekspedisi;
		$data['namaekspedisi'] = $r->namaekspedisi;
		$data['kota'] = $r->kota;
		$data['staktif'] = $r->staktif;
		$data['kecamatan'] = $r->kecamatan;
		$data['kelurahan'] = $r->kelurahan;
		$data['harga'] = $r->harga;
		$data['error'] 			= '';
		$data['toptitle'] 		= 'Referensi Ekspedisi';
		$data['title'] 			= 'Edit Referensi Ekspedisi';
		$data['content'] 		= 'Referensi_ekspedisi/manage';

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);	
	}

	function save(){
		$data = array(); //Manggota Ekspedisi
		$data['idanggota'] = $this->session->userdata('idanggota');
		$data['idekspedisi'] = $this->input->post('idekspedisi');
		$data['namaekspedisi'] = $this->input->post('namaekspedisi');
		$data['staktif'] = '1';

		// if($this->Referensi_ekspedisi_model->cekData() < 1){
			if($this->Referensi_ekspedisi_model->saveEkspedisi($data)){
				$detail = json_decode($this->input->post('detaildata'));
				foreach($detail as $r){
					$this->db->set('idanggota', $data['idanggota']);
					$this->db->set('idekspedisi', $data['idekspedisi']);
					$this->db->set('provinsi', $r[5]);
					$this->db->set('kota', $r[6]);
					$this->db->set('kecamatan', $r[7]);
					$this->db->set('desa', $r[8]);
					$this->db->set('harga', RemoveComma($r[4]));
					$this->Referensi_ekspedisi_model->saveDetail();
				}
				redirect('referensi_ekspedisi');
			}
		// }else{
			$detail = json_decode($this->input->post('detaildata'));
				foreach($detail as $r){
					$this->db->set('idanggota', $data['idanggota']);
					$this->db->set('idekspedisi', $data['idekspedisi']);
					$this->db->set('provinsi', $r[5]);
					$this->db->set('kota', $r[6]);
					$this->db->set('kecamatan', $r[7]);
					$this->db->set('desa', $r[8]);
					$this->db->set('harga', RemoveComma($r[4]));
					$this->Referensi_ekspedisi_model->saveDetail();
				}
				redirect('referensi_ekspedisi');
		// }
	}

	function hapus($provinsi,$kota,$kecamatan,$desa){
		if($this->Referensi_ekspedisi_model->hapusEkspedisi($provinsi,$kota,$kecamatan,$desa)){
			redirect('Referensi_ekspedisi','refresh');
			return true;
		}else{
			print_r($this->Referensi_ekspedisi_model->hapusEkspedisi($provinsi,$kota,$kecamatan,$desa));
		}
	}

	function getKecamatan($kota){
		$data = get_all('mkecamatan',array('kotaid' => $kota),'nama');
		$this->output->set_output(json_encode($data));
	}

	function getDesa($kecamatan){
		$data = get_all('mdesa',array('kecamatanid' => $kecamatan),'nama');
		$this->output->set_output(json_encode($data));
	}

	function getKota($provinsi){
		$data = get_all('mkota',array('provinsiid' => $provinsi),'nama');
		$this->output->set_output(json_encode($data));
	}

	function updateHarga(){
		$this->Referensi_ekspedisi_model->updateHarga();
	}

	function getKodePos($provinsi,$kota,$kecamatan,$desa){
		$data = get_all('mkodepos',array('provinsiid' => $provinsi, 'kotaid' => $kota, 'kecamatanid' => $kecamatan, 'desaid' => $desa));
		$this->output->set_output(json_encode($data));
	}

	function getReff(){
		$provinsi = $this->input->post('provinsi');
		$kota = $this->input->post('kota');
		$kecamatan = $this->input->post('kecamatan');
		$desa = $this->input->post('desa');

		// Cek Validasi
		if($provinsi != 'null' && $kota == 'null' && $kecamatan == 'null' && $desa == 'null'){
			$data = $this->Referensi_ekspedisi_model->getKotKecDes($provinsi);
			$this->output->set_output(json_encode($data));
		}
		elseif($provinsi != 'null' && $kota != 'null' && $kecamatan == 'null' && $desa == 'null'){
			$data = $this->Referensi_ekspedisi_model->getKecDes($kota);
			$this->output->set_output(json_encode($data));
		}
		elseif($provinsi != 'null' && $kota != 'null' && $kecamatan != 'null' && $desa == 'null'){
			$data = $this->Referensi_ekspedisi_model->getDes($kecamatan);
			$this->output->set_output(json_encode($data));
		}else{
			echo "GAGAL";
		}
	}

}

/* End of file Referensi_ekspedisi.php */
/* Location: ./application/controllers/Referensi_ekspedisi.php */