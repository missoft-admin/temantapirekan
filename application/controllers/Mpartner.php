<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| DEVELOPER 	: IYAN ISYANTO
|--------------------------------------------------------------------------
|
*/

class Mpartner extends CI_Controller {

	public function __construct() {
		parent::__construct();
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Mpartner_model','model');
	}

	public function index(){
		$data['error'] 			= '';
		$data['toptitle'] 		= 'Partner';
		$data['title'] 			= 'Daftar Partner';
		$data['content'] 		= 'Mpartner/index';

		// $data['list_index'] = $this->model->getAll();

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}

	public function edit_informasi() {
		$id = $this->uri->segment(3);
		if($id) {
			$data = $this->model->getPartnerInformasi($id);
			if($data) {
				$data['error'] 			= '';
				$data['toptitle'] 		= 'Edit Informasi Penjual';
				$data['title'] 			= 'Formulir';
				$data['content'] 		= 'Mpartner/edit_informasi';

				$data = array_merge($data, path_variable());
				$this->parser->parse('page_template', $data);
			} else {
				setFlashMsg('error','data tidak ditemukan.','mpartner');
			}
		} else {
			setFlashMsg('error','data tidak ditemukan.','mpartner');
		}
	}


	public function save_informasi(){
		// print_r($this->input->post('reswidth2').' width, height: '.$this->input->post('resheight2').','.$this->input->post('x_axis2').' width Y, height X: '.$this->input->post('y_axis2'));exit();
		// $this->form_validation->set_rules('namaanggota.', .'Nama', 'trim|required|min_length[4]|max_length[60]|alpha_numeric_spaces');
		// // $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required|min_length[1]|max_length[200]');
		// // $this->form_validation->set_rules('desa', 'Desa', 'trim|required|min_length[1]|max_length[50]');
		// // $this->form_validation->set_rules('kecamatan', 'Kecamatan', 'trim|required|min_length[1]|max_length[50]');
		// // $this->form_validation->set_rules('kota', 'Kota', 'trim|required|min_length[1]|max_length[40]');
		// // $this->form_validation->set_rules('kodepos', 'Kode Pos', 'trim|required|min_length[1]|max_length[10]');
		// // $this->form_validation->set_rules('agama', 'Agama', 'trim|required|min_length[1]|max_length[15]');
		// // $this->form_validation->set_rules('jeniskelamin', 'Jenis Kelamin', 'trim|required|min_length[1]|max_length[1]');
		// // $this->form_validation->set_rules('statuspernikahan', 'Status Nikah', 'trim|required|min_length[1]|max_length[1]');
		// // $this->form_validation->set_rules('idanggota', 'ID', 'trim|required|min_length[1]|max_length[12]');

		// if($this->form_validation->run() == TRUE) {
			if($this->model->saveInformasi()) {
				setFlashMsg('confirm','data telah disimpan.','mpartner/edit_informasi/'.$this->session->userdata('idanggota'));
			} else {
				// setFlashMsg('error','data gagal disimpan.','mpartner/edit_informasi/'.$this->session->userdata('idanggota'));
				print_r($this->model->saveInformasi());
			}
		// } else {
		// 	// setFlashMsg('error','data gagal disimpan.','mpartner/edit_informasi/'.$this->session->userdata('idanggota'));
		// 	print_r($this->model->saveInformasi());
		// }
	}

	public function failed_save_informasi() {
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mpartner/edit_informasi';

		$data['toptitle'] 	= 'Edit Informasi Partner';
		$data['title'] 		= 'Formulir';

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}

	public function edit_rekening() {
		$id = $this->uri->segment(3);
		if($id) {
			$cekAnggota = $this->model->getPartnerInformasi($id);
			if($cekAnggota) {
				$data = $cekAnggota;
				$data['error'] 			= '';
				$data['toptitle'] 		= 'Edit Rekening Partner';
				$data['title'] 			= 'Formulir';
				$data['content'] 		= 'Mpartner/edit_rekening';

				$data = array_merge($data, path_variable());
				$this->parser->parse('page_template', $data);
			} else {
				setFlashMsg('error','data tidak ditemukan.','mpartner');
			}
		} else {
			setFlashMsg('error','data tidak ditemukan.','mpartner');
		}
	}


	public function save_rekening(){
		$this->form_validation->set_rules('norekening', 'Nomor Rekening', 'trim|required|min_length[1]|max_length[20]');
		$this->form_validation->set_rules('namabank', 'Nama Bank', 'trim|required|min_length[1]|max_length[50]');
		$this->form_validation->set_rules('cabangbank', 'Cabang Bank', 'trim|required|min_length[1]|max_length[50]');

		if($this->form_validation->run() == TRUE) {
			if($this->model->saveRekening()) {
				setFlashMsg('confirm','data telah disimpan.','mpartner');
			} else {
				setFlashMsg('error','data gagal disimpan.','mpartner');
			}
		} else {
			$this->failed_save_rekening($this->input->post('idanggota'));
		}
	}

	public function failed_save_rekening() {
		$data = $this->input->post();
		$data['error'] 	 = validation_errors();
		$data['content'] = 'Mpartner/edit_rekening';

		$data['toptitle'] 	= 'Edit Rekening Partner';
		$data['title'] 		= 'Formulir';

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}

	public function verifikasi_bayar_pendaftaran() {
		$id = $this->uri->segment(3);
		if($id) {
			$cekPembayaran = $this->model->getBayarDaftarPartner($id);
			if($cekPembayaran) {
				$data = $this->model->getBayarDaftarPartner($id);
				$data['error'] 			= '';
				$data['toptitle'] 		= 'Verifikasi Pembayaran Pendaftaran';
				$data['title'] 			= 'Informasi Pembayaran Pendaftaran';
				$data['content'] 		= 'Mpartner/verifikasi_bayar_pendaftaran';

				$data = array_merge($data, path_variable());
				$this->parser->parse('page_template', $data);
			} else {
				setFlashMsg('error','data pembayaran tidak ditemukan.','mpartner');
			}
		} else {
			show_404();
		}
	}

	public function verifikasi_bayar_pendaftaran_decision() {
		$id = $this->uri->segment(3);
		if($id) {
			$data = $this->model->getBayarDaftarPartner($id);
			$data['error'] 			= '';
			$data['toptitle'] 		= 'Verifikasi Pembayaran Pendaftaran';
			$data['title'] 			= 'Ketetapan Informasi Pembayaran';
			$data['content'] 		= 'Mpartner/verifikasi_bayar_pendaftaran_decision';

			$data = array_merge($data, path_variable());
			$this->parser->parse('page_template', $data);
		} else {
			setFlashMsg('error','data tidak ditemukan.','mpartner');
		}
	}

	public function save_verifikasi_bayar_pendaftaran() {
		$this->form_validation->set_rules('idanggota', 'ID', 'trim|required|min_length[1]|max_length[12]');
		$this->form_validation->set_rules('periodeawal', 'Periode Awal', 'trim|required');
		$this->form_validation->set_rules('periodeakhir', 'Periode Akhir', 'trim|required');
		if($this->form_validation->run() === TRUE) {
			$periode 	= abs($this->input->post('periodeawal') - $this->input->post('periodeakhir'));
			$idanggota 	= $this->input->post('idanggota');

			if($this->model->saveVerifikasiBayarPendaftaran( $idanggota,$periode )) {
				setFlashMsg('confirm','data telah diverifikasi.','mpartner');
			} else {
				setFlashMsg('error','data gagal diverifikasi.','mpartner');
			}
		} else {
			setFlashMsg('error','data gagal diverifikasi.','mpartner');
		}
	}

	public function blokir_partner() {
		$id = $this->uri->segment(3);
		if($id) {
			if($this->model->blokirPartner($id)) {
				setFlashMsg('confirm','data telah diblokir.','mpartner');
			} else {
				setFlashMsg('error','data gagal diblokir.','mpartner');
			}
		} else {
			setFlashMsg('error','data tidak ditemukan.','mpartner');
		}
	}

	public function aktifkan_partner() {
		$id = $this->uri->segment(3);
		if($id) {
			if($this->model->aktifkanPartner($id)) {
				setFlashMsg('confirm','data telah diaktifkan.','mpartner');
			} else {
				setFlashMsg('error','data gagal diaktifkan.','mpartner');
			}
		} else {
			setFlashMsg('error','data tidak ditemukan diaktifkan.','mpartner');
		}
	}

	public function detail() {
		$id = $this->uri->segment(3);
		if($id) {
			$getTab = $this->input->get('tab');
			$detailPartner = $this->model->getDetailPartner($id);
			if($getTab == 'produk') {
				if($detailPartner) {
					$getEdit = $this->input->get('edit');
					if($getTab == 'produk' && $getEdit) {
						$produkPartnerDetail = $this->model->getProdukPartnerDetail($id, $getEdit);
						if($produkPartnerDetail) {
							$data 					= array_merge($detailPartner,$produkPartnerDetail);
							$data['error'] 			= '';
							$data['toptitle'] 		= 'Edit Produk ' . $produkPartnerDetail['namaproduk'];
							$data['title'] 			= 'Formulir';
							$data['content'] 		= 'Mpartner/edit_produk';
							$data['listProduk'] 	= $this->model->getProdukPartner($data['idanggota']);
						} else {
							setFlashMsg('error','data produk tidak ditemukan.','mpartner/detail/'.$id.'?tab=produk');
						}
					} else {
						$data 					= $detailPartner;
						$data['error'] 			= '';
						$data['toptitle'] 		= 'Produk ' . $data['namaanggota'];
						$data['title'] 			= 'Daftar Produk';
						$data['content'] 		= 'Mpartner/produk';
						$data['listProduk'] 	= $this->model->getProdukPartner($data['idanggota']);
					}
				} else {
					setFlashMsg('error','data tidak ditemukan.','mpartner');
				}
			}
			$data = array_merge($data, path_variable());
			$this->parser->parse('page_template', $data);
		} else {
			setFlashMsg('error','data tidak ditemukan.','mpartner');
		}
	}

	public function save_produk_partner() {
		$this->form_validation->set_rules('namaproduk', 'Nama Produk', 'trim|required|min_length[4]|max_length[100]');
		$this->form_validation->set_rules('tipeproduk', 'Tipe Produk', 'trim|required|min_length[1]|max_length[15]');
		$this->form_validation->set_rules('beratpackproduk', '', 'trim|required|min_length[1]|max_length[10]');
		$this->form_validation->set_rules('hargaproduk', '', 'trim|required|min_length[1]|max_length[20]');
		$this->form_validation->set_rules('hargajual', '', 'trim|required|min_length[1]|max_length[20]');
		$this->form_validation->set_rules('keuntungan', '', 'trim|required|min_length[1]|max_length[20]');
		$this->form_validation->set_rules('bsponsor', '', 'trim|required|min_length[1]|max_length[20]');
		$this->form_validation->set_rules('bpartner1', '', 'trim|required|min_length[1]|max_length[20]');
		$this->form_validation->set_rules('bpartner2', '', 'trim|required|min_length[1]|max_length[20]');
		$this->form_validation->set_rules('bloyaliti', '', 'trim|required|min_length[1]|max_length[20]');
		$this->form_validation->set_rules('pendapatan', '', 'trim|required|min_length[1]|max_length[20]');

		if($this->input->post('sthotdeals') == 1) {
			$this->form_validation->set_rules('tanggalawalhotdeals', '', 'trim|required');
			$this->form_validation->set_rules('tanggalakhirhotdeals', '', 'trim|required');
		}

		if($this->form_validation->run() == TRUE) {
			if($this->model->saveProdukPartner()) {
				setFlashMsg('confirm','data telah disimpan.','mpartner/detail/'.$this->input->post('idanggota').'?tab=produk');
			} else {
				setFlashMsg('error','data gagal disimpan.','mpartner/detail/'.$this->input->post('idanggota').'?tab=produk');
			}
		} else {
			$this->failed_save_produk_partner($this->input->post('idanggota'));
		}
	}

	public function failed_save_produk_partner($id) {
		$post = $this->input->post();
		$detailPartner = $this->model->getDetailPartner($post['idanggota']);
		$produkPartnerDetail = $this->model->getProdukPartnerDetail($post['idanggota'], $post['idproduk']);

		$data 					= array_merge($detailPartner,$produkPartnerDetail,$post);
		$data['error'] 	 		= validation_errors();
		$data['toptitle'] 		= 'Edit Produk ' . $produkPartnerDetail['namaproduk'];
		$data['title'] 			= 'Formulir';
		$data['content'] 		= 'Mpartner/edit_produk';

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}


	public function getKota($id) {
		if($id) {
			$options = json_encode( createOptionsDropdown('mkota','id','nama',array('provinsiid' => $id)) ) ;
			$this->output->set_output($options);
		} else {

		}
	}

	public function getKecamatan($id) {
		if($id) {
			$options = json_encode( createOptionsDropdown('mkecamatan','id','nama',array('kotaid' => $id)) ) ;
			$this->output->set_output($options);
		} else {

		}
	}

	public function getDesa($id) {
		if($id) {
			$options = json_encode( createOptionsDropdown('mdesa','id','nama',array('kecamatanid' => $id)) ) ;
			$this->output->set_output($options);
		} else {

		}
	}

	public function getKodepos($id) {
		if($id) {
			$options = json_encode( getByFields('mkodepos', array('desaid' => $id) ) ) ;
			$this->output->set_output($options);
		} else {

		}
	}

	public function getIndex()
  {
      $this->table = 'manggota';
      $this->column_search   = array('namaanggota','idsponsor','alamat','jenisanggota', 'staktif');
      $this->column_order    = array('namaanggota','idsponsor','alamat','jenisanggota', 'staktif');
      $this->order           = array('jenisanggota' => 'desc', 'staktif' => 'desc', 'namaanggota' => 'desc');
      $this->where           = array('tipeanggota' => 2, 'idanggota !=' => '000000000000');

      $list = $this->datatable->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $r) {
          $no++;
          $row = array();

					if($r->idsponsor){
						$sponsor = getByFields('manggota', array('idanggota' => $r->idsponsor) )->namaanggota;
					}else{
						$sponsor = '-';
					}

					if($r->staktif == 1){
						$aksi = '<a class="dropdown-item" href="./mpartner/blokir_partner/'. $r->idanggota .'" onclick="return confirm("Anda yakin akan memblokir member ini ?");">Blokir Member</a>';
					}else{
						$aksi = '<a class="dropdown-item" href="./mpartner/aktifkan_partner/'.$r->idanggota.'" onclick="return confirm("Anda yakin akan aktifkan member ini ?");">Aktifkan Partner</a>';
					}

          $row[] = $r->namaanggota;
          $row[] = $sponsor;
          $row[] = $r->alamat;
          $row[] = jenisAnggotaPartner($r->jenisanggota);
          $row[] = statusMember($r->staktif);
          $row[] = '<div class="dropdown">
						<button class="btn white dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-gear"></i> </button>
						<div class="dropdown-menu" x-placement="bottom-start">
							<a class="dropdown-item" href="./mpartner/edit_informasi/'.$r->idanggota.'">Edit Informasi</a>
							<a class="dropdown-item" href="./mpartner/edit_rekening/'.$r->idanggota.'">Edit Rekening</a>
							<a class="dropdown-item" href="./mpartner/verifikasi_bayar_pendaftaran/'.$r->idanggota.'">Verifikasi Bayar Pendaftaran</a>
							'.$aksi.'
						</div>
						<div class="btn-group">
							'.anchor('mpartner/detail/'.$r->idanggota.'?tab=produk', 'Data Produk', 'data-toggle="tooltip" title="View Partner" class="btn btn-outline primary"').'
						</div>
					</div>';
          $row[] = $r->jenisanggota;

          $data[] = $row;
      }
      $output = array(
	      "draw" => $_POST['draw'],
	      "recordsTotal" => $this->datatable->count_all(),
	      "recordsFiltered" => $this->datatable->count_all(),
	      "data" => $data
      );
      echo json_encode($output);
  }
}

/* End of file Mpartner.php */
/* Location: ./application/controllers/Mpartner.php */
