<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manggota extends CI_Controller {

	/**
	 * Master Anggota controller.
	 * Developer @RendyIchtiarSaputra
	 */

	public function __construct()
  {
		parent::__construct();
		// PermissionUserLoggedIn($this->session);
		PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('Manggota_model');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['toptitle'] 		= 'Data Pribadi';
		$data['title'] 			= 'Data Pribadi';
		$data['content'] 		= 'Manggota/index';

		$data['list_index'] = $this->Manggota_model->getAll();

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}

	function manage(){
		$data = array();
		$data['error'] 			= '';
		$data['toptitle'] 		= 'Edit Data Pribadi';
		$data['title'] 			= 'Edit Data Pribadi';
		$data['content'] 		= 'Manggota/manage';

		$data['list_index'] = $this->Manggota_model->getAll();

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);	
	}

	function rekening(){
		$data = array();
		$data['error'] 			= '';
		$data['toptitle'] 		= 'Edit Data Rekening';
		$data['title'] 			= 'Edit Data Rekening';
		$data['content'] 		= 'Manggota/manageRek';

		$data['list_index'] = $this->Manggota_model->getAll();

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);	
	}

	function edit(){
		if($this->Manggota_model->edit()){
			redirect('dashboard','refresh');
		}else{
			echo "GAGAL";
		}
	}

	function editRekening(){
		$data = array();
		$data['norekening'] = $this->input->post('norekening');
		$data['namabank'] = $this->input->post('namabank');
		$data['cabangbank'] = $this->input->post('cabangbank');
		$data['atasnama'] = $this->input->post('atasnama');		

		$this->db->where('idanggota',$this->session->userdata('idanggota'));
		$this->db->update('manggota', array('norekening' => $this->input->post('norekening'), 'namabank' => $this->input->post('namabank'), 'cabangbank' => $this->input->post('cabangbank'), 'atasnama' => $this->input->post('atasnama')));
		redirect('manggota/rekening');
	}

	function peta_jaringan($id){

		$data = array();
		$data['error'] 			= '';
		$data['toptitle'] 		= 'Anggota Rekomendasi';
		$data['title'] 			= 'Anggota Rekomendasi';
		$data['content'] 		= 'Manggota/petajaringan';
		$data['faSemua'] = 'fa fa-check';
		$data['faPenjual'] = '';
		$data['faPembeli'] = '';
		$data['semua']			= 'active';
		$data['penjual']		= '';
		$data['pembeli']		= '';
		$data['anggota']		= 'Anggota';
		$data['peta']			= 'semua';
		if($id == 0){
			$data['bulan']			= date('m');
			$data['tahun']			= date('Y');
		}else{
			$data['bulan']			= $this->input->post('bulan');
			$data['tahun']			= $this->input->post('tahun');
		}

		$data['list_index1']	= $this->Manggota_model->getPetaMember($id);
		$data['list_index2']	= $this->Manggota_model->getPetaPartner($id);

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}

	function petaPembeli($id){
		$data = array();
		$data['error'] 			= '';
		$data['toptitle'] 		= 'Anggota Rekomendasi';
		$data['title'] 			= 'Anggota Rekomendasi';
		$data['content'] 		= 'Manggota/petajaringan';
		$data['faSemua'] = '';
		$data['faPenjual'] = '';
		$data['faPembeli'] = 'fa fa-check';
		$data['semua']			= '';
		$data['penjual']		= '';
		$data['pembeli']		= 'active';
		$data['anggota']		= 'Pembeli';
		$data['peta']			= 'pembeli';
		
		if($id == 0){
			$data['bulan']			= date('m');
			$data['tahun']			= date('Y');
		}else{
			$data['bulan']			= $this->input->post('bulan');
			$data['tahun']			= $this->input->post('tahun');
		}

		$data['list_index1']	= $this->Manggota_model->getPetaMember($id);
		$data['list_index2']	= $this->Manggota_model->getPetaPartner($id);

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}

	function petaPenjual($id){
		$data = array();
		$data['error'] 			= '';
		$data['toptitle'] 		= 'Anggota Rekomendasi';
		$data['title'] 			= 'Anggota Rekomendasi';
		$data['content'] 		= 'Manggota/petajaringan';
		$data['faSemua'] = '';
		$data['faPenjual'] = 'fa fa-check';
		$data['faPembeli'] = '';
		$data['semua']			= '';
		$data['penjual']		= 'active';
		$data['pembeli']		= '';
		$data['anggota']		= 'Penjual';
		$data['peta']			= 'penjual';

		if($id == 0){
			$data['bulan']			= date('m');
			$data['tahun']			= date('Y');
		}else{
			$data['bulan']			= $this->input->post('bulan');
			$data['tahun']			= $this->input->post('tahun');
		}

		$data['list_index1']	= $this->Manggota_model->getPetaMember($id);
		$data['list_index2']	= $this->Manggota_model->getPetaPartner($id);

		$data = array_merge($data, path_variable());
		$this->parser->parse('page_template', $data);
	}

	function tutupToko(){
		$id = $this->session->userdata('idanggota');
		$this->Manggota_model->tutupToko($id);
		redirect('dashboard');
	}

	function bukaToko(){
		$id = $this->session->userdata('idanggota');
		$this->Manggota_model->bukaToko($id);
		redirect('dashboard');
	}

	function tambahAlamat($provinsi,$kota,$kecamatan,$desa,$lat,$long,$kodepos){
		$alamat = $this->input->post('alamat');
		$this->Manggota_model->tambahAlamat($provinsi,$kota,$kecamatan,$desa,$lat,$long,$alamat,$kodepos);
	}

	function alamat_tujuan($nourut){
		$row_data = $this->Manggota_model->get_alamat_tujuan($nourut);
		$data = array();
		$data['alamat'] = $row_data->alamat;
		$data['provinsi'] = $row_data->provinsi;
		$data['kota'] = $row_data->kota;
		$data['kecamatan'] = $row_data->kecamatan;
		$data['desa'] = $row_data->desa;
		$data['kodepos'] = $row_data->kodepos;
		$data['lat'] = $row_data->lat;
		$data['lon'] = $row_data->long;
		if($this->Manggota_model->update_informasi($data)){
			if($this->Manggota_model->clear_stutama()){
				$this->Manggota_model->update_stutama_alamat($nourut);
				return true;
			}
			return true;
		}
	}
}

/* End of file Manggota.php */
/* Location: ./application/controllers/Manggota.php */